package ru.totopizza.to_to

import android.content.Context
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView

import kotlinx.android.synthetic.main.activity_restaurants.*
import kotlinx.android.synthetic.main.content_restaurants.*

class RestaurantsActivity : BaseActivity() {
    override val noMenu = true
    var items: ArrayList<RestaurantItem> = ArrayList()

    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: RecyclerView.Adapter<*>? = null
    private var mLayoutManager: RecyclerView.LayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurants)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val restaurants = IikoAPI.loadRestraunts(this)
        if (restaurants != null) {
            val rest = restaurants.values.flatten()

            var location: Location? = null

            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
                val criteria = Criteria()
                val provider = locationManager.getBestProvider(criteria, true)
                if (provider != null)
                    location = locationManager.getLastKnownLocation(provider)
            }

            for (restaurant in rest) {
                items.add(RestaurantItem(restaurant, location))
            }

            items.sortBy { item -> item.distanceInMeters }

            mRecyclerView = recycler_view

            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            mRecyclerView?.setHasFixedSize(true)

            // use a linear layout manager
            mLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
            mRecyclerView?.layoutManager = mLayoutManager

            // specify an adapter (see also next example)
            mAdapter = RecyclerViewAdapter(this, items)
            mRecyclerView?.setAdapter(mAdapter)
        }

        val mapButton = findViewById<ImageView>(R.id.map_restaurant)
        mapButton.setOnClickListener { finish() }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    class RecyclerViewAdapter(private val getContext: BaseActivity,
                              var mDataset: ArrayList<RestaurantItem>,
                              val isSelectable: Boolean = false,
                              val showCity: Boolean = true,
                              val selectedAddress: String = ""): RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

        private val TYPE_HEADER = 0
        private val TYPE_ITEM = 1

        var items = ArrayList<Item>()

        var mOnClickListener: ( (Restaurant) -> Unit )? = null

        init {
            var city = ""
            for (item in mDataset) {
                if (showCity) {
                    if (city != item.From.city) {
                        city = item.From.city
                        items.add(HeaderItem(city))
                    }
                }
                items.add(item)
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            // create a new view
            if (viewType == TYPE_HEADER) {
                val view = LayoutInflater.from(parent?.context).inflate(R.layout.cell_header, parent, false)
                return VHHeader(view)
            } else if (viewType == TYPE_ITEM) {
                if (showCity) {
                    val view = LayoutInflater.from(parent?.context).inflate(R.layout.cell_restaurant, parent, false)
                    return VHItem(getContext, view)
                }
                val view = LayoutInflater.from(parent?.context).inflate(R.layout.cell_restaurant_select, parent, false)
                return VHItem(getContext, view)
            }
            throw RuntimeException("there is no type that matches the type $viewType + make sure your using types correctly")
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            if (holder is VHHeader) {
                val item = items[position] as HeaderItem
                holder.mTitleLabel?.text = item.title
            } else if (holder is VHItem) {
                val item = items[position] as RestaurantItem
                holder.mTitleLabel?.text = item.From.street
                holder?.mWorkTimeLabel?.text = item.From.worktime

                if (item.distance != "") {
                    holder.mDistanceLabel?.visibility = View.VISIBLE
                    holder.mDistanceLabel?.text = item.distance
                } else {
                    holder.mDistanceLabel?.visibility = View.GONE
                }

                holder.item = item.From
                if (isSelectable) {
                    holder.mOnClickListener = mOnClickListener
                }
                if (selectedAddress.isNotEmpty() && selectedAddress == item.From.street) {
                    holder.mSelectableDot?.setImageDrawable(getContext.resources.getDrawable(R.drawable.roundedbg))
                }
            }
        }

        override fun getItemCount(): Int {
            return items.count()
        }

        override fun getItemViewType(position: Int): Int {
            return if (isPositionHeader(position)) TYPE_HEADER else TYPE_ITEM
        }

        private fun isPositionHeader(position: Int): Boolean {
            val item = items[position]
            return item is HeaderItem
        }

        internal inner class VHHeader(itemView: View) : ViewHolder(itemView) {
            var mTitleLabel: TextView? = null

            init {
                mTitleLabel = itemView.findViewById(R.id.title_cart)
            }
        }

        internal inner class VHItem(private val getContext: BaseActivity, itemView: View) : ViewHolder(itemView) {
            var mTitleLabel: TextView? = null
            var mPhoneLabel: TextView? = null
            var mWorkTimeLabel: TextView? = null
            var mDistanceLabel: TextView? = null
            var mSelectableDot: ImageView? = null

            var mPhoneButton: ImageButton? = null

            var item:Restaurant? = null

            var mOnClickListener: ( (Restaurant) -> Unit )? = null

            init {
                mTitleLabel = itemView.findViewById(R.id.title_rest)
                mPhoneLabel = itemView.findViewById(R.id.phone)
                mWorkTimeLabel = itemView.findViewById(R.id.work_time)
                mDistanceLabel = itemView.findViewById(R.id.distance)
                mPhoneButton = itemView.findViewById(R.id.delivery_call)

                mPhoneButton?.setOnClickListener {
                    if (!item?.phone.isNullOrEmpty()){
                        getContext.makeCallOn(item?.phone!!)
                    }
                }
                mSelectableDot = itemView.findViewById(R.id.dot_rest)

                itemView.setOnClickListener {
                    if(item != null) mOnClickListener?.invoke(item!!)
                }
            }
        }

        open class ViewHolder(getView: View): RecyclerView.ViewHolder(getView)
    }
}


