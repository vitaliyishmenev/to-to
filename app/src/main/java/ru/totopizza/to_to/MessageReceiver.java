package ru.totopizza.to_to;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class MessageReceiver extends BroadcastReceiver {
    private static MessageListener mListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle data = intent.getExtras();
        Object[] pdus = new Object[0];
        if (data != null) {
            pdus = (Object[]) data.get("pdus");
        }
        if (pdus != null) {
            for(int i=0; i < pdus.length; i++){
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
                String message = "Sender : " + smsMessage.getDisplayOriginatingAddress()
                        + "Email From: " + smsMessage.getEmailFrom()
                        + "Emal Body: " + smsMessage.getEmailBody()
                        + "Display message body: " + smsMessage.getDisplayMessageBody()
                        + "Time in millisecond: " + smsMessage.getTimestampMillis()
                        + "Message: " + smsMessage.getMessageBody();
                mListener.messageReceived(message);
            }
        }
    }

    public static void bindListener(MessageListener listener){
        mListener = listener;
    }

    public interface MessageListener {
        public void messageReceived(String message);
    }
}
