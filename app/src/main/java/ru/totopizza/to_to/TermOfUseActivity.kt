package ru.totopizza.to_to

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.view.MenuItem
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_term_of_use.*

class TermOfUseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_term_of_use)
        setSupportActionBar(toolbar_term_of_use)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val textBlock1 = findViewById<TextView>(R.id.agreement_value_1)
        textBlock1.text = Html.fromHtml(resources.getString(R.string.agreement_value_1))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (android.R.id.home == item.itemId){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
