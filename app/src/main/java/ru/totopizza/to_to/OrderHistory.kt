package ru.totopizza.to_to

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import kotlinx.android.synthetic.main.activity_order_history.*
import ru.totopizza.to_to.business.PRODUCTITEM
import ru.totopizza.to_to.business.UserHistoryItem

class OrderHistory : BaseActivity() {
    override val noMenu = true
    private lateinit var historyRecycler: RecyclerView
    private lateinit var adapter: HistoryAdapter
    private lateinit var emptyMessage: TextView
    private var orderHistory: ArrayList<UserHistoryItem>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_history)
        setSupportActionBar(toolbar)

        orderHistory = App.userManager.getUserHistory()
        //loadOrderHistory()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        emptyMessage = findViewById(R.id.empty_orders)
        historyRecycler = findViewById(R.id.cart_recycler)
        historyRecycler.layoutManager = LinearLayoutManager(this)
        adapter = HistoryAdapter(this, orderHistory)
        historyRecycler.adapter = adapter
        updateEmptyMessageVisibility()
    }

/*
    private fun loadOrderHistory() {
        getSharedPreferences("TTAppPrefs", MODE_PRIVATE)
        val orderHistoryJson = prefs.getString("orderHistory", null)
        if (orderHistoryJson != null) {
            val moshi = Moshi.Builder().build()
            val type = Types.newParameterizedType(List::class.java, OrderData::class.java)
            val adapter = moshi.adapter<List<OrderData>>(type)
            val result = adapter.fromJson(orderHistoryJson)

            if (result != null) {
                for (order in result) {
                    val items = getItems(order)
                    val item = UserHistoryItem("", "${order.date} ${order.time}", "", "", "", order.sum.toString(), items, "")
                    orderHistory?.add(item)
                }
                if (orderHistory.count() > 5) {
                    orderHistory = ArrayList(orderHistory.reversed().subList(0, 5))
                }
            }
        }
    }

    private fun getItems(order: OrderData): List<PRODUCTITEM> {
        val resultList = ArrayList<PRODUCTITEM>()
        if (order.items.isNullOrEmpty())
            return resultList
        for (itemData in order.items) {
            resultList.add(PRODUCTITEM("", itemData.id, itemData.name, itemData.price.toString(), "", itemData.count))
        }
        return resultList
    }
    */

    private fun updateEmptyMessageVisibility() {
        if (orderHistory.isNullOrEmpty()) {
            emptyMessage.visibility = View.VISIBLE
        } else {
            emptyMessage.visibility = View.GONE
        }
    }

    class HistoryAdapter(var context: BaseActivity, private val items: List<UserHistoryItem>?) : RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val view = inflater.inflate(R.layout.cell_order_history_item, parent, false)
            return HistoryViewHolder(context, view)
        }

        override fun getItemCount(): Int {
            if (!items.isNullOrEmpty()) {
                return items.count()
            }
            return 0
        }

        override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
            if (items != null && items.count() > position - 1)
                holder.bind(items[position])
        }

        class HistoryViewHolder(private var contex: BaseActivity, view: View) : RecyclerView.ViewHolder(view) {

            private var date = itemView.findViewById<TextView>(R.id.date_value)
            private var time = itemView.findViewById<TextView>(R.id.time_value)
            private var price = itemView.findViewById<TextView>(R.id.price_value)
            private var positions = itemView.findViewById<TextView>(R.id.positions_title)
            private var positionsButton = itemView.findViewById<ImageView>(R.id.positions_arrow)
            private var orderList = itemView.findViewById<RecyclerView>(R.id.order_list)
            private lateinit var positionsAdapter: PositionsAdapter
            private var addButton = itemView.findViewById<Button>(R.id.add_to_cart_btn)

            fun bind(item: UserHistoryItem) {
                var dateStr = ""
                var timeStr = ""
                if (item.DATE_INSERT.isNotEmpty()) {
                    dateStr = item.DATE_INSERT.substringBefore(" ")
                    timeStr = item.DATE_INSERT.substringAfter(" ")
                }
                date.text = dateStr
                time.text = timeStr
                val summ = "${item.PRICE} ₽"
                price.text = summ
                orderList.layoutManager = LinearLayoutManager(contex)
                positionsAdapter = PositionsAdapter(contex, item.PRODUCT_ITEM)
                orderList.adapter = positionsAdapter

                positions.setOnClickListener { showOrderList() }
                positionsButton.setOnClickListener { showOrderList() }
                addButton.setOnClickListener { addToCart(item) }
            }

            private fun showOrderList() {
                if (orderList.visibility == View.VISIBLE) {
                    orderList.visibility = View.GONE
                } else {
                    orderList.visibility = View.VISIBLE
                }
            }

            private fun addToCart(item: UserHistoryItem) {
                IikoAPI.addToCartFromHistory(item.PRODUCT_ITEM)
                vibrate(contex)
                openMainActivity()
            }

            private fun openMainActivity() {
                val intent = Intent(contex, MainActivity::class.java)
                intent.putExtra("open_cart", true)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                contex.startActivity(intent)
            }
        }
    }

    class PositionsAdapter(var context: BaseActivity, private var positions: List<PRODUCTITEM>) : RecyclerView.Adapter<PositionsAdapter.PositionViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PositionViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val view = inflater.inflate(R.layout.cell_order_history_position, parent, false)
            return PositionViewHolder(view)
        }

        override fun getItemCount(): Int {
            return positions.count()
        }

        override fun onBindViewHolder(holder: PositionViewHolder, position: Int) {
            holder.bind(positions[position])
        }

        class PositionViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            private var productName = itemView.findViewById<TextView>(R.id.product_name)
            private var productCount = itemView.findViewById<TextView>(R.id.product_count)
            private var productPrice = itemView.findViewById<TextView>(R.id.product_price)

            fun bind(data: PRODUCTITEM) {
                productName.text = data.NAME
                val count = "${data.QUANTITY} шт."
                productCount.text = count
                var price = data.PRICE
                if (price.endsWith(".0000")) {
                    price = price.substringBefore(".0000")
                } else if (price.endsWith("00")) {
                    price = price.substringBefore("00")
                }
                val amount = "$price ₽"
                productPrice.text = amount
            }
        }
    }
}
