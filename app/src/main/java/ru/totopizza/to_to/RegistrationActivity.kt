package ru.totopizza.to_to

import android.os.Bundle
import android.widget.EditText
import com.github.vacxe.phonemask.PhoneMaskManager
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.TextView
import org.jetbrains.anko.doAsync
import ru.totopizza.to_to.business.SmsSender
import java.util.*

class RegistrationActivity : BaseActivity() {
    private var clearPhone: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        val prefs = getSharedPreferences("TTAppPrefs", MODE_PRIVATE)
        //val phone = prefs.getString("customer_phone", "")

        val isCheckout = intent.getBooleanExtra("isCheckout", false)

        val phoneEdit = findViewById<EditText>(R.id.phone)

        PhoneMaskManager()
                .withMask(" (###) ###-##-##")
                .withRegion("+7")
                .withValueListener {
                    clearPhone = it
                }
                .bindTo(phoneEdit)

        val nextBtn = findViewById<Button>(R.id.next_btn)
        nextBtn.isEnabled = false
        phoneEdit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                val textLength = clearPhone.length
                if (textLength < 10 || !clearPhone.startsWith("+79")) {
                    nextBtn.isEnabled = false
                    nextBtn.background = resources.getDrawable(R.drawable.roundedbggrey)
                } else {
                    nextBtn.isEnabled = true
                    nextBtn.background = resources.getDrawable(R.drawable.roundedbg)
                }
            }
            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
            }
        })

        nextBtn.setOnClickListener {
            val rand = Random()
            val code = rand.nextInt(8999) + 1000
            sendSms(code.toString())
            val requestTime = System.currentTimeMillis()

            val intent = Intent(this, ConfirmRegistrationActivity::class.java)
            intent.putExtra("phone", phoneEdit.text.toString())
            intent.putExtra("clearPhone", clearPhone)
            intent.putExtra("code", code.toString())
            intent.putExtra("requestTime", requestTime)
            intent.putExtra("isCheckout", isCheckout)
            //intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            startActivity(intent)
        }

        nextBtn.isEnabled = false
        val personalDataAgreement = findViewById<TextView>(R.id.agree_presonal_data)
        personalDataAgreement.setOnClickListener {
            val intent = Intent(this, TermOfUseActivity::class.java)
            startActivity(intent)
        }
    }

    private fun sendSms(smsCode: String){
        doAsync {
            val sender = SmsSender()
            sender.SendCodeToPhone(smsCode, clearPhone)
        }
    }
}
