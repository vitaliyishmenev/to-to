package ru.totopizza.to_to

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.RecyclerView
import com.github.vacxe.phonemask.PhoneMaskManager
import com.google.android.material.textfield.TextInputLayout
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import info.hoang8f.android.segmented.SegmentedGroup
import kotlinx.android.synthetic.main.activity_checkout.*
import ru.totopizza.to_to.business.*
import java.util.*
import kotlin.collections.ArrayList

class CheckoutActivity : BaseActivity() {
    private val restaurantRequest = 54
    private val addressAddRequest = 55
    private val addressEditRequest = 56
    private var personQuantity = 0
    private var personMaxQuantity = 10
    private var selectedRest: Restaurant? = null
    private var selectedRestAddress = ""
    private var selectedAddress: Address? = null
    private val addressManager = App.addressManager
    private val userManager = App.userManager

    private var clearPhone: String = ""
    private var myCity: String = ""
    private var myCityId: String = ""
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var addressAdapter: AddressAdapter? = null
    private var freeDeliveryPrice = -1
    private var deliveryPrice = -1
    private var urlicoName: String = ""
    private var urlicoOfficialName: String = ""

    private lateinit var prefs: SharedPreferences
    private lateinit var delivery: SegmentedGroup
    private lateinit var name: EditText
    private lateinit var nameInputLayout: TextInputLayout
    private lateinit var phone: EditText
    private lateinit var phoneInputLayout: TextInputLayout
    private lateinit var city: TextView
    private lateinit var addresses: RecyclerView
    private lateinit var addAddress: TextView
    private lateinit var comment: EditText
    private lateinit var personQuantityTextView: TextView

    //private lateinit var quantityLayout: LinearLayout
    private lateinit var persons: TextView
    private lateinit var btnMore: Button
    private lateinit var btnLess: Button
    private lateinit var deliveryAddressTitle: TextView
    private lateinit var restLayout: LinearLayout
    private lateinit var rest: TextView
    private lateinit var addressLayout: LinearLayout
    private lateinit var checkoutBtn: Button
    private lateinit var deliveryTime: TextView
    private lateinit var deliveryTimeLayout: LinearLayout
    private lateinit var deliveryTimeValue: TextView

    override val showCartMenu: Boolean
        get() = false

    override val noMenu: Boolean
        get() = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checkout)
        setSupportActionBar(toolbar_checkout)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        prefs = getSharedPreferences("TTAppPrefs", MODE_PRIVATE)

        delivery = findViewById(R.id.delivery)
        name = findViewById(R.id.nameEdit_checkout)
        nameInputLayout = findViewById(R.id.nameInputLayout_checkout)
        phone = findViewById(R.id.phoneEdit_checkout)
        phoneInputLayout = findViewById(R.id.phoneInputLayout_checkout)
        city = findViewById(R.id.selectedCityValue)
        addresses = findViewById(R.id.addresses)
        addAddress = findViewById(R.id.addAddress)
        comment = findViewById(R.id.comment)
        personQuantityTextView = findViewById(R.id.personQuantity)
        //quantityLayout = findViewById(R.id.quantityLayout)
        persons = findViewById(R.id.quantity)
        btnMore = findViewById(R.id.more)
        btnLess = findViewById(R.id.less)
        deliveryAddressTitle = findViewById(R.id.deliveryAddressTitle)
        restLayout = findViewById(R.id.delivery_addresses_layout_account)
        rest = findViewById(R.id.delivery_addresses_account)
        addressLayout = findViewById(R.id.userAddressLayout)
        checkoutBtn = findViewById(R.id.checkout_btn)
        deliveryTime = findViewById(R.id.deliveryTime)
        deliveryTimeLayout = findViewById(R.id.deliveryTimeLayout)
        deliveryTimeValue = findViewById(R.id.deliveryTimeValue)

        delivery.setOnCheckedChangeListener { _, i ->
            when (i) {
                R.id.isSelfservice -> {
                    restLayout.visibility = View.VISIBLE
                    addressLayout.visibility = View.GONE
                }
                R.id.isDelivery -> {
                    restLayout.visibility = View.GONE
                    addressLayout.visibility = View.VISIBLE
                }
            }
            setDeliveryTitle()
            updateCheckoutButton()
        }

        PhoneMaskManager()
                .withMask(" (###) ###-##-##")
                .withRegion("+7")
                .withValueListener {
                    clearPhone = it
                }
                .bindTo(phone)

        if (userManager.isLoggedIn()) {
            name.setText(userManager.getName())
            phone.setText(userManager.getPhone())
        }
        phone.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                updateCheckoutButton()
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
            }
        })
        myCity = CitiesInfo.instance.getSelectedCity()
        myCityId = CitiesInfo.instance.getSelectedCityId()
        addressManager.loadAddressList(true, false)

        city.text = myCity
        city.isFocusable = false
        city.isClickable = true
        city.keyListener = null

        addresses.setHasFixedSize(true)

        layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        addresses.layoutManager = layoutManager
        addressAdapter = AddressAdapter(editCallback, this, selectAddressCallback)
        addresses.adapter = addressAdapter

        addAddress.setOnClickListener {
            val intent = Intent(this, AddressEditActivity::class.java)
            intent.putExtra("city", myCity)
            intent.putExtra("cityId", myCityId)
            intent.putExtra("allow_delete", false)
            intent.putExtra("show_save_to_account_сheck", userManager.isLoggedIn())
            this.startActivityForResult(intent, addressAddRequest)
        }

        btnMore.setOnClickListener {
            if (personQuantity < personMaxQuantity) {
                personQuantity++
                updatePersonQuantityInTextView()
            }
        }
        btnLess.setOnClickListener {
            if (personQuantity > 0) {
                personQuantity--
            }
            updatePersonQuantityInTextView()
        }
        //if (IikoAPI.isOnlyPizzaInCart) {
        //    personQuantityTextView.visibility = View.GONE
        //    quantityLayout.visibility = View.GONE
        //    personQuantity = 0
        //}
        updatePersonQuantityInTextView()

        restLayout.visibility = View.GONE

        rest.isFocusable = false
        rest.isClickable = true
        rest.keyListener = null

        rest.setOnClickListener {
            val detailActivity = Intent(this, RestaurantSelectActivity::class.java)
            detailActivity.putExtra("city", myCity)
            detailActivity.putExtra("cityId", myCityId)
            if (selectedRestAddress.isNotEmpty()) {
                detailActivity.putExtra("selectedRestAddress", selectedRestAddress)
            }
            this.startActivityForResult(detailActivity, restaurantRequest)
        }

        var items: ArrayList<RestaurantItem> = ArrayList()
        val restaurants = IikoAPI.loadRestraunts(this)
        if (restaurants != null) {
            val rest = restaurants.values.flatten()
            var location: Location? = null
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
                val criteria = Criteria()
                val provider = locationManager.getBestProvider(criteria, true)
                if (provider != null)
                    location = locationManager.getLastKnownLocation(provider)
            }

            rest.mapTo(items) { RestaurantItem(it, location) }
            items = ArrayList(items.filter { item -> item.From.city == city.text.toString() })
            items.sortBy { item -> item.distanceInMeters }
        }

        /*val r = items.firstOrNull()
        if (r != null) {
            selectedRest = r.From
            rest.text = r.From.street
        }*/
        setDeliveryTitle()
        deliveryTimeValue.text = "Как можно скорее"
        loadWorkTimeScheme()
        deliveryTimeLayout.setOnClickListener { showTimeSelectDialog() }
        checkoutBtn.setOnClickListener {
            if (!checkCurrentTime()/* && !BuildConfig.DEBUG*/) {
                showTimeRestrictionDialog()
                return@setOnClickListener
            }
            val canContinue = checkMandatoryField()
            if (canContinue) {
                userManager.saveName(name.text.toString())
                checkout()
            }
        }
        updateCheckoutButton()
    }

    private fun showTimeRestrictionDialog() {
        var startMinStr = startMinutes.toString()
        if (startMinutes == 0) {
            startMinStr = "00"
        }

        var endMinStr = endMinutes.toString()
        if (endMinutes == 0) {
            endMinStr = "00"
        }
        val text = "Извините, но мы закрыты. \nНаш режим работы доставки с $startHour:$startMinStr до $endHour:$endMinStr"
        AlertDialog.Builder(this)
                .setMessage(text)
                .setCancelable(true)
                .setPositiveButton("Ok") { dialog, which -> dialog?.cancel() }
                .create()
                .show()
    }

    private fun updatePersonQuantityInTextView() {
        persons.text = personQuantity.toString()
        if (personQuantity != 0) {
            btnLess.isEnabled = true
            btnLess.setTextColor(resources.getColor(R.color.colorPrimary))
        } else {
            btnLess.isEnabled = false
            btnLess.setTextColor(resources.getColor(R.color.secondaryText))
        }
        if (personQuantity == personMaxQuantity) {
            btnMore.isEnabled = false
            btnMore.setTextColor(resources.getColor(R.color.secondaryText))
        } else {
            btnMore.isEnabled = true
            btnMore.setTextColor(resources.getColor(R.color.colorPrimary))
        }
    }

    private var currentTimeSelectorIndex = 0
    private fun showTimeSelectDialog() {
        val dialog = Dialog(this)
        dialog.setTitle("Выберите время доставки")
        dialog.setContentView(R.layout.time_select)

        val times = createTimeList()
        val numberPicker = dialog.findViewById<NumberPicker>(R.id.number_picker)
        numberPicker.minValue = 0
        numberPicker.maxValue = times.size - 1
        numberPicker.value = currentTimeSelectorIndex
        numberPicker.wrapSelectorWheel = false
        numberPicker.displayedValues = times
        numberPicker.setOnClickListener {
            currentTimeSelectorIndex = numberPicker.value
            deliveryTimeValue.text = times[currentTimeSelectorIndex]
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun loadWorkTimeScheme() {
        val scheme = IikoAPI.loadWorkTime(this)
        if (scheme != null) {
            val timeNow = getTimeNow()
            var dayOfWeek = timeNow.get(Calendar.DAY_OF_WEEK) - 1
            if (dayOfWeek <= 0) {
                dayOfWeek = 7
            }
            startHour = scheme.getTimeByParam(myCityId, dayOfWeek.toString(), WorkTimeScheme.startHour)
            startMinutes = scheme.getTimeByParam(myCityId, dayOfWeek.toString(), WorkTimeScheme.startMin)
            endHour = scheme.getTimeByParam(myCityId, dayOfWeek.toString(), WorkTimeScheme.endHour)
            endMinutes = scheme.getTimeByParam(myCityId, dayOfWeek.toString(), WorkTimeScheme.endMin)
        }
    }

    private var startHour = 11
    private var startMinutes = 0
    private var endHour = 23
    private var endMinutes = 30
    private val beforeCloseLimitMinutes = 0

    private fun createTimeList(): Array<String> {
        val timeList = ArrayList<String>()
        timeList.add("Как можно скорее")
        val timeNow = Calendar.getInstance()
        val hourNow = timeNow.get(Calendar.HOUR_OF_DAY)
        val minNow = timeNow.get(Calendar.MINUTE)

        var startHour = hourNow + 1
        var startMinute: Int
        if (hourNow <= this.startHour - 1) {
            startHour = this.startHour
        }
        val quarter = minNow / 15
        startMinute = quarter * 15 + 15
        if (startMinute == 60)
            startMinute = 0

        for (h in startHour..endHour) {
            if (h == startHour && startMinutes != 0) {
                startMinute = startMinutes
            }
            for (m in startMinute..59 step 15) {
                if (m == 0)
                    timeList.add("$h:${m}0")
                else {
                    if (endMinutes < beforeCloseLimitMinutes && h == endHour - 1 && m >= (60 + endMinutes)) {
                        timeList.add("$h:$m")
                        break
                    }
                    if (endMinutes > beforeCloseLimitMinutes && h == endHour && m >= endMinutes - beforeCloseLimitMinutes) {
                        timeList.add("$h:$m")
                        break
                    }
                    timeList.add("$h:$m")
                }
            }
            startMinute = 0
        }
        return timeList.toTypedArray()
    }

    private fun setDeliveryTitle() {
        if (isSelfService()) {
            deliveryTime.text = "Время самовывоза"
            deliveryAddressTitle.text = "Адрес самовывоза"
        } else {
            deliveryTime.text = "Время доставки"
            deliveryAddressTitle.text = "Адрес доставки"
        }
    }

    private val editCallback = object : OnClickEditListener {
        override fun onClickEdit(context: Context, addressPosition: Int) {
            val intent = Intent(context, AddressEditActivity::class.java)
            intent.putExtra("city", myCity)
            intent.putExtra("cityId", myCityId)
            intent.putExtra("addressPosition", addressPosition)
            intent.putExtra("allow_delete", true)
            intent.putExtra("show_save_to_account_сheck", false)
            startActivityForResult(intent, addressEditRequest)
        }
    }

    private val selectAddressCallback = object : SelectAddressCallback {
        override fun setProperties(addressProperty: AddressProperty) {
            freeDeliveryPrice = addressProperty.freeDeliveryPrice
            deliveryPrice = addressProperty.deliveryPrice
            urlicoName = addressProperty.urlicoName
            urlicoOfficialName = addressProperty.urlicoOfficialName

            val orderSum = IikoAPI.getCartPrice()
            if (freeDeliveryPrice != -1 && freeDeliveryPrice != 0 && orderSum >= freeDeliveryPrice) {
                deliveryPrice = 0
            }
            updateCheckoutButton()
        }

        override fun setSelectedAddress(address: Address?) {
            selectedAddress = address
        }
    }

    private fun updateCheckoutButton() {
        val inputedPhone = phone.text.toString()
        if (!isSelfService()
                && deliveryPrice >= 0
                && freeDeliveryPrice >= 0
                && selectedAddress != null
                && inputedPhone.isNotEmpty()
                && inputedPhone.length >= 10
                && clearPhone.startsWith("+79")) {
            setCheckoutButtonEnable(true)
        } else if (isSelfService() && selectedRest != null) {
            setCheckoutButtonEnable(true)
        } else {
            setCheckoutButtonEnable(false)
        }
    }

    private fun setCheckoutButtonEnable(enable: Boolean) {
        checkoutBtn.isEnabled = enable
        val drawableId = if (enable)
            R.drawable.roundedbg
        else
            R.drawable.roundedbggrey
        checkoutBtn.background = resources.getDrawable(drawableId)
    }

    private fun isSelfService(): Boolean {
        return delivery.checkedRadioButtonId == R.id.isSelfservice
    }

    private fun checkout() {
        var userComment = comment.text.toString()
        if (isSelfService()) {
            userComment += "Адрес самовывоза: "
            val address = selectedRest?.address!!
            userComment += address["city"] + ", " + address["street"] + " " + address["home"] + ", "
        }
        userComment += "время: ${deliveryTimeValue.text}"

        saveOrderDetails(userComment)
        val intent = Intent(this, PaymentMethodActivity::class.java)
        intent.putExtra("isDelivery", !isSelfService())
        startActivity(intent)
    }

    private fun saveOrderDetails(userComment: String) {
        val isSelfService: Boolean = delivery.checkedRadioButtonId == R.id.isSelfservice

        val moshi = Moshi.Builder().build()
        val typeRest = Types.newParameterizedType(Restaurant::class.java)
        val adapterRest = moshi.adapter<Restaurant>(typeRest)
        val selectedRestJson = adapterRest.toJson(selectedRest)
        val typeAddr = Types.newParameterizedType(Address::class.java)
        val adapterAddr = moshi.adapter<Address>(typeAddr)
        val selectedAddressJson = adapterAddr.toJson(selectedAddress)

        val prefEditor = prefs.edit()
        prefEditor.putString("userComment", userComment)
        prefEditor.putInt("personQuantity", personQuantity)
        prefEditor.putBoolean("isSelfService", isSelfService)
        prefEditor.putString("selectedRest", selectedRestJson)
        prefEditor.putString("selectedAddress", selectedAddressJson)
        prefEditor.putInt("deliveryPrice", deliveryPrice)
        prefEditor.putString("urlicoName", urlicoName)
        prefEditor.putString("urlicoOfficialName", urlicoOfficialName)
        prefEditor.apply()
    }

    private fun checkMandatoryField(): Boolean {
        val mandatoryField = resources.getString(R.string.mandatory_field)
        var result = true
        if (name.text.toString() == "") {
            nameInputLayout.error = mandatoryField
            result = false
        } else {
            nameInputLayout.isErrorEnabled = false
        }
        if (phone.text.toString() == "") {
            phoneInputLayout.error = mandatoryField
            result = false
        } else {
            phoneInputLayout.isErrorEnabled = false
        }
        if (isSelfService() && selectedRest == null) {
            rest.error = "Выберите адрес ресторана"
            result = false
        }
        if (!isSelfService() && selectedAddress == null) {
            Toast.makeText(this, "Укажите адрес доставки", Toast.LENGTH_LONG).show()
            result = false
        }
        return result
    }

    private fun checkCurrentTime(): Boolean {
        val timeNow = getTimeNow()
        val hourNow = timeNow.get(Calendar.HOUR_OF_DAY)
        val minNow = timeNow.get(Calendar.MINUTE)
        var isOpen = false
        if (hourNow == startHour) {
            isOpen = minNow >= startMinutes
        } else if (hourNow > startHour) {
            isOpen = true
        }
        var isNotClosed = false
        if (hourNow == endHour) {
            isNotClosed = minNow < endMinutes
        } else if (hourNow < endHour) {
            isNotClosed = true
        }
        return isOpen && isNotClosed
    }

    private fun getTimeNow(): Calendar {
        return Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"))
    }

    /*
    override fun onResume() {
        super.onResume()
        //addressManager.reloadAddressList()
    }*/

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == restaurantRequest && resultCode == RESULT_OK) {
            val r: Restaurant = data?.extras?.getSerializable("rest") as Restaurant
            selectedRest = r
            selectedRestAddress = data.extras?.getString("restAddress") ?: ""
            val rest = findViewById<TextView>(R.id.delivery_addresses_account)
            rest.text = r.street
            updateCheckoutButton()
        }
        if (requestCode == addressAddRequest && resultCode == RESULT_OK) {
            //val address: Address = data?.extras?.getSerializable("address") as Address
            //addressManager.addNewAddress(address)
            addressAdapter?.onNewAddressAdded()
            addressAdapter?.notifyDataSetChanged()
        }

        if (requestCode == addressEditRequest && resultCode == RESULT_OK) {
            val addressDeleted: Int = data?.getIntExtra("address_deleted", -1) ?: -1
            if (addressDeleted >= 0) {
                addressAdapter?.addressDeleted(addressDeleted)
            } else {
                addressAdapter?.addressChanged()
            }
            addressAdapter?.notifyDataSetChanged()
        }
    }

    class AddressAdapter(private val editCallBack: OnClickEditListener,
                         private val context: Context,
                         private var selectAddressCallback: SelectAddressCallback) : RecyclerView.Adapter<AddressAdapter.AddressViewHolder>() {
        private val logtag = "logtag"
        private val radioButtonList = ArrayList<RadioButton>()
        private val addressManager = App.addressManager

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.cell_address_checkout, parent, false)
            return AddressViewHolder(view)
        }

        override fun getItemCount(): Int {
            val count = addressManager.addressCount()
            Log.d(logtag, "AddressAdapter getItemCount() => $count")
            return count
        }

        override fun onBindViewHolder(holder: AddressViewHolder, position: Int) {
            Log.d(logtag, "AddressAdapter onBindViewHolder($position)")
            val pos = position
            val dataset = addressManager.addressList
            val address = dataset[pos]
            Log.d(logtag, "     address: $address")
            holder.addressName.text = address.name
            var addressStr = ""
            if (!address.suburban.isNullOrEmpty() && address.suburban != address.city) {
                addressStr = address.suburban + ", "
            }
            addressStr += "${address.street}, д.${address.home}"
            if (address.apartment != "") {
                addressStr += ", кв. ${address.apartment}"
            }
            holder.address.text = addressStr

            radioButtonList.add(holder.radioButton)

            holder.radioButton.setOnClickListener {
                Log.d(logtag, "     OnSelectAddress ViewHolder($pos)")
                for (button in radioButtonList) {
                    button.isChecked = false
                }
                radioButtonList[position].isChecked = true
                val preferAddress = dataset[pos]
                selectAddressCallback.setSelectedAddress(preferAddress)
                selectAddressCallback.setProperties(AddressProperty(holder.urlicoName, holder.urlicoOfficialName, holder.freeDeliveryPrice, holder.deliveryPrice))
            }
            holder.editAddressButton.setOnClickListener {
                //Log.d(logtag, "     OnEditAddress ViewHolder($pos)")
                editCallBack.onClickEdit(context, pos)
            }
            val locationAddress = getAddressStr(address)
            Log.d(logtag, "     pos: $pos")
            Log.d(logtag, "     locationAddress: $locationAddress")
            val properties = CitiesInfo.instance.getPropertiesByAddress(locationAddress, context)
            if (properties != null) {
                var deliveryPrice = properties.deliveryPrice
                var freeDeliveryPrice = properties.freeDeliveryPrice
                if (address.delivery > 0) {
                    deliveryPrice = address.delivery
                    freeDeliveryPrice = 0
                }
                holder.freeDeliveryPrice = freeDeliveryPrice
                holder.deliveryPrice = deliveryPrice
                if (!address.urlico.isNullOrEmpty()) {
                    holder.urlicoName = address.urlico
                    holder.urlicoOfficialName = CitiesInfo.instance.getUrlicoOfficialName(address.urlico)
                } else {
                    holder.urlicoName = properties.urlicoName
                    holder.urlicoOfficialName = properties.urlicoOfficialName
                }
                Log.d(logtag, "     deliveryPrice: $deliveryPrice")
                Log.d(logtag, "     freeDeliveryPrice: $freeDeliveryPrice")
                Log.d(logtag, "     urlicoName: ${properties.urlicoName}")
                Log.d(logtag, "     urlicoOfficialName: ${properties.urlicoOfficialName}")

                val orderSum = IikoAPI.getCartPrice()
                val deliveryPriceStr: String
                if (orderSum >= freeDeliveryPrice && freeDeliveryPrice != 0) {
                    deliveryPriceStr = context.resources.getString(R.string.free)
                } else {
                    deliveryPriceStr = "$deliveryPrice \u20BD"
                }
                holder.deliveryPriceText.text = deliveryPriceStr
            } else {
                Log.d(logtag, "     properties == null")
                selectAddressCallback.setProperties(AddressProperty("", "", -1, -1))
                holder.deliveryPriceText.text = context.resources.getString(R.string.notDelivery)
            }
        }

        private fun getAddressStr(address: Address): String {
            val result = StringBuilder()
            if (!address.suburban.isNullOrEmpty() && address.city != address.suburban) {
                result.append(address.suburban)
            } else {
                result.append("город ${address.city}")
            }
            result.append(", ")
            val needStreetString = isItStreet(address.street)
            if (needStreetString) {
                result.append("улица ")
            }
            result.append(address.street)
            result.append(", д.${address.home}")
            return result.toString()
        }

        private fun isItStreet(streetName: String): Boolean {
            val nonStreetObjects = arrayOf("НСТ", "СОНТ", "СОТ", "Cад", "СНТ", "ЖК", "ДНТ")
            for (obj in nonStreetObjects) {
                val streetNameLc = streetName.toLowerCase()
                if (streetNameLc.contains(obj.toLowerCase())) {
                    return false
                }
            }
            return true
        }

        fun onNewAddressAdded() {
            radioButtonList.clear()
            //notifyItemInserted(radioButtonList.size-1)
            notifyDataSetChanged()
        }

        fun addressChanged() {
            radioButtonList.clear()
            notifyDataSetChanged()
        }

        fun addressDeleted(index: Int) {
            radioButtonList.clear()
            notifyItemRemoved(index)
            notifyDataSetChanged()
        }

        class AddressViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
            var addressName: TextView = view.findViewById(R.id.address_name_addresses)
            var address: TextView = view.findViewById(R.id.address_value_addresses)
            var editAddressButton: ImageView = view.findViewById(R.id.edit_address_addresses)
            var radioButton: RadioButton = view.findViewById(R.id.radioButton)
            var deliveryPriceText: TextView = view.findViewById(R.id.delivery_price)
            var freeDeliveryPrice = -1
            var deliveryPrice = -1
            var urlicoName = ""
            var urlicoOfficialName = ""
        }
    }

    interface SelectAddressCallback {
        fun setSelectedAddress(address: Address?)
        fun setProperties(addressProperty: AddressProperty)
    }
}

interface OnClickEditListener {
    fun onClickEdit(context: Context, addressPosition: Int)
}

