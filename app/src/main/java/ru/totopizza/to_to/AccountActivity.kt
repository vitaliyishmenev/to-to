package ru.totopizza.to_to

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.MenuItem
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_account.*
import ru.totopizza.to_to.business.UserManager

class AccountActivity : AppCompatActivity() {
    private val user = App.userManager
    private lateinit var nameEdit: EditText
    private lateinit var phoneEdit: TextView
    private lateinit var emailEdit: EditText
    private lateinit var birthdayEdit: TextView
    private lateinit var deliveryAddressesLayout: LinearLayout
    private lateinit var orderHistoryLayout: LinearLayout
    private lateinit var cancel: TextView
    private var userChanged = false
    private var afterConfirmation = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)
        setSupportActionBar(toolbar_account)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        afterConfirmation = intent.getBooleanExtra("afterConfirmation", false)
        nameEdit = findViewById(R.id.name_edit_account)
        if (user.getName().isNotEmpty()) {
            nameEdit.setText(user.getName())
        }
        phoneEdit = findViewById(R.id.phone_edit_account)
        phoneEdit.text = user.getPhone()

        emailEdit = findViewById(R.id.mail_edit_account)
        if (user.getMail().isNotEmpty()) {
            emailEdit.setText(user.getMail())
        }
        birthdayEdit = findViewById(R.id.birthday_edit_account)
        val birthdayText = user.getBirthdayText()
        if (birthdayText.isNotEmpty())
            birthdayEdit.text = birthdayText

        deliveryAddressesLayout = findViewById(R.id.delivery_addresses_layout_account)
        orderHistoryLayout = findViewById(R.id.order_history_layout_account)
        cancel = findViewById(R.id.cancel_account)

        birthdayEdit.setOnClickListener {
            if (!user.getBirthdayIsSetted()) {
                showDatePicker()
            }
        }
        deliveryAddressesLayout.setOnClickListener { showAddressActivity() }
        orderHistoryLayout.setOnClickListener { showOrderHistoryActivity() }
        nameEdit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                val name = s.toString()
                user.saveName(name)
                userChanged = true
            }
        })
        emailEdit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                val isValid = isValidEmail(s)
                if (isValid) {
                    val mail = s.toString()
                    user.saveMail(mail)
                    userChanged = true
                }
            }
        })
        cancel.setOnClickListener { exit() }
    }

    private var dialog: DatePickerDialog? = null

    private fun showDatePicker() {
        val year = user.getBirthdayYear()
        val month = user.getBirthdayMonth()
        val day = user.getBirthdayDay()
        dialog = DatePickerDialog(this, dateListener, year, month, day)
        //tryHideYear(dialog!!)
        dialog?.show()
    }

/*
    private fun tryHideYear(pickerDialog: DatePickerDialog) {
        val dp = findDatePicker(pickerDialog.window?.decorView as ViewGroup)
        if (dp != null) {
            (dp.getChildAt(0) as ViewGroup).getChildAt(0).visibility = View.GONE
        }

        try {
            val dialogFields: Array<Field> = picker.javaClass.declaredFields
            for (datePickerField in dialogFields) {
                if (datePickerField.name == "mDatePicker") {
                    datePickerField.isAccessible = true
                    val yearPickerFields: Array<Field> = datePickerField.javaClass.declaredFields
                    for (yearPickerField in yearPickerFields) {
                        if (yearPickerField.name == "mYearPicker") {
                            yearPickerField.isAccessible = true
                            var yearPicker = Any()
                            yearPicker = field.get(picker)
                            (yearPicker as View).setVisibility(View.GONE)
                        }
                    }
                }
            }
        } catch (e: SecurityException) {
            Log.d("ERROR", e.message)
        } catch (e: IllegalArgumentException) {
            Log.d("ERROR", e.message)
        } catch (e: IllegalAccessException) {
            Log.d("ERROR", e.message)
        }
    }
    private fun findDatePicker(group: ViewGroup?): DatePicker? {
        if (group != null) {
            var i = 0
            val j = group.childCount
            while (i < j) {
                val child = group.getChildAt(i)
                if (child is DatePicker) {
                    return child
                } else if (child is ViewGroup) {
                    val result = findDatePicker(child)
                    if (result != null) return result
                }
                i++
            }
        }
        return null
    }
    */

    private var pickedYear: Int = 2001
    private var pickedMonth: Int = 2
    private var pickedDay: Int = 2
    private val dateListener = DatePickerDialog.OnDateSetListener { _, picked_year, picked_monthOfYear, picked_dayOfMonth ->
        pickedYear = picked_year
        pickedMonth = picked_monthOfYear
        pickedDay = picked_dayOfMonth
        dialog?.dismiss()
        showBirthdayConfirmDialog()
    }

    private fun showBirthdayConfirmDialog() {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setCancelable(false)
        alertDialog.setTitle("Проверьте правильность даты")
        alertDialog.setMessage("После установки изменить дату рождения будет невозможно")
        alertDialog.setPositiveButton("ОК") { _, _ -> onConfirmBirthday() }
        alertDialog.setNegativeButton("Отмена") { dialog, _ -> dialog.dismiss() }
        alertDialog.show()
    }

    private fun onConfirmBirthday() {
        user.saveCustomerBirthday(pickedYear, pickedMonth+1, pickedDay)
        val month = user.getMonthByIndex(pickedMonth+1)
        val birthdayText = "$pickedDay $month"
        birthdayEdit.text = birthdayText
        userChanged = true
    }

    private fun showAddressActivity() {
        val intent = Intent(this, DeliveryAddressesActivity::class.java)
        intent.putExtra("allow_add", true)
        intent.putExtra("show_save_to_account_сheck", false)
        startActivity(intent)
    }

    private fun showOrderHistoryActivity() {
        val intent = Intent(this, OrderHistory::class.java)
        startActivity(intent)
    }

    fun isValidEmail(target: CharSequence): Boolean {
        return if (TextUtils.isEmpty(target)) {
            false
        } else {
            android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }

    private fun exit() {
        user.saveLoggedIn(false)
        user.resetBirthday()
        openMainActivity()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (android.R.id.home == item.itemId) {
            openMainActivity()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun openMainActivity(){
        if (afterConfirmation) {
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        } else {
            finish()
        }
    }

    override fun onPause() {
        super.onPause()
        if (userChanged)
            user.saveUserToServer()
    }
}
