package ru.totopizza.to_to

import androidx.multidex.MultiDexApplication
import ru.totopizza.to_to.business.AddressManager
import ru.totopizza.to_to.business.UserManager
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.*

public class App : MultiDexApplication() {
    companion object {
        val userManager = UserManager()
        val addressManager = AddressManager()
    }

    override fun onCreate() {
        super.onCreate()
        userManager.load(this)
        addressManager.init(this)
    }

}