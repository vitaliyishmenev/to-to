package ru.totopizza.to_to

public data class Location(val lat: Double, val lon: Double)