package ru.totopizza.to_to

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_delivery_addresses.*
import ru.totopizza.to_to.business.AddressManager
import ru.totopizza.to_to.business.CitiesInfo

class DeliveryAddressesActivity : AppCompatActivity() {

    private val addressManager = App.addressManager
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var addressAdapter: AddressAdapter? = null
    private lateinit var addresses: RecyclerView
    private var city = ""
    private var cityId = ""
    private val addressAddRequest = 75
    private var showSaveToAccountCheckBox = true

    private lateinit var emptyMessageLayout: LinearLayout
    private lateinit var addAddressButton: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delivery_addresses)
        setSupportActionBar(toolbar_addresses)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        showSaveToAccountCheckBox = intent.getBooleanExtra("show_save_to_account_сheck", true)

        city = CitiesInfo.instance.getSelectedCity()
        cityId = CitiesInfo.instance.getSelectedCityId()
        addressManager.init(this)
        addressManager.loadAddressList(false, true)

        addresses = findViewById(R.id.recycler_view_address)
        emptyMessageLayout = findViewById(R.id.empty_message_layout)
        emptyMessageLayout.setOnClickListener { startAddAddressActivity() }

        layoutManager = LinearLayoutManager(this)
        addresses.layoutManager = layoutManager
        addressAdapter = AddressAdapter(this, addressManager, editCallback)
        addresses.adapter = addressAdapter

        addAddressButton = findViewById(R.id.add_address)
        val allowAddAddress = intent.getBooleanExtra("allow_add", false)
        if (!allowAddAddress) {
            addAddressButton.visibility = View.GONE
            addAddressButton.isEnabled = false
        } else {
            addAddressButton.visibility = View.VISIBLE
            addAddressButton.isEnabled = true
            addAddressButton.setOnClickListener { startAddAddressActivity() }
        }

    }

    override fun onResume() {
        super.onResume()
        addressAdapter?.notifyDataSetChanged()
        updateEmptyMessageVisibility()
    }

    private fun updateEmptyMessageVisibility(){
        if (addressManager.addressCount() == 0) {
            emptyMessageLayout.visibility = View.VISIBLE
        } else {
            emptyMessageLayout.visibility = View.GONE
        }
    }

    private val editCallback = object : OnClickEditListener {
        override fun onClickEdit(context: Context, addressPosition: Int) {
            val intent = Intent(context, AddressEditActivity::class.java)
            intent.putExtra("city", city)
            intent.putExtra("cityId", cityId)
            intent.putExtra("addressPosition", addressPosition)
            intent.putExtra("allow_delete", true)
            intent.putExtra("show_save_to_account_сheck", false)
            startActivity(intent)
        }
    }

    private fun startAddAddressActivity() {
        val intent = Intent(this, AddressEditActivity::class.java)
        intent.putExtra("city", city)
        intent.putExtra("cityId", cityId)
        intent.putExtra("allow_delete", false)
        intent.putExtra("show_save_to_account_сheck", showSaveToAccountCheckBox)
        this.startActivityForResult(intent, addressAddRequest)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == addressAddRequest && resultCode == RESULT_OK) {
            //val address:Address = data?.extras?.getSerializable("address") as Address
            //addressManager.addNewAddress(address)
            addressAdapter?.notifyDataSetChanged()
            updateEmptyMessageVisibility()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (android.R.id.home == item.itemId){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    class AddressAdapter(val context: Context, private val addressManager: AddressManager, private val editCallBack: OnClickEditListener): RecyclerView.Adapter<AddressAdapter.AddressViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.cell_address_adresses, parent, false)
            return AddressViewHolder(view)
        }

        override fun getItemCount(): Int {
            return addressManager.addressCount()
        }

        override fun onBindViewHolder(holder: AddressViewHolder, position: Int) {
            val address = addressManager.addressList[position]
            holder.addressName.text = address.name
            var addressStr = "${address.street}, д.${address.home}"
            if (address.apartment != "") {
                addressStr += ", кв. ${address.apartment}"
            }
            holder.address.text = addressStr

            holder.itemView.setOnClickListener {
                editCallBack.onClickEdit(context, position)
            }
        }

        class AddressViewHolder(var view: View): RecyclerView.ViewHolder(view) {
            var addressName: TextView = view.findViewById(R.id.address_name_addresses)
            var address: TextView = view.findViewById(R.id.address_value_addresses)
        }
    }
}
