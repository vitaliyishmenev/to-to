package ru.totopizza.to_to

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

import kotlinx.android.synthetic.main.activity_main.*


class PromoActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_promo)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val titleText = intent.getStringExtra("title")
        val imgUrl = intent.getStringExtra("url")
        val description = intent.getStringExtra("description")

        val imageView = findViewById<ImageView>(R.id.image)
        val descView = findViewById<TextView>(R.id.desc)
        val titleView = findViewById<TextView>(R.id.title_promo)

        if (imgUrl != null && imgUrl != "") {
            Picasso.get()
                    .load(imgUrl)
                    .placeholder(R.drawable.placeholder)
                    .into(imageView)
        } else {
            imageView.visibility = View.GONE
        }

        descView.text = description
        titleView.text = titleText
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

//    override fun onOptionsItemSelected(item: MenuItemViewHolder) = when (item.itemId) {
//        R.id.home -> {
//            finish()
//            true
//        }
//        else -> super.onOptionsItemSelected(item)
//    }

}
