package ru.totopizza.to_to

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.constraintlayout.widget.ConstraintLayout
import android.view.View
import android.widget.Button
import android.widget.RadioButton
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.google.android.gms.wallet.*
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import kotlinx.android.synthetic.main.activity_payment_method.*
import java.util.*

import org.json.JSONObject
import com.google.android.gms.common.api.ApiException
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.json.JSONException
import ru.totopizza.to_to.checkout.GooglePay
import ru.totopizza.to_to.checkout.SberAcquiring
import ru.totopizza.to_to.business.ProductListItem
import ru.totopizza.to_to.business.ServerApi
import ru.totopizza.to_to.business.CitiesInfo
import ru.totopizza.to_to.mail.GMailSender
import java.io.IOException
import java.lang.Exception
import kotlin.concurrent.schedule

class PaymentMethodActivity : AppCompatActivity() {
    private lateinit var prefs: SharedPreferences
    private val logtag = "logtag"
    private var customerCity = ""
    private var customerName = ""
    private var customerPhone = ""
    private var selectedPaymentMethodIndex: Int = 0

    private var isSelfService: Boolean = false
    private var personQuantity: Int = 0
    private var userComment: String = ""
    private var paymentMethodComment: String = ""
    private var cashBackComment: String = ""
    private var deliveryPrice: Int = 0
    private var urlicoName: String = ""
    private var urlicoOfficialName: String = ""
    private var selectedRest: Restaurant? = null
    private var selectedAddress: Address? = null
    private var sberOrderId = ""

    //private var orderHistory = ArrayList<OrderData>()
    //private var orderHistoryArray = ArrayList<ItemData>()
    //private var orderHistoryItem: OrderData? = null
    private var orderItems = ArrayList<Any>()
    private var orderItemsForServer = ArrayList<ProductListItem>()
    private var orderSum = 0
    private var payedSum = 0
    private var generatedOrderNumber = ""
    private var paymentToken = ""
    private var stringToMailOrder = ""
    private var itemsAsString = ""
    //private var orderRequest: Map<String, Any>? = null

    private lateinit var noCash: TextView
    private lateinit var cash1000: TextView
    private lateinit var cash2000: TextView
    private lateinit var cash5000: TextView
    private lateinit var cashBackList: List<TextView>
    private lateinit var cashBackLayout: ConstraintLayout

    private lateinit var cardRadioButton: RadioButton
    private lateinit var cashRadioButton: RadioButton
    private lateinit var cardByAppRadioButton: RadioButton
    private lateinit var googlePayRadioButton: RadioButton
    private lateinit var paymentMethodList: List<RadioButton>
    private lateinit var checkoutBtn: Button
    private val indexCard = 0
    private val indexCash = 1
    private val indexSber = 2
    private val indexGpay = 3

    private lateinit var mPaymentsClient: PaymentsClient
    private val LOAD_PAYMENT_DATA_REQUEST_CODE = 421
    private val PAY_WITH_SBER_REQUEST_CODE = 335

    private var showOnlinePM = true
    private var isOrderPosted = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_method)
        setSupportActionBar(toolbar_payment_method)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        prefs = getSharedPreferences("TTAppPrefs", MODE_PRIVATE)

        initView()

        mPaymentsClient = Wallet.getPaymentsClient(
                this,
                Wallet.WalletOptions.Builder()
                        .setEnvironment(WalletConstants.ENVIRONMENT_PRODUCTION)
                        .build())

        loadCustomer()
        loadOrderDetails()
        loadSelectedPaymentMethod()
        //updateOnlinePMView()
    }

    private fun initView() {
        cardRadioButton = findViewById(R.id.card_radio_button)
        cashRadioButton = findViewById(R.id.cash_radio_button)
        cardByAppRadioButton = findViewById(R.id.card_by_app_radio_button)
        googlePayRadioButton = findViewById(R.id.google_pay_radio_button)
        paymentMethodList = listOf(cardRadioButton, cashRadioButton, cardByAppRadioButton, googlePayRadioButton)

        noCash = findViewById(R.id.no_cash_back)
        cash1000 = findViewById(R.id.cash_back_1000)
        cash2000 = findViewById(R.id.cash_back_2000)
        cash5000 = findViewById(R.id.cash_back_5000)
        cashBackLayout = findViewById(R.id.cash_back_layout)
        cashBackList = listOf(cash1000, cash2000, cash5000, noCash)

        cardRadioButton.setOnClickListener { onSelectPaymentMethod(indexCard) }
        cashRadioButton.setOnClickListener { onSelectPaymentMethod(indexCash) }
        cardByAppRadioButton.setOnClickListener { onSelectPaymentMethod(indexSber) }
        googlePayRadioButton.setOnClickListener { onSelectPaymentMethod(indexGpay) }

        cash1000.setOnClickListener { onClickCashBack(0) }
        cash2000.setOnClickListener { onClickCashBack(1) }
        cash5000.setOnClickListener { onClickCashBack(2) }
        noCash.setOnClickListener { onClickCashBack(3) }

        checkoutBtn = findViewById(R.id.checkout_btn)
        checkoutBtn.setOnClickListener {
            checkoutBtn.isEnabled = false
            Timer("blockCheckout", false).schedule(5000) {
                this@PaymentMethodActivity.runOnUiThread {
                    checkoutBtn.isEnabled = true
                }
            }
            doAsync {
                checkout()
            }
        }

        val isDelivery = intent.getBooleanExtra("isDelivery", false)
        if (isDelivery) {
            cashRadioButton.text = "Наличными курьеру"
            cardRadioButton.text = "Картой курьеру"
        }
    }

    //call updateOnlinePMView() after loadOrderDetails()
    private fun updateOnlinePMView() {
        val urlicoLogin = SberAcquiring.getLogin(urlicoName)
        if (!urlicoName.isNullOrEmpty() && urlicoLogin.isNotEmpty() && showOnlinePM) {
            cardByAppRadioButton.visibility = View.VISIBLE
            cardByAppRadioButton.isEnabled = true
            googlePayRadioButton.visibility = View.VISIBLE
            googlePayRadioButton.isEnabled = true
            possiblyShowGooglePayButton()
        } else {
            cardByAppRadioButton.isEnabled = false
            cardByAppRadioButton.visibility = View.GONE
            googlePayRadioButton.isEnabled = false
            googlePayRadioButton.visibility = View.GONE
        }
    }

    override fun onResume() {
        super.onResume()
        onSelectPaymentMethod(selectedPaymentMethodIndex)
        updateOnlinePMView()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (android.R.id.home == item.itemId) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onSelectPaymentMethod(index: Int) {
        generatedOrderNumber = ""
        selectedPaymentMethodIndex = index
        selectedPaymentMethod()
        if (selectedPaymentMethodIndex == indexCash) {
            cashBackLayout.visibility = View.VISIBLE
        } else {
            cashBackLayout.visibility = View.GONE
        }
        if (selectedPaymentMethodIndex == indexGpay) {
            checkoutBtn.text = "Оплатить через Google Pay"
        } else {
            checkoutBtn.text = "Оформить заказ"
        }
        paymentMethodComment = when (selectedPaymentMethodIndex) {
            indexCard -> cardRadioButton.text.toString()
            indexCash -> cashRadioButton.text.toString()
            indexSber -> "оплачено через Сбербанк"
            indexGpay -> "оплачено через Google Pay"
            else -> ""
        }
        if (selectedPaymentMethodIndex != indexCash) {
            cashBackComment = ""
            return
        }
    }

    private fun onClickCashBack(index: Int) {
        for (tv in cashBackList) {
            tv.setTextColor(resources.getColor(R.color.secondaryText))
        }
        cashBackList[index].setTextColor(resources.getColor(R.color.colorPrimary))
        when (index) {
            0 -> cashBackComment = "Нужна сдача с 1000 р."
            1 -> cashBackComment = "Нужна сдача с 2000 р."
            2 -> cashBackComment = "Нужна сдача с 5000 р."
            3 -> cashBackComment = "Без сдачи"
        }
    }

    private fun checkout() {
        //loadOrderHistory()
        loadOrderItems()
        saveSelectedPaymentMethod()
        when (selectedPaymentMethodIndex) {
            indexCard -> postOrder(false)
            indexCash -> postOrder(false)
            indexSber -> sberCheckout()
            indexGpay -> requestPaymentWithGooglePay()
        }
    }

    /*
    private fun loadOrderHistory() {
        val orderHistoryJson = prefs.getString("orderHistory", null)
        if (orderHistoryJson != null) {
            val moshi = Moshi.Builder().build()

            val type = Types.newParameterizedType(List::class.java, OrderData::class.java)
            val adapter = moshi.adapter<List<OrderData>>(type)

            val result = adapter.fromJson(orderHistoryJson)
            if (result != null) {
                orderHistory = ArrayList(result)
            }
        }
    }*/

    private fun loadOrderItems() {
        orderSum = deliveryPrice
        orderItems.clear()
        orderItemsForServer.clear()
        //orderHistoryArray.clear()
        itemsAsString = ""
        var pizzaSize = ""
        var modifierId = ""

        val cart = IikoAPI.cart
        for (item in cart) {
            val product = IikoAPI.getProduct(item.value.id)
            if (product != null) {
                var sum = 0
                if (item.value.size != null) {
                    val size = IikoAPI.getProduct(item.value.size!!)
                    if (size != null) {
                        sum = size.price * item.value.quantity
                    }
                } else sum = product.price * item.value.quantity

                orderSum += sum

                val modifierArray: ArrayList<Map<String, Any>> = ArrayList()
                val ids = ArrayList<String>()
                var isPizzaSize: Boolean
                val isPizzaPiquancy = false

                if (product.groupModifiers.count() > 0) {
                    for (mod in product.groupModifiers) {
                        val gMod = IikoAPI.getGroup(mod.modifierId)

                        if (gMod != null) {
                            isPizzaSize = gMod.name.contains("пицца", true)
//                                    isPizzaPiquancy = gMod.name.contains("острота", true)

                            if (isPizzaSize) {
                                val p = IikoAPI.getProduct(item.value.size!!)
                                if (p != null) {
                                    ids.add(mod.modifierId)
                                    val preprepareModifier = mapOf(
                                            "groupId" to mod.modifierId,
                                            "groupName" to gMod.name,
                                            "amount" to mod.minAmount,
                                            "id" to p.id,
                                            "name" to p.name
                                    )
                                    modifierId = p.id
                                    pizzaSize = p.name

                                    modifierArray.add(preprepareModifier)

                                    if (p.description != "") {

                                        ids.add(p.description)
                                        val preprepareModifier = mapOf(
                                                "id" to p.description,
                                                "amount" to item.value.quantity
                                        )

                                        modifierArray.add(preprepareModifier)
                                    }
                                }
                            } else if (isPizzaPiquancy) {
                                val p = IikoAPI.getProduct(item.value.piquancy!!)
                                if (p != null) {
                                    ids.add(mod.modifierId)
                                    val preprepareModifier = mapOf(
                                            "groupId" to mod.modifierId,
                                            "groupName" to gMod.name,
                                            "amount" to mod.minAmount,
                                            "id" to p.id,
                                            "name" to p.name
                                    )
                                    modifierId = p.id
                                    modifierArray.add(preprepareModifier)
                                }
                            }
                        }
                    }
                }

                val orderItem = mapOf(
                        "id" to product.id,
                        "code" to product.code,
                        "name" to product.name,
                        "amount" to item.value.quantity,
                        "sum" to sum,
                        "modifiers" to modifierArray
                )
                itemsAsString += "${product.name} $pizzaSize x ${item.value.quantity} шт.: $sum руб. \n"

                val itemHistory = ItemData(product.id, item.value.size
                        ?: "", product.name, sum, item.value.quantity)

                //orderHistoryArray.add(itemHistory)
                orderItems.add(orderItem)
                orderItemsForServer.add(ProductListItem(item.value.quantity, product.id, modifierId))
            }
        }
    }

    private fun postOrder(payed: Boolean) {
        Log.d(logtag, "PaymentMethodActivity postOrder(payed: $payed)")
        if (isOrderPosted)
            return
        doAsync {
            val orderRequest = createOrderRequest(payed)
            Log.d(logtag, "PaymentMethodActivity postOrder(), orderRequest: $orderRequest")
            //IikoAPI.resetMailFlag()
            saveSelectedPaymentMethod()
            GMailSender().sendMail("Новый заказ", stringToMailOrder)
            IikoAPI.postOrder(orderRequest) { data ->
                if (data.isNullOrEmpty()) {
                    return@postOrder
                }
                onOrderPosted(data)
            }
        }
    }

    private fun onOrderPosted(data: String) {
        Log.d(logtag, "PaymentMethodActivity onOrderPosted(data: $data)")
        isOrderPosted = true
        try {
            val result = convertToResult(data)
            //todo сохранить номер заказа
            val number = result?.get("number") as Int?
            val orderId = result?.get("orderId") as String?
            /*
            if (number != null) {
                orderHistoryItem?.number = number
            }*/
            sendOrderToServer()
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        runOnUiThread{
            openOrderCompleteActivity()
        }
    }

    private fun convertToResult(data: String): Map<String, Any>? {
        val moshi = Moshi.Builder().build()
        val type = Types.newParameterizedType(Map::class.java, String::class.java, Any::class.java)
        val adapter = moshi.adapter<Map<String, Any>>(type)
        return adapter.fromJson(data)
    }

    private fun createOrderRequest(payed: Boolean): Map<String, Any> {
        val addressOrder: Map<String, String>
        val adressString: String
        if (isSelfService && selectedRest != null) {
            addressOrder = selectedRest?.address!!
            adressString = convertRestAddressToString(selectedRest?.address!!)
        } else {
            val city = if (selectedAddress != null && !selectedAddress!!.suburban.isNullOrEmpty()) {
                selectedAddress!!.suburban
            } else
                selectedAddress?.city ?: customerCity

            val street = selectedAddress?.street ?: ""
            val home = selectedAddress?.home ?: ""
            val apartment = selectedAddress?.apartment ?: ""
            val floor = selectedAddress?.floor ?: ""
            val entrance = selectedAddress?.entrance ?: ""
            addressOrder = mapOf(
                    "city" to city,
                    "street" to street,
                    "home" to home,
                    "apartment" to apartment,
                    "floor" to floor,
                    "entrance" to entrance)
            adressString = "Адрес доставки: $city, ул. $street, д. $home, кв. $apartment, этаж: $floor, подъезд: $entrance"
        }
        var paymentMethodStr = "Способ оплаты: $paymentMethodComment"
        var paymentTypeCode = ""
        when (selectedPaymentMethodIndex) {
            indexCard -> {
                paymentTypeCode = IikoAPI.getPaymentTypeCodeForCard()
            }
            indexCash -> {
                paymentMethodStr += ", $cashBackComment"
                paymentTypeCode = IikoAPI.getPaymentTypeCodeForCash()
            }
            indexSber -> {
                paymentMethodStr += ", юрлицо: $urlicoOfficialName"
                paymentTypeCode = IikoAPI.getPaymentTypeCodeForOnlinePM()
            }
            indexGpay -> {
                paymentMethodStr += ", юрлицо: $urlicoOfficialName"
                paymentTypeCode = IikoAPI.getPaymentTypeCodeForOnlinePM()
            }
        }
        val deliveryCostStr = "Стоимость доставки: $deliveryPrice"
        val order = mapOf(
                "items" to orderItems,
                "phone" to customerPhone,
                "address" to addressOrder,
                "comment" to "$userComment, $paymentMethodStr, $deliveryCostStr,  Отпавленно из МП Android v. ${BuildConfig.VERSION_NAME}",
                "personsCount" to personQuantity,
                "fullSum" to orderSum,
                "isSelfService" to isSelfService
        ).toMutableMap()
        val orderString = convertOrderToString()

        val customerOrder = mapOf(
                "name" to customerName,
                "phone" to customerPhone
        )
        val customerString = "Имя: $customerName\nНомер телефона: $customerPhone"
        val commentString = "Коментарий: $userComment"
        stringToMailOrder = "$customerString \n$orderString \n$adressString \n$commentString, \n$paymentMethodStr, \n$deliveryCostStr, \nОтпавленно из МП Android"

        val orderRequest: Map<String, Any>
        if (paymentToken.isNotEmpty() || payed) {
            val paymentType = mapOf(
                    "id" to "",
                    "code" to paymentTypeCode,     //12 - карта, cash - кэш, 25 - онлайн
                    "name" to "Оплата банковской картой",
                    "comment" to "paymentToken: $paymentToken, Оплачено: $payedSum р., Номер заказа в сбере: $generatedOrderNumber",
                    "deleted" to false)
            val paymentItems = mapOf(
                    "sum" to orderSum,
                    "paymentType" to paymentType,
                    "isProcessedExternally" to true,
                    "isPreliminary" to false)
            orderRequest = mapOf(
                    "organization" to IikoAPI._orgID,
                    "customer" to customerOrder,
                    "order" to order,
                    "paymentItems" to paymentItems)
        } else {
            orderRequest = mapOf(
                    "organization" to IikoAPI._orgID,
                    "customer" to customerOrder,
                    "order" to order)
        }
        return orderRequest
    }

    private fun convertOrderToString(): String {
        var result = "Заказ:\n"
        result += itemsAsString
        result += "Стоимость доставки: $deliveryPrice\n"
        result += "Сумма: $orderSum\n"
        result += "Количество персон: $personQuantity\n"
        result += "Способ оплаты: $paymentMethodComment "
        if (cashBackComment != "")
            result += ", $cashBackComment"
        return result
    }

    private fun convertRestAddressToString(addressMap: Map<String, String>): String {
        var result = "Ресторан: "
        if (addressMap.containsKey("city")) {
            result += addressMap["city"] + ", "
        }
        if (addressMap.containsKey("street")) {
            result += "ул. " + addressMap["street"] + ", "
        }
        if (addressMap.containsKey("home")) {
            result += "д. " + addressMap["home"]
        }
        return result
    }

    private fun getTimeNow(): String {
        val timeNow = Calendar.getInstance()
        val hourNow = timeNow.get(Calendar.HOUR_OF_DAY)
        val minNow = timeNow.get(Calendar.MINUTE)
        if (minNow < 10) {
            return "$hourNow:0$minNow"
        } else {
            return "$hourNow:$minNow"
        }
    }

    private fun sendOrderToServer() {
        try {
            val sApi = ServerApi()
            val phone = App.userManager.getClearedPhone()
            val city = CitiesInfo.instance.getSelectedCity()
            sApi.sendOrder(phone, city, orderItemsForServer, deliveryPrice.toString(), orderSum.toString(), object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Log.d(logtag, "PaymentMethodActivity orderCallback onFailure")
                    e.printStackTrace()
                }

                override fun onResponse(call: Call, response: Response) {
                    val responseData = response.body?.string() ?: ""
                    Log.d(logtag, "PaymentMethodActivity orderCallback onResponse responseData: $responseData")
                    App.userManager.reloadHistory()
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun openOrderCompleteActivity() {
        Log.d(logtag, "PaymentMethodActivity openOrderCompleteActivity()")
        val intent = Intent(this, OrderCompleteActivity::class.java)
        startActivity(intent)
    }

    private fun loadCustomer() {
        customerCity = CitiesInfo.instance.getSelectedCity()
        customerName = App.userManager.getName()
        customerPhone = App.userManager.getPhone()
    }

    private fun loadOrderDetails() {
        isSelfService = prefs.getBoolean("isSelfService", false)
        personQuantity = prefs.getInt("personQuantity", 0)
        userComment = prefs.getString("userComment", "") ?: ""
        deliveryPrice = prefs.getInt("deliveryPrice", 0)
        urlicoName = prefs.getString("urlicoName", "") ?: ""
        urlicoOfficialName = prefs.getString("urlicoOfficialName", "") ?: ""

        val restaurantJson = prefs.getString("selectedRest", null)
        if (restaurantJson != null) {
            val moshi = Moshi.Builder().build()
            val type = Types.newParameterizedType(Restaurant::class.java)
            val adapter = moshi.adapter<Restaurant>(type)
            selectedRest = adapter.fromJson(restaurantJson)
        }

        val addressJson = prefs.getString("selectedAddress", null)
        if (addressJson != null) {
            val moshi = Moshi.Builder().build()
            val type = Types.newParameterizedType(Address::class.java)
            val adapter = moshi.adapter<Address>(type)
            selectedAddress = adapter.fromJson(addressJson)
        }
    }
/*
    //tod отключить локальное сохранение истории после успешого подключения сохранения/загрузки истории на/с сервер(а)
    private fun saveOrderHistory() {
        val moshi = Moshi.Builder().build()
        val type = Types.newParameterizedType(List::class.java, OrderData::class.java)
        val adapter = moshi.adapter<List<OrderData>>(type)
        val jsonOrderHistory = adapter.toJson(orderHistory)
        val prefEditor = prefs.edit()
        prefEditor.putString("orderHistory", jsonOrderHistory)
        prefEditor.apply()
    }*/

    private fun loadSelectedPaymentMethod() {
        selectedPaymentMethodIndex = prefs.getInt("paymentMethod", 0)
        selectedPaymentMethod()
    }

    private fun selectedPaymentMethod() {
        for (rb in paymentMethodList) {
            rb.isChecked = false
        }
        paymentMethodList[selectedPaymentMethodIndex].isChecked = true
    }

    private fun saveSelectedPaymentMethod() {
        val prefEditor = prefs.edit()
        prefEditor.putInt("paymentMethod", selectedPaymentMethodIndex)
        prefEditor.apply()
    }

    private fun possiblyShowGooglePayButton() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            googlePayRadioButton.visibility = View.GONE
            return
        }

        val isReadyToPayJson = GooglePay.getIsReadyToPayRequest()
        if (isReadyToPayJson == null || !isReadyToPayJson.isPresent) {
            googlePayRadioButton.visibility = View.GONE
            return
        }
        val request = IsReadyToPayRequest.fromJson(isReadyToPayJson.get().toString())
        if (request == null) {
            googlePayRadioButton.visibility = View.GONE
            return
        }
        val task = mPaymentsClient.isReadyToPay(request)
        task.addOnCompleteListener {
            try {
                val result = task.getResult(ApiException::class.java)
                if (result != null && result) {
                    googlePayRadioButton.visibility = View.VISIBLE
                }
            } catch (exception: ApiException) {
                exception.printStackTrace()
                googlePayRadioButton.visibility = View.GONE
            }
        }
    }

    private fun requestPaymentWithGooglePay() {
        Log.d(logtag, "PaymentMethodActivity requestPaymentWithGooglePay()")
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            return
        }
        val transactionInfo = JSONObject()
        //код для проверки google pay
        //transactionInfo.put("totalPrice", "1")
        payedSum = orderSum
        transactionInfo.put("totalPrice", payedSum.toString())
        transactionInfo.put("totalPriceStatus", "FINAL")
        transactionInfo.put("currencyCode", "RUB")
        val urlicoLogin = SberAcquiring.getLogin(urlicoName)
        val paymentDataRequestJson = GooglePay.getPaymentDataRequest(transactionInfo, urlicoLogin, urlicoOfficialName)
        if (paymentDataRequestJson == null || !paymentDataRequestJson.isPresent) {
            return
        }
        val request = PaymentDataRequest.fromJson(paymentDataRequestJson.get().toString())
        if (request != null) {
            AutoResolveHelper.resolveTask(mPaymentsClient.loadPaymentData(request), this, LOAD_PAYMENT_DATA_REQUEST_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(logtag, "PaymentMethodActivity onActivityResult(), requestCode: " + requestCode + ", resultCode: " + resultCode)

        if (LOAD_PAYMENT_DATA_REQUEST_CODE == requestCode) {
            if (Activity.RESULT_OK == resultCode && data != null) {
                val paymentData = PaymentData.getFromIntent(data)
                val json = paymentData?.toJson()
                // if using gateway tokenization, pass this token without modification
                val paymentMethodData = JSONObject(json)
                        .getJSONObject("paymentMethodData")
                paymentToken = paymentMethodData
                        .getJSONObject("tokenizationData")
                        .getString("token")

                doAsync {
                    val sber = SberAcquiring()
                    generatedOrderNumber = generateOrderNumber()
                    sber.requestPaymentWithGooglePay(orderSum, paymentToken, urlicoName, generatedOrderNumber, callback)
                }
            } else if (AutoResolveHelper.RESULT_ERROR == resultCode) {
                val status = AutoResolveHelper.getStatusFromIntent(data)
                checkoutBtn.isEnabled = true
                showPayFailedDialog("")
            }
        }

        if (PAY_WITH_SBER_REQUEST_CODE == requestCode && Activity.RESULT_OK == resultCode && data != null) {
            val status = data?.extras?.getString("pay_status") ?: ""
            if (status == "success") {
                postOrder(true)
            } else {
                showPayFailedDialog("")
            }
        }
    }

    private fun showPayFailedDialog(error: String) {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        builder.setTitle("Ошибка оплаты")
        var message = "Во время оплаты произошла ошибка"
        if (error.isNotEmpty()) {
            message += ": $error"
        }
        builder.setMessage(message)
        builder.setPositiveButton("ОК") { dialog, _ -> dialog.dismiss() }
        builder.show()
    }

    private fun sberCheckout() {
        doAsync {
            val sber = SberAcquiring()
            payedSum = orderSum
            generatedOrderNumber = generateOrderNumber()
            sber.requestPayment(urlicoName, payedSum, generatedOrderNumber, callback)
        }
    }

    private val callback = object : SberAcquiring.SberCallback {
        override fun onSuccess(orderId: String) {
            sberOrderId = orderId
            postOrder(true)
        }

        override fun onSuccess(orderId: String, responseUrl: String) {
            sberOrderId = orderId
            openWebView(responseUrl)
        }

        override fun onFail(error: String) {
            showPayFailedDialog(error)
        }
    }

    private fun openWebView(url: String) {
        val intent = Intent(this, WebViewActivity::class.java)
        intent.putExtra("url", url)
        startActivityForResult(intent, PAY_WITH_SBER_REQUEST_CODE)
    }

    private fun generateOrderNumber(): String {
        val allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz1234567890"
        return (1..10)
                .map { allowedChars.random() }
                .joinToString("")
    }

}
