package ru.totopizza.to_to

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Canvas
import android.os.Bundle
import android.os.Handler
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.jakewharton.processphoenix.ProcessPhoenix
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.picasso.Picasso
import me.relex.circleindicator.CircleIndicator
import ru.totopizza.to_to.business.CitiesInfo
import java.util.*
import kotlin.concurrent.timerTask

class MainFragment : Fragment() {

    private var indicator: CircleIndicator? = null
    private lateinit var promoPager: ViewPager
    private lateinit var promoAdapter: ImageFragmentStatePagerAdapter
    private lateinit var cityBtn: Button
    private lateinit var selectCityIcon: ImageView

    private var timer: Timer? = null
    private var page: Int = 0

    private lateinit var categoryTabs: TabLayout
    private lateinit var categoryRecyclerView: RecyclerView
    private lateinit var categoryAdapter: CategoryAdapter
    private var productList = ArrayList<Product>()
    private var favoriteProductList = ArrayList<String>()
    private var tabInfoList = ArrayList<TabInfo>()

    private lateinit var prefs: SharedPreferences

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_main_app_bar, container, false)
        Log.d("logtag", "MainFragment onCreateView()")
        if (activity != null) {
            prefs = activity!!.getSharedPreferences("TTAppPrefs", AppCompatActivity.MODE_PRIVATE)
        }
        promoPager = view.findViewById(R.id.viewPager)
        categoryTabs = view.findViewById(R.id.categoryTabLayout)
        categoryRecyclerView = view.findViewById(R.id.categoryRecyclerView)
        categoryRecyclerView.layoutManager = LinearLayoutManager(context)
        loadData()
        val baseActivity = activity as BaseActivity
        categoryAdapter = CategoryAdapter(baseActivity, productList, favoriteProductList.count()) { item ->
            val detailActivity = Intent(context, ProductActivity::class.java)
            detailActivity.putExtra("id", item.id)
            activity?.runOnUiThread { startActivity(detailActivity) }
        }
        categoryRecyclerView.adapter = categoryAdapter
        categoryRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                val firstPosition = layoutManager.findFirstVisibleItemPosition()
                var tabIndex = 0

                for (i in 1 until tabInfoList.count()) {
                    val startGroupPosition = tabInfoList[i].index
                    if (firstPosition < startGroupPosition) {
                        tabIndex = i - 1
                        break
                    }
                }
                val tabView = categoryTabs.getTabAt(tabIndex)
                categoryTabs.removeOnTabSelectedListener(tabListener)
                tabView?.select()
                categoryTabs.addOnTabSelectedListener(tabListener)
            }
        })

        val prefs = activity?.getSharedPreferences("TTAppPrefs", AppCompatActivity.MODE_PRIVATE)
        val myCity = prefs?.getString("myCity", "")

        cityBtn = view.findViewById(R.id.city_main)
        cityBtn.text = myCity
        cityBtn.setOnClickListener {
            selectCity()
        }
        selectCityIcon = view.findViewById(R.id.select_icon_main)
        selectCityIcon.setOnClickListener {
            selectCity()
        }

        val profileBtn = view.findViewById<ImageButton>(R.id.profile_main)
        profileBtn.setOnClickListener {
            onCLickAccount()
        }
        return view
    }


    private fun loadData() {
        productList.clear()
        tabInfoList.clear()

        loadFavorites()
        if (favoriteProductList.count() > 0) {
            tabInfoList.add(TabInfo(0, "Избранное"))
            for (favoriteProductId in favoriteProductList) {
                val favProduct = IikoAPI.getProduct(favoriteProductId)
                if (favProduct != null)
                    productList.add(favProduct)
            }
        }
        for (group in IikoAPI.groups) {
            tabInfoList.add(TabInfo(productList.count(), group.name))
            val catProducts = IikoAPI.getProducts(group.id)
            for (product in catProducts) {
                productList.add(product)
            }
        }
        fillTabs()
    }

    private fun fillTabs() {
        categoryTabs.removeAllTabs()
        for (tabInfo in tabInfoList) {
            val newTab = categoryTabs.newTab()
            newTab.text = tabInfo.name
            categoryTabs.addTab(newTab)
        }
        if (categoryTabs.tabCount == 0)
            return

        categoryTabs.getTabAt(0)?.select()
        categoryTabs.addOnTabSelectedListener(tabListener)
    }

    private val tabListener = object : TabLayout.OnTabSelectedListener {
        private var lastSelectedTab = 0
        override fun onTabUnselected(tab: TabLayout.Tab?) {
        }

        override fun onTabReselected(tab: TabLayout.Tab?) {
        }

        override fun onTabSelected(tab: TabLayout.Tab?) {
            val position = tab?.position ?: 0
            val tabInfo = tabInfoList[position]
            val layoutManager = categoryRecyclerView.layoutManager as LinearLayoutManager
            if (lastSelectedTab < position) {
                val index2 = tabInfo.index + 3
                if (categoryAdapter.itemCount > index2) {
                    layoutManager.scrollToPosition(tabInfo.index + 3)
                }
            } else {
                layoutManager.scrollToPosition(tabInfo.index)
            }
            lastSelectedTab = position
        }
    }

    private fun loadFavorites() {
        val addressListJson = prefs.getString("favoriteProductList", null)
        if (addressListJson != null) {
            val moshi = Moshi.Builder().build()
            val type = Types.newParameterizedType(List::class.java, String::class.java)
            val adapter = moshi.adapter<List<String>>(type)
            val result = adapter.fromJson(addressListJson)
            if (result != null) {
                favoriteProductList.clear()
                val loadedList = ArrayList(result)
                favoriteProductList.addAll(loadedList)
            }
        }
    }

    private fun onCLickAccount() {
        val user = App.userManager
        if (user.isLoggedIn() && user.getPhone() != "") {
            val intent = Intent(activity, AccountActivity::class.java)
            activity?.startActivity(intent)
        } else {
            val intent = Intent(activity, RegistrationActivity::class.java)
            intent.putExtra("isCheckout", false)
            //intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            activity?.startActivity(intent)
        }
    }

    private fun selectCity() {
        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle("Выберите город доставки")
        builder.setItems(CitiesInfo.availableCities) { _, i ->
            showRestartDialog(i)
        }
        builder.setNegativeButton("Отмена", null)
        builder.show()
    }

    private fun showRestartDialog(selectedCityIndex: Int) {
        val builder = AlertDialog.Builder(context!!)
        builder.setCancelable(false)
        builder.setTitle("Смена региона")
        builder.setMessage("Приложение будет перезапущено для обновления меню")
        builder.setPositiveButton("ОК") { _, _ -> saveAndRestart(selectedCityIndex) }
        builder.setNegativeButton("Отмена") { dialog, _ -> dialog.dismiss() }
        builder.show()
    }

    private fun saveAndRestart(selectedCityIndex: Int) {
        val selectedCityId = CitiesInfo.citiesIds[selectedCityIndex]
        CitiesInfo.instance.setSelectedCity(selectedCityId, context!!)
        App.addressManager.setCityId(selectedCityId)
        Handler().postDelayed({
            ProcessPhoenix.triggerRebirth(context)
        }, 200)
    }

    internal fun setCityToToolbar(cityName: String) {
        activity?.runOnUiThread { cityBtn.text = cityName }
    }

    fun updateBanners() {
        if (fragmentManager == null)
            return

        val selectedCityId = CitiesInfo.instance.getSelectedCityId()
        val data = IikoAPI.banners?.filter { it.city == selectedCityId || it.city == "allcity" }

        var i = 0
        if (data != null && data.count() > 0) {
            promoAdapter = ImageFragmentStatePagerAdapter(fragmentManager!!)
            for ((image, title, description) in data) {
                if (image != "") {
                    createImageFragment(image, title, description)
                    i++
                }
            }
            activity?.runOnUiThread {
                promoPager.adapter = promoAdapter
                promoPager.let { indicator?.setViewPager(it) }
                promoPager.adapter?.notifyDataSetChanged()
            }

            if (timer == null) {
                timer = Timer()
                timer?.schedule(timerTask {
                    activity?.runOnUiThread {
                        page = promoPager?.currentItem!!
                        val count = promoPager?.adapter?.count
                        if (count != null) {
                            if (page < count - 1) {
                                page++
                            } else {
                                page = 0
                            }
                            promoPager?.setCurrentItem(page, true)
                            promoPager?.adapter?.notifyDataSetChanged()
                        }
                    }
                }, 0, 3000)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val favoriteChanged = checkFavoriteStatus()
        if (favoriteChanged) {
            loadData()
            categoryAdapter.updateDataset(productList)
        }
    }

    fun updateCategoryList() {
        loadData()
        categoryAdapter.updateDataset(productList)
    }

    private fun checkFavoriteStatus(): Boolean {
        val isChanged = prefs.getBoolean("favoriteChanged", false)
        if (isChanged) {
            val prefEditor = prefs.edit()
            prefEditor?.putBoolean("favoriteChanged", false)
            prefEditor?.apply()
            return true
        }
        return false
    }

    private fun createImageFragment(url: String, title: String, description: String) {
        promoAdapter.addFragmentView { arg1, arg2, _ ->
            val view = arg1?.inflate(R.layout.image_with_descryption, arg2, false)
            val imageView = view?.findViewById<ImageView>(R.id.image)

            if (url != "") {
                Picasso.get()
                        .load(url)
                        .placeholder(R.drawable.placeholder)
                        .into(imageView)
            } else {
                Picasso.get()
                        .load(R.drawable.placeholder)
                        .into(imageView)
            }

            imageView?.setOnClickListener {
                val detailActivity = Intent(activity, PromoActivity::class.java)
                detailActivity.putExtra("url", url)
                detailActivity.putExtra("title", title)
                detailActivity.putExtra("description", description)
                activity!!.startActivity(detailActivity)
            }

            val titleView = view?.findViewById<TextView>(R.id.title_cart)
            titleView?.text = title

            view!!
        }
    }

    fun scrollToStart() {
        categoryRecyclerView.layoutManager?.scrollToPosition(0)
    }
}

@SuppressLint("ValidFragment")
class ImageFragment : Fragment() {
    private var viewCallback: ((LayoutInflater?, ViewGroup?, Bundle?) -> View)? = null

    fun setCallback(callback: (LayoutInflater?, ViewGroup?, Bundle?) -> View) {
        viewCallback = callback
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = viewCallback?.invoke(inflater, container, savedInstanceState)
        return view
    }
}

class ImageFragmentStatePagerAdapter(getFragmentManager: FragmentManager) : FragmentStatePagerAdapter(getFragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT), ViewPager.PageTransformer {
    private var fragmentList: ArrayList<Fragment> = ArrayList()

    override fun getCount(): Int {
        return fragmentList.count()
    }

    override fun getItem(position: Int): Fragment {
        return fragmentList[position]
    }

    fun addFragmentView(callback: (LayoutInflater?, ViewGroup?, Bundle?) -> View) {
        val imageFragment = ImageFragment()
        imageFragment.setCallback(callback)
        fragmentList.add(imageFragment)
    }

    override fun transformPage(page: View, position: Float) {
        val myLinearLayout = page.findViewById<ScalableCardView>(R.id.card_view)
        var scale = BIG_SCALE
        if (position > 0)
            scale -= position * DIFF_SCALE
        else
            scale += position * DIFF_SCALE
        if (scale < 0) scale = 0f
        myLinearLayout.setScaleBoth(scale)
    }

    companion object {
        val BIG_SCALE = 1.0f
        val SMALL_SCALE = 0.7f
        val DIFF_SCALE = BIG_SCALE - SMALL_SCALE
    }
}

class ScalableCardView : androidx.cardview.widget.CardView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet)

    private var scale = ImageFragmentStatePagerAdapter.BIG_SCALE

    fun setScaleBoth(scale: Float) {
        this.scale = scale
        this.invalidate()    // If you want to see the scale every time you set
        // scale you need to have this line here,
        // invalidate() function will call onDraw(Canvas)
        // to redraw the view for you
    }

    override fun onDraw(canvas: Canvas) {
        // The main mechanism to display scale animation, you can customize it
        // as your needs
        val w = this.width
        val h = this.height
        canvas.scale(scale, scale, (w / 2).toFloat(), (h / 2).toFloat())

        super.onDraw(canvas)
    }
}

data class TabInfo(var index: Int, var name: String)