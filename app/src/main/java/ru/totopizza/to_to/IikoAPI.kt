package ru.totopizza.to_to

import android.content.Context
import android.content.SharedPreferences
import android.location.Location
import org.json.JSONException
import java.io.*
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import ru.totopizza.to_to.business.WorkTimeScheme
import ru.totopizza.to_to.business.CitiesInfo
import ru.totopizza.to_to.business.PRODUCTITEM
import kotlin.collections.ArrayList
import java.nio.charset.Charset
import kotlin.collections.HashMap
import java.text.DecimalFormat

/**
 * Created by Enico on 06/07/2017.
 */
class IikoAPI {
    companion object Factory {
        private val client = OkHttpClient()

        var _token: String = ""
        var _orgID: String = ""

        var banners: List<Banner>? = null
        var nomenclature: Nomenclature? = null

        var cart = HashMap<String, CartItem>()

        var onGoToCart: (() -> Unit)? = null

        fun addTocart(pid: String, size: String? = null, piquancy: String? = null): Int {
            val id = pid + (if (size != null) size else "") + (if (piquancy != null) piquancy else "")
            if (cart.containsKey(id)) {
                cart[id]?.quantity = cart[id]?.quantity!! + 1
            }
            else cart.set(id, CartItem(pid,1, size, piquancy))
            saveCart()
            return cart[id]?.quantity!!
        }

        fun addTocart(pid: String, size: String? = null, quantity: Int = 1): Int {
            val id = pid + (if (size != null) size else "")
            if (cart.containsKey(id)) {
                cart[id]?.quantity = cart[id]?.quantity!! + quantity
            }
            else cart.set(id, CartItem(pid, quantity, size, null))
            saveCart()
            return cart[id]?.quantity!!
        }

        fun removeFromCart(id: String, size: String? = null, piquancy: String? = null): Int {
            val id = id + (if (size != null) size else "") + (if (piquancy != null) piquancy else "")
            if (cart.containsKey(id)) {
                cart[id]?.quantity = cart[id]?.quantity!! - 1
                if (cart[id]?.quantity!! <= 0) {
                    cart.remove(id)
                    saveCart()
                    return 0
                } else {
                    saveCart()
                    return cart[id]?.quantity!!
                }
            }
            else return 0
        }

        fun addToCartFromHistory(products: List<PRODUCTITEM>) {
            for (historyProduct in products) {
                val nomenclatureProduct = getProductByName(historyProduct.NAME)
                if (nomenclatureProduct != null) {
                    addTocart(nomenclatureProduct.id, null, historyProduct.QUANTITY)
                }
            }
        }

        private var prefs: SharedPreferences? = null

        fun loadCart(prefs: SharedPreferences) {
            this.prefs = prefs

            val customerJson = prefs.getString("cart", null)
            if (customerJson != null && customerJson.isNotEmpty()) {
                val moshi = Moshi.Builder().build()
                val type = Types.newParameterizedType(Map::class.java, String::class.java, CartItem::class.java)
                val adapter = moshi.adapter<Map<String, CartItem>>(type)
                val savedCart = adapter.fromJson(customerJson)
                if (savedCart != null) {
                    cart.clear()
                    for (item in savedCart) {
                        val key = item.key
                        val value = item.value
                        cart[key] = value
                    }
                }
            }
        }

        private fun saveCart() {
            val prefEditor = prefs?.edit()
            if (cart.count() > 0) {
                val moshi = Moshi.Builder().build()
                val type = Types.newParameterizedType(Map::class.java, String::class.java, CartItem::class.java)
                val adapter = moshi.adapter<Map<String, CartItem>>(type)
                val jsonCart = adapter.toJson(cart)
                prefEditor?.putString("cart", jsonCart)
            } else {
                prefEditor?.putString("cart", "")
            }
            prefEditor?.apply()
        }

        fun getCartCount(id: String, size: String? = null, piquancy: String? = null): Int {
            val id = id + (if (size != null) size else "") + (if (piquancy != null) piquancy else "")
            if (cart.containsKey(id)) {
                return cart[id]?.quantity!!
            }
            else return 0
        }

        fun getCartCount(): Int {
            val items = cart.values.toList()
            if (items.count() <= 0) return 0
            val count = items.sumBy { it.quantity }
            return count
        }

        fun getCartPrice(): Int {
            var orderSum = 0
            for (item in cart)
            {
                val product = IikoAPI.getProduct(item.value.id)
                if (product != null) {
                    var sum = 0
                    if (item.value.size != null) {
                        val size = IikoAPI.getProduct(item.value.size!!)
                        if (size != null) {
                            sum = size.price * item.value.quantity
                        }
                    } else sum = product.price * item.value.quantity
                    orderSum += sum
                }
            }
            return orderSum
        }

        val groups: List<Group>
        get() {
            if (nomenclature != null) {

                val filtered = nomenclature?.groups!!.filter { group -> group.isIncludedInMenu && !group.name.contains("[HIDE]", false)}
                val sorted = filtered.sortedBy { group -> group.order }
                return sorted
            } else {
                return ArrayList<Group>().toList()
            }
        }

        fun clearCart() {
            cart.clear()
            saveCart()
        }

        val isOnlyPizzaInCart: Boolean
        get() {
            val pizzaCat = nomenclature?.groups?.find { it.name.contains("пицца", true) }
            val nonPizza = cart.filter {
                val p = getProduct(id = it.value.id)
                p?.parentGroup != pizzaCat?.id
            }
            return nonPizza.count() <= 0
        }

        fun getRecomendedProducts() : ArrayList<Product> {
            if (nomenclature != null) {
                return ArrayList<Product>(nomenclature?.products!!.filter {
                    item -> item.isIncludedInMenu && item.tags != null && item.tags.contains("recomend") && item.type == "dish"
                }.sortedBy {
                    item -> item.order
                })
            } else {
                return ArrayList<Product>()
            }
        }

        fun getProducts(forCatetoryId: String?) : ArrayList<Product> {
            if (nomenclature != null && !forCatetoryId.isNullOrEmpty()) {
                return ArrayList<Product>(nomenclature?.products!!.filter { item -> item.isIncludedInMenu && item.parentGroup == forCatetoryId && item.type == "dish" }.sortedBy { item -> item.order })
            } else {
                return ArrayList<Product>()
            }
        }

        fun getProduct(id: String) : Product? {
            if (nomenclature != null) {
                return nomenclature?.products!!.find { product -> product.id == id }
            } else {
                return null
            }
        }

        // тест пицца api
        fun getProductByName(name: String) : Product? {
            if (nomenclature != null) {
                return nomenclature?.products!!.find { product -> product.name == name }
            } else {
                return null
            }
        }

        fun getProductModifier(id: String) : Product? {
            if (nomenclature != null) {
                return nomenclature?.products!!.find { item -> item.id == id && item.type == "modifier" }
            } else {
                return null
            }
        }

        fun getGroup(id: String) : Group? {
            if (nomenclature != null) {
                return nomenclature?.groups!!.find { item -> item.id == id }
            } else {
                return null
            }
        }

//        val info = "This is factory"
//        fun getMoreInfo():String { return "This is factory fun" }

        fun getBanners(callback: () -> Unit) {
            val urlBuider = HttpUrl.Builder()
                    .scheme("http")
                    .host("action.totopizza.ru")
                    .encodedPath("/action.json")

            val thisBlock = this
            val request = Request.Builder().url(urlBuider.build()).build()
            client.newCall(request).enqueue(object : okhttp3.Callback {
                override fun onFailure(call: okhttp3.Call, e: IOException) {
                    e.printStackTrace()
                }

                @Throws(IOException::class)
                override fun onResponse(call: Call, response: Response) {
                    val result = response.body?.string()
                    if (result != null) {
                        try {
                            val moshi = Moshi.Builder().build()
                            val type = Types.newParameterizedType(List::class.java, Banner::class.java)
                            val adapter = moshi.adapter<List<Banner>>(type)
                            val banners = adapter.fromJson(result)

                            if (banners != null) {
                                thisBlock.banners = banners
                                callback()
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                    }
                }
            })
        }

        fun getToken(callback: (String) -> Unit) {

            val keys = mapOf(
                    "vladimir" to mapOf(
                            "user_id" to "ApiToTo",
                            "user_secret" to "dfgbnmuio99"
                    )
            )

            val user = keys["vladimir"]

            if (user != null) requestTo("/api/0/auth/access_token", user) { result ->
                try {
                    val moshi = Moshi.Builder().build()
                    val jsonAdapter = moshi.adapter<String>(String::class.java)

                    val token = jsonAdapter.fromJson(result)

                    if (token != null) {
                        _token = token
                        callback(_token)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }

        private var paymentTypes: PaymentTypes? = null
        fun getPaymentTypes(token: String, orgID: String) {
            val query = mapOf(
                    "access_token" to token,
                    "organization" to orgID
            )

            requestTo("/api/0/rmsSettings/getPaymentTypes", query) { data ->
                try {
                    val moshi = Moshi.Builder().build()
                    val type = Types.newParameterizedType(PaymentTypes::class.java)
                    val adapter = moshi.adapter<PaymentTypes>(type)
                    paymentTypes = adapter.fromJson(data)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }

        fun getPaymentTypeCodeForCash() : String {
            if (paymentTypes == null || paymentTypes!!.paymentTypes.isNullOrEmpty()){
                return ""
            }
            for (type in paymentTypes!!.paymentTypes) {
                if (type.name.toLowerCase() == "наличные"){
                    return type.code
                }
            }
            return ""
        }

        fun getPaymentTypeCodeForCard() : String {
            if (paymentTypes == null || paymentTypes!!.paymentTypes.isNullOrEmpty()){
                return ""
            }
            for (type in paymentTypes!!.paymentTypes) {
                if (type.name.toLowerCase() == "банковская карта"){
                    return type.code
                }
            }
            return ""
        }

        fun getPaymentTypeCodeForOnlinePM() : String {
            if (paymentTypes == null || paymentTypes!!.paymentTypes.isNullOrEmpty()){
                return ""
            }
            for (type in paymentTypes!!.paymentTypes) {
                if (type.name.toLowerCase() == "приложение"){
                    return type.code
                }
            }
            return ""
        }

        fun getOrganisationList(token: String, callback: (String) -> Unit) {

            val query = mapOf(
                    "request_timeout" to "00:00:10",
                    "access_token" to token
            )

            requestTo("/api/0/organization/list", query) { data ->
                try {
                    val moshi = Moshi.Builder().build()
                    val jsonAdapter = moshi.adapter<List<Map<String,Any>>>(List::class.java)
                    val result = jsonAdapter.fromJson(data)
                    if (result != null) {
                        _orgID = result[0]["id"] as String
                        callback(_orgID)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }

        fun getNomenclature(token: String, orgID: String, callback: (String) -> Unit) {
            if (this.nomenclature != null) {
                callback("Products : ${nomenclature?.products?.size}")
                return
            }
            val query = mapOf(
                    "access_token" to token
            )

            requestTo("/api/0/nomenclature/$orgID", query) { data ->
                try {
                    val moshi = Moshi.Builder()
    //                            .add(KotlinJsonAdapterFactory())
    //                            .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
                            .build()
                    val nomenclatureAdapter = moshi.adapter(Nomenclature::class.java)
                    val nomenclature = nomenclatureAdapter.fromJson(data)

                    if (nomenclature != null) {
                        this.nomenclature = nomenclature
                        callback("Products : ${nomenclature.products.size}")
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }

        private var citesAndStreets: List<CitiesStreets>? = null

        fun getCitiesAndStreets(token: String, orgID: String, callback: (String) -> Unit) {
            val query = mapOf(
                    "access_token" to token,
                    "organization" to orgID
            )

            requestTo("/api/0/cities/cities", query) { data ->
                try {
                    val moshi = Moshi.Builder().build()
                    val type = Types.newParameterizedType(List::class.java, CitiesStreets::class.java)
                    val adapter = moshi.adapter<List<CitiesStreets>>(type)

                    citesAndStreets = adapter.fromJson(data)

                    if (citesAndStreets != null) {
                        callback("Cities : ${citesAndStreets?.size}")
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }

        fun getCitiesAndSuburb(cityName: String): List<String> {
            val resultList = ArrayList<String>()
            if (citesAndStreets == null || cityName.isNullOrEmpty())
                return  resultList
            for (streetsInCity in citesAndStreets!!) {
                val cityNameLc = streetsInCity.city.name.toLowerCase()
                if (cityNameLc == cityName.toLowerCase() ||
                        (cityNameLc.startsWith("микрорайон") && cityNameLc.contains(cityName.toLowerCase()))){
                    resultList.add(streetsInCity.city.name)
                }
            }
            val areaName = CitiesInfo.instance.getSelectedCityAreaName()
            for (streetsInCity in citesAndStreets!!) {
                val cityNameLc = streetsInCity.city.name.toLowerCase()
                if (cityNameLc.contains(areaName, true)){
                    resultList.add(streetsInCity.city.name)
                }
            }
            return resultList
        }

        fun getStreetsInSuburban(cityName: String): Array<String> {
            val allStreets = ArrayList<Street>()
            if (citesAndStreets != null) {
                for (streetsInCity in citesAndStreets!!) {
                    if (streetsInCity.city.name.toLowerCase() == cityName.toLowerCase()) {
                        allStreets.addAll(streetsInCity.streets)
                    }
                }
            }
            val resultList = ArrayList<String>()
            if (allStreets.count() > 0) {
                for (street in allStreets) {
                    if (!resultList.contains(street.name))
                        resultList.add(street.name)
                }
            }
            return resultList.toTypedArray()
        }

        fun getCityIdByCityName(cityName: String) : String {
            if (citesAndStreets.isNullOrEmpty())
                return ""
            for (cities in citesAndStreets!!) {
                if (cities.city.name == cityName) {
                    return cities.city.id
                }
            }
            return ""
        }

        fun getStreetIdByStreet(cityId: String, streetName: String) : String {
            if (citesAndStreets.isNullOrEmpty())
                return ""
            for (cities in citesAndStreets!!) {
                if (cities.city.id == cityId) {
                    val streetId = findStreetInCity(cities, streetName)
                    return streetId
                }
            }
            return ""
        }

        private fun findStreetInCity(city: CitiesStreets, streetName: String): String {
            if (city.streets.isNullOrEmpty())
                return ""
            for (street in city.streets) {
                if (street.name.toLowerCase() == streetName.toLowerCase()) {
                    return street.id
                }
            }
            return ""
        }

        fun postOrder(requestData: Map<String, Any>, callback: (String) -> Unit) {
            getToken { token ->
                println("Token: $token")
                _token = token

                val query = mapOf(
                        "access_token" to _token
                )

                val moshi = Moshi.Builder().build()
                val type = Types.newParameterizedType(Map::class.java, String::class.java, Any::class.java)
                val adapter = moshi.adapter<Map<String, Any>>(type)
                val data = adapter.toJson(requestData)

                postTo("/api/0/orders/add", query, data) { data ->
                    callback(data)
                }
            }
        }

        private fun requestTo(path:String, params:Map<String,String>, callback: (String) -> Unit) {
            val urlBuider = HttpUrl.Builder()
                    .scheme("https")
                    .host("iiko.biz")
                    .port(9900)
                    .encodedPath(path)

            for (param in params.entries) {
                urlBuider.addQueryParameter(param.key, param.value)
            }
            val request = Request.Builder().url(urlBuider.build()).build()
            client.newCall(request).enqueue(object : okhttp3.Callback {
                override fun onFailure(call: okhttp3.Call, e: IOException) {
                    e.printStackTrace()
                }

                @Throws(IOException::class)
                override fun onResponse(call: Call, response: Response) {
                    val result = response.body?.string()
                    if (result != null) {
                        callback(result)
                    }
                }
            })
        }

        /*fun resetMailFlag() {
            mailSended = false
        }*/

        private fun postTo(path:String,
                           params:Map<String,String>,
                           data:String,
                           callback: (String) -> Unit) {
            val urlBuider = HttpUrl.Builder()
                    .scheme("https")
                    .host("iiko.biz")
                    .port(9900)
                    .encodedPath(path)

            if (params != null) {
                for (param in params.entries) {
                    urlBuider.addQueryParameter(param.key, param.value)
                }
            }

            val body = RequestBody.create("application/json; charset=utf-8".toMediaTypeOrNull(), data)
            val request = Request.Builder().url(urlBuider.build()).post(body).build()

            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                }

                @Throws(IOException::class)
                override fun onResponse(call: Call, response: Response) {
                    val result = response.body?.string()
                    if (result != null) {
                        if (result != null) callback(result)
                    }
                }
            })
        }

        fun loadRestraunts(context: Context): Map<String, List<Restaurant>>? {
            var json = ""
            try {
                val buf = StringBuilder()
                val stream = context.assets.open("restaurants.json")
                val size = stream.available()
                val buffer = ByteArray(size)
                stream.read(buffer)
                stream.close()

                json = String(buffer, Charset.forName("UTF-8"))
            } catch (ex: IOException) {
                ex.printStackTrace()
                return null
            }

            if (json == "") return null

            var restaurants: Map<String, List<Restaurant>>? = null

            try {
                val moshi = Moshi.Builder().build()

                val listType = Types.newParameterizedType(List::class.java, Restaurant::class.java)
                val type = Types.newParameterizedType(Map::class.java, String::class.java, listType)
                val adapter = moshi.adapter<Map<String, List<Restaurant>>>(type)
                restaurants = adapter.fromJson(json)

            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return restaurants
        }

        fun loadDeliveryInfo(context: Context): Map<String, Delivery>? {
            var json = ""
            try {
                val buf = StringBuilder()
                val stream = context.assets.open("regions.json")
                val size = stream.available()
                val buffer = ByteArray(size)
                stream.read(buffer)
                stream.close()

                json = String(buffer, Charset.forName("UTF-8"))
            } catch (ex: IOException) {
                ex.printStackTrace()
                return null
            }

            if (json == "") return null

            var deliferyInfo: Map<String, Delivery>? = null

            try {
                val moshi = Moshi.Builder().build()
                val type = Types.newParameterizedType(Map::class.java, String::class.java, Delivery::class.java)
                val adapter = moshi.adapter<Map<String, Delivery>>(type)
                deliferyInfo = adapter.fromJson(json)

            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return deliferyInfo
        }

        fun loadWorkTime(context: Context) : WorkTimeScheme? {
            var json = ""
            try {
                val buf = StringBuilder()
                val stream = context.assets.open("workTime.json")
                val size = stream.available()
                val buffer = ByteArray(size)
                stream.read(buffer)
                stream.close()

                json = String(buffer, Charset.forName("UTF-8"))
            } catch (ex: IOException) {
                ex.printStackTrace()
                return null
            }

            if (json == "") return null

            var times: Map<String, Map<String, Map<String, String>>>? = null

            try {
                val moshi = Moshi.Builder().build()

                val innerType = Types.newParameterizedType(Map::class.java, String::class.java, String::class.java)
                val middleType = Types.newParameterizedType(Map::class.java, String::class.java, innerType)
                val outType = Types.newParameterizedType(Map::class.java, String::class.java, middleType)
                val adapter = moshi.adapter<Map<String, Map<String, Map<String, String>>>>(outType)
                times = adapter.fromJson(json)

            } catch (e: JSONException) {
                e.printStackTrace()
            }
            if (times != null)
                return WorkTimeScheme(times)
            return null
        }

    }
}

data class Banner(val image: String,
                  val title: String,
                  val description: String,
                  var city: String,
                  val itemid: String)

data class CitiesStreets(val city:City,
                         val streets: List<Street>)

data class City(val name: String,
                val id: String)

data class Street(val name: String,
                  val id: String)

data class Nomenclature(
        val groups: List<Group>,
        val productCategories: List<Category>,
        val products: List<Product>)

data class Group(
        val id: String,
        val name: String,
        val order: Int,
        val description: String,
        val images: List<Image>,
        val isIncludedInMenu: Boolean,
        val isDeleted: Boolean)

data class Category(
        val id: String,
        val name: String)

//data class Product(
//        val id: String,
//        val code: String,
//        val name: String,
//        val order: Int,
//        val price: Int,
//        val weight: Float,
//        val energyAmount: Float,
//        val energyFullAmount: Float,
//        val fatAmount: Float,
//        val fatFullAmount: Float,
//        val fiberAmount: Float,
//        val fiberFullAmount: Float,
//        val carbohydrateAmount: Float,
//        val carbohydrateFullAmount: Float,
//        val measureUnit: String,
//        val tags: List<String>,
//        val type: String,
//        val description: String,
//        val productCategoryId: String,
//        val parentGroup: String,
//        val images: List<Image>,
//        val groupModifiers: List<Modifier>,
//        val modifiers: List<Modifier>,
//        val isIncludedInMenu: Boolean,
//        val isDeleted: Boolean) {
//    init {
//
//    }
//}

data class Image(
        val imageId: String,
        val imageUrl: String)

data class Modifier(
        val modifierId: String,
        val defaultAmount: Int,
        val minAmount: Int,
        val maxAmount: Int,
        val required: Boolean,
        val childModifiers: List<Modifier>)

data class CartItem(val id: String, var quantity: Int, var size: String? = null, var piquancy: String? = null) : Serializable

data class Restaurant (
        val city:String,
        val street: String,
        val address: Map<String, String>,
        val geo: Coords,
        val phone: String,
        val worktime: String
) : Serializable

data class Delivery(val translate : String, val delivery : String, val phone: String, val time: WorkTime )
data class WorkTime(val week : WorkTimeRange, val weekend : WorkTimeRange )
data class WorkTimeRange(val start : Int, val end : Int )

open class Item() {}

class HeaderItem(val title: String):Item() {}

class RestaurantItem(val From: Restaurant, val location: Location?): Item() {

    var distanceInMeters: Float = -1F
    val distance: String
    get() {
        if (distanceInMeters < 0) return ""
        else if (distanceInMeters < 1000) return Math.round(distanceInMeters).toString() + " м"
        else {
            val decim = DecimalFormat("0.#")
            return decim.format(distanceInMeters/1000) + " км"
        }
    }

    init {
        val restLoc = Location("")
        restLoc.latitude= From.geo.latitude
        restLoc.longitude = From.geo.longitude

        if (location != null) distanceInMeters = location.distanceTo(restLoc)
    }

}

data class Coords(val latitude: Double, val longitude: Double) : Serializable

fun Int.toString(suffix: String): String {
//    val format = DecimalFormat.getCurrencyInstance(Locale.getDefault())
//    format.currency = Currency.getInstance("RUB")
//    return format.format(this)
    val decim = DecimalFormat("#,##0.#")
    return decim.format(this) + " " + suffix
}

fun Float.toString(suffix: String): String {
    val decim = DecimalFormat("#,##0.#")
    return decim.format(this) + " " + suffix
}

data class Product(
        val id: String,
        val code: String,
        val name: String,
        val order: Int,
        val price: Int,
        val weight: Float,
        val energyAmount: Float,
        val energyFullAmount: Float,
        val fatAmount: Float,
        val fatFullAmount: Float,
        val fiberAmount: Float,
        val fiberFullAmount: Float,
        val carbohydrateAmount: Float,
        val carbohydrateFullAmount: Float,
        val measureUnit: String,
        val tags: List<String>,
        val type: String,
        val description: String,
        val productCategoryId: String,
        val parentGroup: String,
        val images: List<Image>,
        val groupModifiers: List<Modifier>,
        val modifiers: List<Modifier>,
        val isIncludedInMenu: Boolean,
        val isDeleted: Boolean,
        val additionalInfo: String?) {


    private @Transient var isPizzaSize = false
    private @Transient var isPizzaPiquancy = false

    @Transient var pizzaSizes: java.util.ArrayList<Product> = java.util.ArrayList()
    @Transient var pizzaPiquancy: java.util.ArrayList<Product> = java.util.ArrayList()

    @Transient var selectedSize: String? = null
    @Transient var selectedSize33: String? = null
    @Transient var selectedPiquancy: String? = null

    private var price23: Int = 0
    @Transient var price33: Int = 0
    private var weight23: Float = 0F

    private @Transient var isSetup = false

    fun setup() {
        if (isSetup) return

        pizzaSizes = java.util.ArrayList()
        pizzaPiquancy = java.util.ArrayList()

        if (groupModifiers.count() > 0) {
            for (mod in groupModifiers) {
                val gMod = IikoAPI.getGroup(mod.modifierId)

                if (gMod != null) {
                    isPizzaSize = gMod.name.contains("пицца", true)
//                    isPizzaPiquancy = gMod.name.contains("острота", true)
                }

                if (mod.childModifiers.count() > 0) {
                    for (mod in mod.childModifiers) {
                        val pMod = IikoAPI.getProductModifier(mod.modifierId)

                        if (pMod != null) {
                            if (isPizzaSize) pizzaSizes.add(pMod)
                            else if (isPizzaPiquancy) pizzaPiquancy.add(pMod)
                        }
                    }
                }
            }
        }

        isPizzaSize = pizzaSizes.count() > 0
        isPizzaPiquancy = pizzaPiquancy.count() > 0

        if (isPizzaSize) {
            selectedSize = pizzaSizes.find { it.name.contains("23 см", true) }?.id
            selectedSize33 = pizzaSizes.find { it.name.contains("33 см", true) }?.id

            if (selectedSize != null) {
                val product = IikoAPI.getProduct(selectedSize!!)
                if (product != null) {
                    price23 = product.price
                    weight23 = product.weight
                }
                if (selectedSize33 != null) {
                    val product33 = IikoAPI.getProduct(selectedSize33!!)
                    if (product33 != null) {
                        price33 = product33.price
                    }
                }
            } else {
                isPizzaSize = false
            }
        }
//        if (isPizzaPiquancy) selectedPiquancy = pizzaPiquancy.find { it.name.contains("не острая", true) }?.id

        isSetup = true
    }


    fun calcPrice() : Int {
        setup()
        return if (isPizzaSize && price23 > 0) price23 else price
    }

    fun calcWeight(): Float {
        setup()
        return if (isPizzaSize && weight23 > 0) weight23 else weight
    }
}

data class OrderData(
        var number: Int,
        var date: String,
        var time: String,
        var sum: Int,
        var items: List<ItemData>
)

data class ItemData(
        val id:String,
        var size: String,
        val name:String,
        val price: Int,
        val count: Int
)

data class Address(
        var name: String,
        var city: String,
        var cityId: String,
        var suburban: String,
        var street: String,
        var streetId: String,
        var streetIiko: String,
        var home: String,
        var apartment: String,
        var floor: String,
        var entrance: String,
        var isPrefer: Boolean,
        var entranceCod: String,
        var comment: String,
        var delivery: Int,
        var freeDelivery: Int,
        var urlico: String,
        var addressId: String
) : Serializable

data class PaymentTypes(
    val paymentTypes: List<PaymentType>
)

data class PaymentType(
    val applicableMarketingCampaigns: Any,
    val code: String,
    val combinable: Boolean,
    val comment: String,
    val deleted: Boolean,
    val externalRevision: Int,
    val id: String,
    val name: String
)