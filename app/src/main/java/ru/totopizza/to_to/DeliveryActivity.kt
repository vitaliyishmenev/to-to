package ru.totopizza.to_to

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity

import kotlinx.android.synthetic.main.activity_delivery.*
import kotlinx.android.synthetic.main.content_delivery.*

class DeliveryActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delivery)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val phone = phone
        phone.setOnClickListener {
            this.makeCallOn(phone.text.toString())
        }

    }

}
