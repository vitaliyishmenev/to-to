package ru.totopizza.to_to

import android.content.Intent
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.content_cart.*
import ru.totopizza.to_to.business.CitiesInfo
import ru.totopizza.to_to.business.UserManager
import java.util.*

/**
 * Created by Enico on 12/07/2017.
 */
class OrdersFragment() : Fragment() {

    var items: ArrayList<CartItem> = ArrayList()

    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: CartActivity.RecyclerViewAdapter? = null
    private var mLayoutManager: RecyclerView.LayoutManager? = null

    private var checkoutBtn:Button? = null
    private var emptyView: TextView? = null
    private var clearBtn: FloatingActionButton? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_orders, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        items = ArrayList(IikoAPI.cart.values.toList())
        val rItems = IikoAPI.getRecomendedProducts()

        mRecyclerView = recycler_view
        mRecyclerView?.setHasFixedSize(true)
        mLayoutManager = LinearLayoutManager(activity)
        mRecyclerView?.layoutManager = mLayoutManager
        mAdapter = CartActivity.RecyclerViewAdapter(activity as BaseActivity, items) { product ->
            val detailActivity = Intent(activity, ProductActivity::class.java)
            detailActivity.putExtra("id", product.id)
            activity?.runOnUiThread { startActivity(detailActivity) }
        }
        mAdapter!!.rDataset = rItems
        mRecyclerView?.adapter = mAdapter

        mAdapter?.callback = {
            this.updateCartCount()
        }

        checkoutBtn = activity?.findViewById(R.id.checkout_btn)
        checkoutBtn?.setOnClickListener {
            //проверка заказов через сайт
            //testServerOrder()
            //return@setOnClickListener
            checkout()
        }
        checkoutBtn?.visibility = if(IikoAPI.getCartCount() > 0) View.VISIBLE else View.INVISIBLE
        emptyView = activity?.findViewById(R.id.empty_message_cart)
    }

    private fun checkout() {
        val hours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
        val minutes = Calendar.getInstance().get(Calendar.MINUTE)
        val seconds = Calendar.getInstance().get(Calendar.SECOND)
        val dayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK)
        val time = hours * 60 * 60 + minutes * 60 + seconds

        val myCityId = CitiesInfo.instance.getSelectedCityId()
        val deliveries = IikoAPI.loadDeliveryInfo(this.activity!!)
        if (deliveries != null && myCityId.isNotEmpty()) {
            val delivery = deliveries[myCityId]
            if (delivery != null) {
                if (App.userManager.isLoggedIn()) {
                    val detailActivity = Intent(this.activity, CheckoutActivity::class.java)
                    startActivity(detailActivity)
                    return
                } else {
                    val intent = Intent(this.activity, RegistrationActivity::class.java)
                    intent.putExtra("isCheckout", true)
                    //intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                    startActivity(intent)
                    return
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mAdapter?.mDataset = ArrayList(IikoAPI.cart.values.toList())
        mRecyclerView?.post {  mAdapter?.notifyDataSetChanged() }
        checkoutBtn?.visibility = if(IikoAPI.getCartCount() > 0) View.VISIBLE else View.INVISIBLE
        emptyView?.visibility = if (mAdapter?.mDataset?.count() != 0)
            View.GONE
        else
            View.VISIBLE
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            mAdapter?.mDataset = ArrayList(IikoAPI.cart.values.toList())
            mRecyclerView?.post({  mAdapter?.notifyDataSetChanged() })
        }
    }

    fun updateCartCount() {
        checkoutBtn?.visibility = if(IikoAPI.getCartCount() > 0) View.VISIBLE else View.INVISIBLE
        mAdapter?.mDataset = ArrayList(IikoAPI.cart.values.toList())
        mRecyclerView?.post {  mAdapter?.notifyDataSetChanged() }
        emptyView?.visibility = if (mAdapter?.mDataset?.count() != 0)
            View.GONE
        else
            View.VISIBLE
    }

}

