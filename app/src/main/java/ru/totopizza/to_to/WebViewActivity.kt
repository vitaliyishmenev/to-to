package ru.totopizza.to_to

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient
import ru.totopizza.to_to.checkout.SberAcquiring

class WebViewActivity : AppCompatActivity() {
    private lateinit var webView: WebView
    private var url = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        url = intent.getStringExtra("url") ?: ""

        webView = findViewById(R.id.web_view)
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                Log.d("logtag", "shouldOverrideUrlLoading url: $url")
                if (url != null){
                    if (url.contains(SberAcquiring.successUrl)){
                        setResult("success")
                        return true
                    }else if (url.contains(SberAcquiring.failUrl)) {
                        setResult("fail")
                        return true
                    }
                }
                return false
            }
        }

        webView.settings.javaScriptEnabled = true
    }

    private fun setResult(status: String) {
        val intent = Intent()
        intent.putExtra("pay_status", status)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onResume() {
        super.onResume()
        webView.loadUrl(url)
    }

}
