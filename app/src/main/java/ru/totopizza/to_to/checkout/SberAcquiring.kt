package ru.totopizza.to_to.checkout;

import android.util.Log
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException
import java.net.MalformedURLException
import java.net.URL
import org.json.JSONObject

class SberAcquiring {
    private val login1 = "totopizza_1-api"
    //private val login2 = "totopizza%5f1%2dapi"
    private val password = "XgPicd%IF8N?a)v"
    //private val password2 = "XgPicd%25IF8N%3fa%29v"
    private val client = OkHttpClient()
    //private val testServer = "https://62.76.205.110/"
    private val prodServer = "https://62.76.205.3/"
    private val registerAddress = "https://securepayments.sberbank.ru/payment/rest/register.do"
    private val googlePayAddress = "https://securepayments.sberbank.ru/payment/google/payment.do"
    private val testRegisterAddress = "https://3dsec.sberbank.ru/payment/rest/register.do"
    private val testGooglePayAddress = "https://3dsec.sberbank.ru/payment/google/payment.do"

    fun requestPayment(urlicoName: String, sum: Int, orderNumber: String, callback: SberCallback) {
        Log.d("logtag", "SberAcquiring requestPayment()")
        val requestStr = createRequestString(urlicoName, sum, orderNumber)

        val body = requestStr.toRequestBody("application/x-www-form-urlencoded; charset=utf-8".toMediaTypeOrNull())
        var url: URL? = null
        try {
            url = URL(registerAddress)
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        }

        if (url == null) {
            return
        }
        val request = Request.Builder()
                .url(url)
                .post(body)
                .build()
        val response: Response
        try {
            response = client.newCall(request).execute()
            Log.d("logtag", "response: $response")
            val jsonData = response.body?.string()
            processResponse(jsonData, callback)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun createRequestString(urlicoName: String, sum: Int, orderNumber: String): String {
        val sumInCent = sum*100             //сумма в копейках
        val login = getLogin(urlicoName)
        val password = getPass(urlicoName)
        val requestStr = "userName=$login&password=$password&orderNumber=$orderNumber&amount=$sumInCent&currency=643&language=ru&returnUrl=$successUrl&failUrl=$failUrl&pageView=MOBILE&sessionTimeoutSecs=2400"
        return requestStr
    }

    private fun processResponse(jsonData: String?, callback: SberCallback){
        if (jsonData == null) {
            callback.onFail("Internal error")
            return
        }
        val jobject = JSONObject(jsonData)
        //val responseUrl = Jobject.
        val isError = jobject.has("errorCode")
        if (isError) {
            val errorCode = jobject.get("errorCode")
            if (errorCode != 0) {
                val errorMessage = jobject.getString("errorMessage")
                Log.d("logtag", "errorMessage: $errorMessage")
                callback.onFail(errorMessage)
                return
            }
        }
        val hasOrderId = jobject.has("orderId")
        var orderId = ""
        if (hasOrderId) {
            orderId = jobject.getString("orderId")
        }
        val hasFormUrl = jobject.has("formUrl")
        var responseUrl = ""
        if (hasFormUrl) {
            responseUrl = jobject.getString("formUrl")
        }
        callback.onSuccess(orderId, responseUrl)
    }

    fun requestPaymentWithGooglePay(sum: Int, paymentToken: String, urlicoName: String, orderNumber: String, callback: SberCallback) {
        val sumInCent = sum*100             //сумма в копейках
        val login = getLogin(urlicoName)
        val requestMap = mapOf(
                "merchant" to login,
                "orderNumber" to orderNumber,
                "language" to "RU",
                "paymentToken" to paymentToken,
                "ip" to "127.0.0.1",
                "amount" to sumInCent,
                "currencyCode" to 643
        )
        val json = converttoJson(requestMap)
        val jsonType = "application/json; charset=utf-8".toMediaTypeOrNull()
        val body = RequestBody.create(jsonType, json)
        var url: URL? = null
        try {
            url = URL(googlePayAddress)
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        }

        if (url == null) {
            return
        }
        val request = Request.Builder()
                .url(url)
                .post(body)
                .build()
        val response: Response
        try {
            response = client.newCall(request).execute()
            Log.d("logtag", "response: $response")
            val jsonData = response.body?.string()
            processResponseGooglePay(jsonData, callback)
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    private fun processResponseGooglePay(jsonData: String?, callback: SberCallback) {
        if (jsonData == null) {
            callback.onFail("Internal error")
            return
        }
        val jobject = JSONObject(jsonData)
        //val responseUrl = Jobject.
        val hasSuccess = jobject.has("success")
        if (hasSuccess){
            val isSuccess = jobject.getBoolean("success")
            if (isSuccess) {
                val data = jobject.getString("data")
                val dataObject = JSONObject(data)
                val orderId = dataObject.getString("orderId")
                callback.onSuccess(orderId)
            } else {
                val error = jobject.getString("error")
                val errorObject = JSONObject(error)
                val errorMessage = errorObject.getString("message")
                callback.onFail(errorMessage)
            }
        } else {
            callback.onFail("response error")
        }
    }

    private fun converttoJson(map: Map<String, Any>): String{
        val moshi = Moshi.Builder().build()
        val type = Types.newParameterizedType(Map::class.java, String::class.java, Any::class.java)
        val adapter = moshi.adapter<Map<String, Any>>(type)
        return adapter.toJson(map)
    }

    private fun getPass(urlicoName: String): String {
        return when(urlicoName){
            "urlico1" -> "\$O35p54A\$"
            "urlico2" -> "%9R5e4D%"
            "urlico3" -> "\$O35p54A\$"
            "urlico4" -> "\$O35p54A\$"
            "urlico5" -> "\$O35p54A\$"
            "urlico6" -> "%9R5e4D%"
            "urlico7" -> "%9R5e4D%"
            "urlico8" -> "\$O35p54A\$"
            "urlico9" -> "\$O35p54A\$"
            else -> "XgPicd%IF8N?a)v"
        }
    }

    public interface SberCallback{
        fun onSuccess(orderId: String, responseUrl: String)
        fun onSuccess(orderId: String)
        fun onFail(error: String)
    }
    companion object{
        const val successUrl = "https://totopizza.ru/success/"
        const val failUrl = "https://totopizza.ru/fail"

        fun getLogin(urlicoName: String): String {
            return when(urlicoName){
                "urlico1" -> "totopizza5-api"
                "urlico2" -> "totopizza_2-api"
                "urlico3" -> "totopizza6-api"
                "urlico4" -> "totopizza2-api"
                "urlico5" -> "totopizza3-api"
                "urlico6" -> "totopizza-api"
                "urlico7" -> "totopizza1-api"
                "urlico8" -> "totopizza_3-api"
                "urlico9" -> "P330500673971-api"
                else -> ""
            }
        }
    }
}
