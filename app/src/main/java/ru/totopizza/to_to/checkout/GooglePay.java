package ru.totopizza.to_to.checkout;

import android.os.Build;
import android.util.Log;

import java.util.Optional;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GooglePay {
    private static String logtag = "logtag";
    private static String merchantId = "BCR2DN6T3P4P7RBA";

    /**
     * Create a Google Pay API base request object with properties used in all requests
     * @return Google Pay API base request object
     * @throws JSONException
     */
    private static JSONObject getBaseRequest() throws JSONException {
        return new JSONObject()
                .put("apiVersion", 2)
                .put("apiVersionMinor", 0);
    }

    /**
     * Identify your gateway and your app's gateway merchant identifier
     * <p>The Google Pay API response will return an encrypted payment method capable of being charged
     * by a supported gateway after payer authorization
     * @return payment data tokenization for the CARD payment method
     * @throws JSONException
     * @see <a
     *     href="https://developers.google.com/pay/api/android/reference/object#PaymentMethodTokenizationSpecification">PaymentMethodTokenizationSpecification</a>
     */
    private static JSONObject getTokenizationSpecification(String urlicoLogin) throws JSONException {
        JSONObject tokenizationSpecification = new JSONObject();
        tokenizationSpecification.put("type", "PAYMENT_GATEWAY");
        tokenizationSpecification.put(
                "parameters",
                new JSONObject()
                        .put("gateway", "sberbank")
                        .put("gatewayMerchantId", urlicoLogin));

        return tokenizationSpecification;
    }

    private static JSONArray getAllowedCardNetworks() {
        return new JSONArray()
                .put("MASTERCARD")
                .put("VISA");
    }

    private static JSONArray getAllowedCardAuthMethods() {
        return new JSONArray()
                .put("PAN_ONLY")
                .put("CRYPTOGRAM_3DS");
    }

    /**
     * Describe your app's support for the CARD payment method
     *
     * <p>The provided properties are applicable to both an IsReadyToPayRequest and a
     * PaymentDataRequest
     *
     * @return a CARD PaymentMethod object describing accepted cards
     * @throws JSONException
     * @see <a
     *     href="https://developers.google.com/pay/api/android/reference/object#PaymentMethod">PaymentMethod</a>
     */
    private static JSONObject getBaseCardPaymentMethod() throws JSONException {
        JSONObject cardPaymentMethod = new JSONObject();
        cardPaymentMethod.put("type", "CARD");
        cardPaymentMethod.put(
                "parameters",
                new JSONObject()
                        .put("allowedAuthMethods", GooglePay.getAllowedCardAuthMethods())
                        .put("allowedCardNetworks", GooglePay.getAllowedCardNetworks()));

        return cardPaymentMethod;
    }

    /**
     * Describe the expected returned payment data for the CARD payment method
     *
     * @return a CARD PaymentMethod describing accepted cards and optional fields
     * @throws JSONException
     * @see <a
     *     href="https://developers.google.com/pay/api/android/reference/object#PaymentMethod">PaymentMethod</a>
     */
    private static JSONObject getCardPaymentMethod(String urlicoLogin) throws JSONException {
        Log.d(logtag, "PaymentMethodActivity getCardPaymentMethod()");
        JSONObject cardPaymentMethod = GooglePay.getBaseCardPaymentMethod();
        cardPaymentMethod.put("tokenizationSpecification", GooglePay.getTokenizationSpecification(urlicoLogin));
        return cardPaymentMethod;
    }

    /**
     * Information about the merchant requesting payment information
     *
     * @return information about the merchant
     * @throws JSONException
     * @see <a href="https://developers.google.com/pay/api/android/reference/object#MerchantInfo">MerchantInfo</a>
     */
    private static JSONObject getMerchantInfo(String merchantName) throws JSONException {
        JSONObject merchantJSON =  new JSONObject();
        merchantJSON.put("merchantId", merchantId);
        merchantJSON.put("merchantName", merchantName);
        return merchantJSON;
    }

    /**
     * An object describing accepted forms of payment by your app, used to determine a viewer's
     * readiness to pay
     *
     * @return API version and payment methods supported by the app
     * @see <a
     *     href="https://developers.google.com/pay/api/android/reference/object#IsReadyToPayRequest">IsReadyToPayRequest</a>
     */
    public static Optional<JSONObject> getIsReadyToPayRequest() {
        Log.d(logtag, "PaymentMethodActivity getIsReadyToPayRequest()");
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            return null;
        }
        try {
            JSONObject isReadyToPayRequest = GooglePay.getBaseRequest();
            isReadyToPayRequest.put("allowedPaymentMethods", new JSONArray().put(getBaseCardPaymentMethod()));
            return Optional.of(isReadyToPayRequest);
        } catch (JSONException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    /**
     * An object describing information requested in a Google Pay payment sheet
     * @return payment data expected by your app
     * @see <a
     *     href="https://developers.google.com/pay/api/android/reference/object#PaymentDataRequest">PaymentDataRequest</a>
     */
    public static Optional<JSONObject> getPaymentDataRequest(JSONObject transactionInfo, String urlicoLogin, String merchantName) {
        Log.d(logtag, "PaymentMethodActivity requestPaymentWithGooglePay()");
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            return null;
        }
        try {
            JSONObject paymentDataRequest = GooglePay.getBaseRequest();
            paymentDataRequest.put("allowedPaymentMethods", new JSONArray().put(GooglePay.getCardPaymentMethod(urlicoLogin)));
            paymentDataRequest.put("transactionInfo", transactionInfo);
            paymentDataRequest.put("merchantInfo", GooglePay.getMerchantInfo(merchantName));
            return Optional.of(paymentDataRequest);
        } catch (JSONException e) {
            return Optional.empty();
        }
    }
}