package ru.totopizza.to_to

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

import ru.totopizza.to_to.PromoFragment.OnListFragmentInteractionListener

class PromoRecyclerViewAdapter(private val getContex: Context, private val mValues: List<Banner>, private val mListener: OnListFragmentInteractionListener?) : androidx.recyclerview.widget.RecyclerView.Adapter<PromoRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.simple_image, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.mItem = mValues[position]

        val url = mValues[position].image
        if (url != "") {
            Picasso.get()
                    .load(url)
                    .placeholder(R.drawable.placeholder)
                    .into(holder.mContentView)
        }

        holder.mView.setOnClickListener {
            mListener?.onListFragmentInteraction(holder.mItem!!)
        }
    }

    override fun getItemCount(): Int {
        return mValues.size
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        var mContentView: ImageView? = null
//        var mDescriptionView: TextView? = null
        var mItem: Banner? = null

        init {
            mContentView = mView.findViewById(R.id.image)
//            mDescriptionView = mView.findViewById(R.id.descriptionView)
        }
    }
}
