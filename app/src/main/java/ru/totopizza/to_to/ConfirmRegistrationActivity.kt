package ru.totopizza.to_to

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.CountDownTimer
import androidx.appcompat.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import org.jetbrains.anko.doAsync
import java.text.SimpleDateFormat
import java.util.*
import android.view.inputmethod.InputMethodManager.SHOW_IMPLICIT
import android.os.Build
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.core.content.ContextCompat
import ru.totopizza.to_to.business.SmsSender
import ru.totopizza.to_to.business.UserManager

class ConfirmRegistrationActivity : AppCompatActivity() {
    private lateinit var sendAgainButton: Button
    private lateinit var sendAgainMessage: TextView
    private lateinit var codeEdit: EditText
    private lateinit var timer: Timer

    private lateinit var dotImage1: ImageView
    private lateinit var dotImage2: ImageView
    private lateinit var dotImage3: ImageView
    private lateinit var dotImage4: ImageView

    private val awaitTimeMillis: Long = 60000
    private var phone: String = ""
    private var clearPhone: String = ""
    private var code: String = ""
    private var requestTime: Long = 0
    private var isCheckout = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirm_registration_activity)

        val phoneTextView = findViewById<TextView>(R.id.code_sed_to_phone_number)
        codeEdit = findViewById(R.id.code)
        sendAgainButton = findViewById(R.id.new_code_btn)
        sendAgainMessage = findViewById(R.id.if_code_not_received)

        dotImage1 = findViewById(R.id.code_dot_1)
        dotImage2 = findViewById(R.id.code_dot_2)
        dotImage3 = findViewById(R.id.code_dot_3)
        dotImage4 = findViewById(R.id.code_dot_4)

        phone = intent?.getStringExtra("phone") ?: ""
        clearPhone = intent?.getStringExtra("clearPhone") ?: ""
        code = intent?.getStringExtra("code") ?: ""
        requestTime = intent.getLongExtra("requestTime", System.currentTimeMillis())
        isCheckout = intent.getBooleanExtra("isCheckout", false)

        val codeSendTo = resources.getString(R.string.code_sent_to) + " " + phone
        phoneTextView.text =  codeSendTo

        codeEdit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                if (editable != null){
                    val inputCode = editable.toString()
                    if (inputCode == code) {
                        GoNext()
                    }else if (editable.toString().length == 4){
                        codeEdit.setText("")
                        updateDotsColor(0)
                    }
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val count = s?.count() ?: 0
                updateDotsColor(count)
            }
        })
        //codeEdit.visibility = View.INVISIBLE

        val millis = awaitTimeMillis - (System.currentTimeMillis() - requestTime)
        if (millis > 0) {
            timer = Timer(this, millis, sendAgainButton, sendAgainMessage)
            timer.start()
        } else {
            sendAgainMessage.visibility = View.INVISIBLE
            sendAgainButton.isEnabled = true
            sendAgainButton.background = getDrawable(R.drawable.roundedbg)
        }

        sendAgainButton.setOnClickListener {
            sendAgainButton.isEnabled = false
            sendAgainButton.background = getDrawable(R.drawable.roundedbggrey)
            sendAgainMessage.visibility = View.VISIBLE
            val rand = Random()
            val smsCode = rand.nextInt(8999) + 1000
            sendSms(smsCode.toString())
            timer = Timer(this, millis, sendAgainButton, sendAgainMessage)
            timer.start()
        }
    }

    private fun updateDotsColor(count: Int){
        if (count > 0) {
            dotImage1.setImageResource(R.drawable.radio_on)
        } else {
            dotImage1.setImageResource(R.drawable.radio_grey)
        }
        if (count > 1) {
            dotImage2.setImageResource(R.drawable.radio_on)
        } else {
            dotImage2.setImageResource(R.drawable.radio_grey)
        }
        if (count > 2) {
            dotImage3.setImageResource(R.drawable.radio_on)
        } else {
            dotImage3.setImageResource(R.drawable.radio_grey)
        }
        if (count > 3) {
            dotImage4.setImageResource(R.drawable.radio_on)
        } else {
            dotImage4.setImageResource(R.drawable.radio_grey)
        }
    }

    private fun sendSms(smsCode: String){
        doAsync {
            val sender = SmsSender()
            sender.SendCodeToPhone(smsCode, clearPhone)
        }
    }
    private fun GoNext(){
        val user = App.userManager
        user.saveLoggedIn(true)
        user.savePhoneConfirmed(true)
        user.savePhone(phone)
        user.saveName("")
        user.resetBirthday()
        user.loadFromServer()

        if (isCheckout){
            val detailActivity = Intent(this, CheckoutActivity::class.java)
            startActivity(detailActivity)
        }else {
            val intent = Intent(this, AccountActivity::class.java)
            intent.putExtra("afterConfirmation", false)
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        //getReadSMSPermission()
        showKeyboard()
    }

    private fun showKeyboard() {
        codeEdit.requestFocus()
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(codeEdit, SHOW_IMPLICIT)
    }

    private fun checkReadSMSPermission(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return checkSelfPermission(android.Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED
        } else {
            return true
        }
    }
    private val REQUEST_READ_SMS_PERMISSION = 887

    public fun getReadSMSPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!checkReadSMSPermission()) {
                val permissionsArray = arrayOf(android.Manifest.permission.READ_SMS)
                requestPermissions(permissionsArray, REQUEST_READ_SMS_PERMISSION)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_READ_SMS_PERMISSION) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
                    MessageReceiver.bindListener {
                        if (it == code) {
                            GoNext()
                        }
                    }
                }
            } else {
                showKeyboard()
            }
        }
    }

    class Timer(context: Context,
                millis: Long,
                private val sendAgainButton: Button,
                private val sendAgainMessage: TextView) : CountDownTimer(millis, 1000) {

        private val requestNewCodeMessage: String = context.resources.getString(R.string.get_new_code_message)
        private val buttonBackground = context.getDrawable(R.drawable.roundedbg)
        private val formatMinSec = SimpleDateFormat("m мин. ss сек.", Locale.getDefault())
        private val formatSec = SimpleDateFormat("ss сек.", Locale.getDefault())

        override fun onTick(millisUntilFinished: Long) {
            val dateTime = Date(millisUntilFinished)
            val message = if (millisUntilFinished >= 60000) {
                requestNewCodeMessage + " " + formatMinSec.format(dateTime)
            } else {
                requestNewCodeMessage + " " + formatSec.format(dateTime)
            }
            sendAgainMessage.text = message
            sendAgainMessage.visibility = View.VISIBLE
        }

        override fun onFinish() {
            sendAgainMessage.visibility = View.INVISIBLE
            sendAgainButton.isEnabled = true
            sendAgainButton.background = buttonBackground
        }
    }
}
