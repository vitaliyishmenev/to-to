package ru.totopizza.to_to

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.squareup.picasso.Picasso

import kotlinx.android.synthetic.main.activity_category.*
import kotlinx.android.synthetic.main.content_category.*
import java.util.ArrayList
import com.google.android.material.floatingactionbutton.FloatingActionButton
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager


class CategoryActivity : BaseActivity() {
    private var items = ArrayList<Product>()
    private var fabExpanded = false
    private var fabMenu: FloatingActionButton? = null
    private var layoutFabPriceAsc: LinearLayout? = null
    private var layoutFabPriceDesc: LinearLayout? = null
    private var layoutFabNameAsc: LinearLayout? = null
    private var layoutFabNameDesc: LinearLayout? = null

    override fun onResume() {
        super.onResume()
        updateCartCount()
        recycler_view.adapter?.notifyDataSetChanged()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val titleText = intent.getStringExtra("title")
        val imgUrl = intent.getStringExtra("url")
        val catId = intent.getStringExtra("catId")

        supportActionBar?.title = titleText

        items = IikoAPI.getProducts(catId)

        // set up the RecyclerView
        val recyclerView = recycler_view
        recyclerView.layoutManager = LinearLayoutManager(this)
        val adapter = CategoryAdapter(this, items, 0) { item ->
            val detailActivity = Intent(this, ProductActivity::class.java)
            detailActivity.putExtra("id", item.id)
            this.runOnUiThread { startActivity(detailActivity) }
        }
        recyclerView.adapter = adapter

        if (imgUrl != null && imgUrl != "") {
            Picasso.get()
                    .load(imgUrl)
                    .placeholder(R.drawable.placeholder)
                    .into(parallax_header_imageview)
        } else {
            parallax_header_imageview.visibility = View.GONE
        }

        fabMenu = this.findViewById(R.id.fabMenu)

        layoutFabPriceAsc = this.findViewById(R.id.layoutFabPriceAsc)
        layoutFabPriceDesc = this.findViewById(R.id.layoutFabPriceDesc)
        layoutFabNameAsc = this.findViewById(R.id.layoutFabNameAsc)
        layoutFabNameDesc = this.findViewById(R.id.layoutFabNameDesc)

        fabMenu?.setOnClickListener {
            if (fabExpanded) {
                closeSubMenusFab()
            } else {
                openSubMenusFab()
            }
        }

        layoutFabPriceAsc?.setOnClickListener {
            items.sortBy { it.price }
            this.runOnUiThread { recyclerView.adapter?.notifyDataSetChanged() }
            closeSubMenusFab()
        }

        layoutFabPriceDesc?.setOnClickListener {
            items.sortByDescending { it.price }
            this.runOnUiThread { recyclerView.adapter?.notifyDataSetChanged() }
            closeSubMenusFab()
        }

        layoutFabNameAsc?.setOnClickListener {
            items.sortBy { it.name }
            this.runOnUiThread { recyclerView.adapter?.notifyDataSetChanged() }
            closeSubMenusFab()
        }

        layoutFabNameDesc?.setOnClickListener {
            items.sortByDescending { it.name }
            this.runOnUiThread { recyclerView.adapter?.notifyDataSetChanged() }
            closeSubMenusFab()
        }

        closeSubMenusFab()
    }

    private fun closeSubMenusFab() {
        layoutFabPriceAsc?.setVisibility(View.INVISIBLE)
        layoutFabPriceDesc?.setVisibility(View.INVISIBLE)
        layoutFabNameAsc?.setVisibility(View.INVISIBLE)
        layoutFabNameDesc?.setVisibility(View.INVISIBLE)

        fabMenu?.setImageResource(R.drawable.icons8_sortingarrowsfilled_50)
        fabExpanded = false
    }

    //Opens FAB submenus
    private fun openSubMenusFab() {
        layoutFabPriceAsc?.setVisibility(View.VISIBLE)
        layoutFabPriceDesc?.setVisibility(View.VISIBLE)
        layoutFabNameAsc?.setVisibility(View.VISIBLE)
        layoutFabNameDesc?.setVisibility(View.VISIBLE)

        //Change settings icon to 'X' icon
        fabMenu?.setImageResource(R.drawable.ic_close_black_24dp)
        fabExpanded = true
    }
}

