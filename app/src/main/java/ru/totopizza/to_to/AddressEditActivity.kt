package ru.totopizza.to_to

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_edit_address.*
import ru.totopizza.to_to.business.ComplexStreet
import ru.totopizza.to_to.business.StreetNameParser
import ru.totopizza.to_to.business.SuburbanNameParser
import java.lang.Exception

class AddressEditActivity : BaseActivity() {
    private lateinit var prefs: SharedPreferences
    private val addressManager = App.addressManager
    private var currentAddress: Address? = null
    private var addressIndex = -1
    private var allowDelete = false
    private lateinit var citySpinner: Spinner
    private lateinit var nameEdit: EditText
    private lateinit var streetEdit: AutoCompleteTextView
    private lateinit var streetEditAdapter: ArrayAdapter<String>
    private lateinit var homeEdit: EditText
    private lateinit var apartmentEdit: EditText
    private lateinit var entranceEdit: EditText
    private lateinit var entranceCodeEdit: EditText
    private lateinit var commentEdit: EditText
    private lateinit var floorEdit: EditText
    private lateinit var saveToAddressesCheckBox: CheckBox
    private lateinit var save: TextView
    private lateinit var deleteButton: TextView
    private var city = ""
    private var cityId = ""
    private var suburban = ""
    override val noMenu = true
    private val streetNameParser = StreetNameParser()
    private lateinit var simpleStreets: ArrayList<String>
    private val suburbanNameParser = SuburbanNameParser()
    private var addressServerId = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_address)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        prefs = getSharedPreferences("TTAppPrefs", MODE_PRIVATE)

        city = intent.getStringExtra("city") ?: ""
        cityId = intent.getStringExtra("cityId") ?: ""
        allowDelete = intent.getBooleanExtra("allow_delete", false)

        nameEdit = findViewById(R.id.nameEdit)
        citySpinner = findViewById(R.id.citySpinner)
        streetEdit = findViewById(R.id.streetEdit)
        homeEdit = findViewById(R.id.homeEdit)
        apartmentEdit = findViewById(R.id.apartmentEdit)
        entranceEdit = findViewById(R.id.entranceEdit)
        entranceCodeEdit = findViewById(R.id.entranceCodeEdit)
        commentEdit = findViewById(R.id.commentEdit)
        floorEdit = findViewById(R.id.floorEdit)
        saveToAddressesCheckBox = findViewById(R.id.saveToAddresses)
        save = findViewById(R.id.saveAddress)
        deleteButton = findViewById(R.id.delete_edited_address)

        val showSaveToAccountCheckBox = intent.getBooleanExtra("show_save_to_account_сheck", true)
        if (showSaveToAccountCheckBox) {
            saveToAddressesCheckBox.visibility = View.VISIBLE
            saveToAddressesCheckBox.isEnabled = true
        } else {
            saveToAddressesCheckBox.visibility = View.GONE
            saveToAddressesCheckBox.isEnabled = false
        }

        suburbanNameParser.parse(IikoAPI.getCitiesAndSuburb(city))
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, suburbanNameParser.getNames())
        citySpinner.adapter = adapter

        parseRawStreets(IikoAPI.getStreetsInSuburban(city))
        streetEditAdapter = ArrayAdapter(
                this,
                android.R.layout.simple_dropdown_item_1line,
                simpleStreets
        )
        streetEdit.setAdapter(streetEditAdapter)
        streetEdit.addTextChangedListener(object : TextWatcher {
            private var correctText = ""
            private var cursorPosition = 0
            override fun afterTextChanged(editable: Editable?) {
                try {
                    if (streetEdit.text.toString() != correctText) {
                        streetEdit.setText(correctText)
                        val textLength = streetEdit.text.length
                        /*if (textLength == 0) {
                            cursorPosition = 0
                            streetEdit.setText("")
                        } else */
                        if (cursorPosition > textLength) {
                            cursorPosition = textLength
                        }
                        streetEdit.setSelection(cursorPosition)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                updateSaveButtonEnabled()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                try {
                    if (s != null) {
                        if (s.toString().isEmpty()) {
                            correctText = ""
                            return
                        }
                        val inputedStreet = s.toString()
                        if (correctText == inputedStreet)
                            return
                        var isContain = false
                        for (street in simpleStreets) {
                            if (street.toLowerCase().contains(inputedStreet.toLowerCase())) {
                                isContain = true
                                break
                            }
                        }
                        if (isContain) {
                            correctText = inputedStreet
                            cursorPosition = streetEdit.selectionStart
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
        homeEdit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                updateSaveButtonEnabled()
            }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })

        citySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val newSuburban = suburbanNameParser.getNameByPosition(position)
                if (suburban != newSuburban) {
                    suburban = newSuburban
                    parseRawStreets(IikoAPI.getStreetsInSuburban(suburban))
                    streetEditAdapter.clear()
                    for (street in simpleStreets){
                        streetEditAdapter.add(street)
                    }
                    streetEditAdapter.notifyDataSetChanged()
                    clearAddressFields()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }

        addressIndex = intent.getIntExtra("addressPosition", -1)
        if (addressIndex >= 0) {
            addressManager.setCurrentAddressIndex(addressIndex)
            currentAddress = addressManager.getCurrentAddress()
            if (currentAddress != null) {
                nameEdit.setText(currentAddress!!.name)
                suburban = currentAddress!!.suburban ?: city
                val selectedSuburbPosition = suburbanNameParser.getNameByPosition(suburban)
                citySpinner.setSelection(selectedSuburbPosition)
                streetEdit.setText(currentAddress!!.street)
                homeEdit.setText(currentAddress!!.home)
                apartmentEdit.setText(currentAddress!!.apartment)
                entranceEdit.setText(currentAddress!!.entrance)
                entranceCodeEdit.setText(currentAddress!!.entranceCod)
                commentEdit.setText(currentAddress!!.comment)
                floorEdit.setText(currentAddress!!.floor)
                addressServerId = currentAddress!!.addressId
            }
        }

        save.setOnClickListener {
            if (currentAddress != null){
                editCurrentAddress()
            }else{
                createNewAddress(city)
            }
            var canContinue = true
            if (currentAddress?.street == "") {
                streetEdit.error = resources.getString(R.string.mandatory_field)
                canContinue = false
            }
            if (currentAddress?.home == "") {
                homeEdit.error = resources.getString(R.string.mandatory_field)
                canContinue = false
            }
            if (canContinue){
                val isSave = currentAddress != null
                if (isSave || addressIndex >= 0) {
                    saveAddressToManager(saveToAddressesCheckBox.isChecked)
                }
                val intent = Intent()
                intent.putExtra("address_saved", isSave)
                intent.putExtra("address", currentAddress!!)
                intent.putExtra("addressPosition", addressManager.getCurrentAddressIndex())
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }

        if (allowDelete) {
            deleteButton.visibility = View.VISIBLE
        } else {
            deleteButton.visibility = View.GONE
        }
        deleteButton.setOnClickListener {
            deleteThisAddress()
        }
    }

    private fun parseRawStreets(rawStreets: Array<String>) {
        streetNameParser.parse(rawStreets)
        simpleStreets = streetNameParser.getStreets()
    }

    private fun clearAddressFields() {
        streetEdit.setText("")
        homeEdit.setText("")
        apartmentEdit.setText("")
        entranceEdit.setText("")
        entranceCodeEdit.setText("")
        commentEdit.setText("")
        floorEdit.setText("")
    }

    private fun updateSaveButtonEnabled() {
        if (isStreetExist(streetEdit.text.toString()) && homeEdit.text.toString() != "") {
            save.isEnabled = true
            save.setTextColor(resources.getColor(R.color.colorPrimary))
        } else {
            save.isEnabled = false
            save.setTextColor(resources.getColor(R.color.disabledButton))
        }
    }

    private fun isStreetExist(streetName: String): Boolean {
        if (streetName.isNullOrEmpty() || simpleStreets.isNullOrEmpty())
            return false
        for (street in simpleStreets) {
            if (streetName.toLowerCase() == street.toLowerCase()) {
                return true
            }
        }
        return false
    }

    private fun editCurrentAddress() {
        currentAddress!!.name = nameEdit.text.toString()
        currentAddress!!.suburban = suburban
        val streetName = streetEdit.text.toString()
        currentAddress!!.street = streetName
        currentAddress!!.home = homeEdit.text.toString()
        currentAddress!!.apartment = apartmentEdit.text.toString()
        currentAddress!!.entrance = entranceEdit.text.toString()
        currentAddress!!.entranceCod = entranceCodeEdit.text.toString()
        currentAddress!!.floor = floorEdit.text.toString()
        currentAddress!!.comment = commentEdit.text.toString()

        var delivery = suburbanNameParser.getSuburbanDeliveriesByName(suburban)
        val complexStreet = getComplexStreetByName(streetName)
        val streetDelivery = complexStreet?.delivery ?: -1
        if (streetDelivery > 0) {
            delivery = streetDelivery
        }
        currentAddress!!.delivery = delivery
        val streetDeliveryFree = complexStreet?.deliveryFree ?: -1
        if (streetDeliveryFree > 0) {
            currentAddress!!.freeDelivery = streetDeliveryFree
        }
        val streetUrlico = complexStreet?.urlico ?: ""
        if (!streetUrlico.isNullOrEmpty()) {
            currentAddress!!.urlico = streetUrlico
        }
        val streetIiko = complexStreet?.originalName ?: ""
        if (!streetIiko.isNullOrEmpty()) {
            currentAddress!!.streetIiko = streetIiko
        }
        if (currentAddress!!.addressId.isNullOrEmpty()){
            currentAddress!!.addressId = addressServerId
        }
    }

    private fun createNewAddress(city: String) {
        val cityId = IikoAPI.getCityIdByCityName(suburban)
        val name = nameEdit.text.toString()
        val inputedStreet = streetEdit.text.toString()
        val streetId = IikoAPI.getStreetIdByStreet(cityId, inputedStreet)
        val inputedHome = homeEdit.text.toString()
        val inputedApartment = apartmentEdit.text.toString()
        val inputedFloor = floorEdit.text.toString()
        val inputedEntrance = entranceEdit.text.toString()
        val inputedEntranceCode = entranceCodeEdit.text.toString()
        val inputedComment = commentEdit.text.toString()
        val isPrefer = addressManager.addressCount() == 0
        var delivery = suburbanNameParser.getSuburbanDeliveriesByName(suburban)
        val complexStreet = getComplexStreetByName(inputedStreet)
        val streetDelivery = complexStreet?.delivery ?: -1
        if (streetDelivery > 0) {
            delivery = streetDelivery
        }
        val feliveryFree = complexStreet?.deliveryFree ?: -1
        val streetIiko = complexStreet?.originalName ?: ""
        val urlico = complexStreet?.urlico ?: ""

        currentAddress = Address(name,
                city,
                cityId,
                suburban,
                inputedStreet,
                streetId,
                streetIiko,
                inputedHome,
                inputedApartment,
                inputedFloor,
                inputedEntrance,
                isPrefer,
                inputedEntranceCode,
                inputedComment,
                delivery,
                feliveryFree,
                urlico,
                "")
    }

    private fun saveAddressToManager(saveToAccount: Boolean) {
        if (addressIndex >= 0) {
            addressManager.saveEditedCurrentAddress(currentAddress)
        } else if (currentAddress != null) {
            addressManager.addNewAddress(currentAddress!!, saveToAccount)
        }
    }

    private fun getComplexStreetByName(streetName: String): ComplexStreet? {
        if (simpleStreets.isNullOrEmpty() || streetName.isNullOrEmpty())
            return null
        return streetNameParser.getComplexStreetByName(streetName)
    }

    private fun deleteThisAddress() {
        if (addressIndex >= 0) {
            addressManager.deleteCurrentAddress(addressIndex, currentAddress)
            val intent = Intent()
            intent.putExtra("address_deleted", addressIndex)
            setResult(Activity.RESULT_OK, intent)
        }
        finish()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
