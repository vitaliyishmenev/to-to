package ru.totopizza.to_to

import android.content.ActivityNotFoundException
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.content.Intent
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.Switch
import android.widget.TextView
import com.onesignal.OneSignal

class AboutFragment : Fragment() {
    private var cityName: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_about, container, false)

        val phoneLayout = view.findViewById<LinearLayout>(R.id.phone_number_layout)
        phoneLayout.setOnClickListener {
            if (activity is BaseActivity) {
                (activity as BaseActivity).makeCallOn("88007070232")
            }

        }
        val text = "Единый телефон доставки  - 8 (800) 707-02-32\n\n" +
                "Стоимость доставки по г.Гусь-Хрустальный 50 рублей. \n\n" +
                "Стоимость доставки по г.Владимир 100 рублей, за город - от 130 (более подробную информацию вы можете получить от диспетчера).\n\n" +
                "Стоимость доставки по г.Ковров 80 рублей, стоимость доставки за город уточняйте у диспетчера.\n\n" +
                "Бесплатная доставка от 500 рублей (более подробную информацию вы можете получить от диспетчера).\n\n" +
                "Стоимость доставки по г.Вязники 60 рублей, стоимость доставки за город уточняйте у диспетчера."

        val deliveryInfoLayout = view.findViewById<View>(R.id.delivery_info)
        deliveryInfoLayout.setOnClickListener {
            val detailActivity = Intent(activity, DeliveryActivity::class.java)
            activity?.startActivity(detailActivity)
        }

        val prefs = activity?.getSharedPreferences("TTAppPrefs", AppCompatActivity.MODE_PRIVATE)

        val fbBtn = view.findViewById<ImageButton>(R.id.fbBtn)
        fbBtn.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/groups/to.to.pizza"))
            startActivity(browserIntent)
        }

        val vkBtn = view.findViewById<ImageButton>(R.id.vkBtn)
        vkBtn.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://vk.com/to_to_pizza"))
            startActivity(browserIntent)
        }

        val instBtn = view.findViewById<ImageButton>(R.id.instBtn)
        instBtn.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/pizzaria_toto"))
            startActivity(browserIntent)
        }

        val okBtn = view.findViewById<ImageButton>(R.id.okBtn)
        okBtn.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://ok.ru/totopizza"))
            startActivity(browserIntent)
        }

        val etBtn = view.findViewById<ImageButton>(R.id.etLogo)
        etBtn.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://enjoytouch.ru/"))
            startActivity(browserIntent)
        }

        val review = view.findViewById<View>(R.id.reviewApp)
        review.setOnClickListener {
            val uri = Uri.parse("market://details?id=" + activity?.packageName)
            val goToMarket = Intent(Intent.ACTION_VIEW, uri)
            // To count with Play market backstack, After pressing back button,
            // to taken back to our application, we need to add following flags to intent.
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_NEW_DOCUMENT or Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
            try {
                activity?.startActivity(goToMarket)
            } catch (excption: ActivityNotFoundException) {
                activity?.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + activity?.packageName)))
            }
        }

        val share = view.findViewById<View>(R.id.shareApp)
        share.setOnClickListener {
            val sharingIntent = Intent(Intent.ACTION_SEND)
            sharingIntent.type = "text/plain"
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Приложение Tо-То пицца")
            sharingIntent.putExtra(Intent.EXTRA_TEXT, "http://play.google.com/store/apps/details?id=" + activity?.packageName)
            activity?.startActivity(Intent.createChooser(sharingIntent, "Поделится с помощью"))

        }

        val pushBtn = view.findViewById<Switch>(R.id.push_switch)
        val receivePushNotification = prefs?.getBoolean("receivePushNotification", true) ?: true
        pushBtn.isChecked = receivePushNotification
        pushBtn.setOnCheckedChangeListener { _, isChecked ->
            OneSignal.setSubscription(isChecked)
            val prefEditor = prefs?.edit()
            prefEditor?.putBoolean("receivePushNotification", isChecked)
            prefEditor?.apply()
        }
        return view
    }
}
