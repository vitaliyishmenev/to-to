package ru.totopizza.to_to

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import java.util.ArrayList

class CategoryAdapter(private val getContext: BaseActivity,
                      private var dataSet: List<Product>,
                      private val favoriteCount: Int,
                      private val clickListener: (Product) -> Unit) : RecyclerView.Adapter<CategoryAdapter.ProductViewHolder>() {
    private val pizzaType = 0
    private val nonPizzaType = 1
    var callback: (() -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val layoutId = if (viewType == pizzaType) {
            R.layout.cell_product_pizza
        } else {
            R.layout.cell_product
        }
        val view = inflater.inflate(layoutId, parent, false)
        return ProductViewHolder(getContext, view, clickListener)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val showDivider = needShowDivider(position)
        holder.bind(dataSet[position], showDivider)
    }

    private fun needShowDivider(position: Int): Boolean {
        var showDivider = false
        if (position > favoriteCount - 1) {
            val currentCategory = dataSet[position].parentGroup
            var nextCategory = ""
            if (dataSet.count() > position + 1) {
                nextCategory = dataSet[position + 1].parentGroup
            }
            if (nextCategory.isNotEmpty() && currentCategory != nextCategory) {
                showDivider = true
            }
        } else if (position == favoriteCount - 1) {
            showDivider = true
        }
        return showDivider
    }

    override fun getItemViewType(position: Int): Int {
        val product = dataSet[position]
        val isPizza = product.name.contains("пицца", true)
        return if (isPizza)
            pizzaType
        else
            nonPizzaType
    }

    override fun getItemCount(): Int {
        return dataSet.count()
    }

    fun updateDataset(newList: ArrayList<Product>) {
        dataSet = newList
        notifyDataSetChanged()
    }

    inner class ProductViewHolder(private val getContext: BaseActivity,
                                  view: View,
                                  private var clickListener: (Product) -> Unit) : RecyclerView.ViewHolder(view) {
        private var product: Product? = null

        internal var mImageView = itemView.findViewById<ImageView>(R.id.image)
        internal var mTitleLabel = itemView.findViewById<TextView>(R.id.title_cart)
        internal var mDescLabel = itemView.findViewById<TextView>(R.id.desc)
        internal var mWeightLabel = itemView.findViewById<TextView>(R.id.weight)
        internal var mPriceLabel = itemView.findViewById<TextView>(R.id.price)
        private var mItemsCountLabel = itemView.findViewById<TextView>(R.id.itemsCount)
        var divider = itemView.findViewById<View>(R.id.divider)
        internal var mTagLabel = itemView.findViewById<TextView>(R.id.tag)
        private var minus = itemView.findViewById<Button>(R.id.minus)
        private var plus = itemView.findViewById<Button>(R.id.plus)

        internal var isSetup = false

        internal var isPizzaSize = false
        internal var isPizzaPiquancy = false

        internal var pizzaSizes: ArrayList<Product> = ArrayList()
        internal var pizzaPiquancy: ArrayList<Product> = ArrayList()

        internal var selectedSize: String? = null
        internal var selectedPiquancy: String? = null

        internal var selectedSize33: String? = null

        internal var mPriceLabel2: TextView? = null
        private var mItemsCountLabel2: TextView? = null
        private var minus2: Button? = null
        private var plus2: Button? = null
        internal var btns23: LinearLayout? = null
        internal var size23: LinearLayout? = null
        internal var btns33: LinearLayout? = null
        internal var size33: LinearLayout? = null

        internal var isSingleSize = true

        init {
            minus?.setOnClickListener {
                IikoAPI.removeFromCart(product?.id ?: "", selectedSize, selectedPiquancy)
                updateUI()
                callback?.invoke()
                getContext.updateCartCount()
            }
            plus?.setOnClickListener {
                IikoAPI.addTocart(product?.id ?: "", selectedSize, selectedPiquancy)
                updateUI()
                BaseActivity.vibrate(getContext)
                callback?.invoke()
                getContext.updateCartCount()
            }

            mPriceLabel?.setOnClickListener {
                plus?.callOnClick()
            }

            mPriceLabel2 = itemView.findViewById(R.id.price2)
            mItemsCountLabel2 = itemView.findViewById(R.id.itemsCount2)
            btns23 = itemView.findViewById(R.id.cartBtns23)
            size23 = itemView.findViewById(R.id.size23)
            btns33 = itemView.findViewById(R.id.cartBtns33)
            size33 = itemView.findViewById(R.id.size33)

            minus2 = itemView.findViewById(R.id.minus2)
            plus2 = itemView.findViewById(R.id.plus2)

            minus2?.setOnClickListener {
                IikoAPI.removeFromCart(product?.id ?: "", selectedSize33, selectedPiquancy)
                updateUI()
                callback?.invoke()
                getContext.updateCartCount()
            }
            plus2?.setOnClickListener {
                IikoAPI.addTocart(product?.id ?: "", selectedSize33, selectedPiquancy)
                updateUI()
                BaseActivity.vibrate(getContext)
                callback?.invoke()
                getContext.updateCartCount()
            }

            mPriceLabel2?.setOnClickListener {
                plus2?.callOnClick()
            }

            itemView.setOnClickListener {
                if (product != null) {
                    clickListener(product!!)
                }
            }
        }

        fun bind(product: Product, showDivider: Boolean) {
            this.product = product

            mWeightLabel?.text = (product.calcWeight() * 1000).toString("гр")
            mPriceLabel?.text = product.calcPrice().toString("\u20BD")
            mTitleLabel?.text = product.name
            mDescLabel?.text = product.description
            selectedSize = product.selectedSize
            selectedPiquancy = product.selectedPiquancy
            selectedSize33 = product.selectedSize33

            if (product.price33 > 0) {
                mPriceLabel2?.text = product.price33.toString("\u20BD")
                mPriceLabel2?.visibility = View.VISIBLE
                btns33?.visibility = View.VISIBLE
                size33?.visibility = View.VISIBLE
                isSingleSize = false
            } else {
                mPriceLabel2?.visibility = View.GONE
                btns33?.visibility = View.GONE
                size33?.visibility = View.GONE
                isSingleSize = true
            }

            val tag = product.tags?.firstOrNull()
            if (tag != null && tag != "recomend" && tag != "") {
                mTagLabel?.text = tag
                mTagLabel?.visibility = View.VISIBLE
            } else {
                mTagLabel?.visibility = View.INVISIBLE
            }

            val img = product.images.firstOrNull()
            mImageView?.setImageResource(R.drawable.placeholder)
            if (img != null && img.imageUrl != "") {
                Picasso.get()
                        .load(img.imageUrl)
                        .placeholder(R.drawable.placeholder)
                        .into(mImageView)
            }
            if (showDivider) {
                divider?.visibility = View.VISIBLE
            } else {
                divider?.visibility = View.GONE
            }
            updateUI()
        }

        private fun updateUI() {
            val count = IikoAPI.getCartCount(product?.id ?: "", selectedSize, selectedPiquancy)
            if (count > 0) {
                size23?.visibility = View.GONE
                btns23?.visibility = View.VISIBLE
                mItemsCountLabel?.text = count.toString()

                if (count > 1){
                    minus.setTextColor(getContext.resources.getColor(R.color.colorPrimary))
                    minus.background = getContext.resources.getDrawable(R.drawable.rounded_border_square)
                } else {
                    minus.setTextColor(getContext.resources.getColor(R.color.secondaryText))
                    minus.background = getContext.resources.getDrawable(R.drawable.rounded_border_square_grey)
                }
            } else {
                if (!isSingleSize) {
                    size23?.visibility = View.VISIBLE
                } else {
                    size23?.visibility = View.GONE
                }
                btns23?.visibility = View.GONE
            }

            if (mItemsCountLabel2 != null && !isSingleSize) {
                val itemsCount = IikoAPI.getCartCount(product?.id
                        ?: "", selectedSize33, selectedPiquancy)
                if (itemsCount > 0) {
                    size33?.visibility = View.GONE
                    btns33?.visibility = View.VISIBLE
                    mItemsCountLabel2?.text = itemsCount.toString()

                    if (itemsCount > 1){
                        minus2?.setTextColor(getContext.resources.getColor(R.color.colorPrimary))
                        minus2?.background = getContext.resources.getDrawable(R.drawable.rounded_border_square)
                    } else {
                        minus2?.setTextColor(getContext.resources.getColor(R.color.secondaryText))
                        minus2?.background = getContext.resources.getDrawable(R.drawable.rounded_border_square_grey)
                    }
                } else {
                    size33?.visibility = View.VISIBLE
                    btns33?.visibility = View.GONE
                }
            } else {
                size33?.visibility = View.GONE
                btns33?.visibility = View.GONE
            }
        }
    }
}