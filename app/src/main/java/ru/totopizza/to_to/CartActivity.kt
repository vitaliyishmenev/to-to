package ru.totopizza.to_to

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso

import kotlinx.android.synthetic.main.activity_cart.*
import android.widget.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE
import ru.totopizza.to_to.business.CitiesInfo
import ru.totopizza.to_to.business.UserManager

class CartActivity : BaseActivity() {

    override val showCartMenu: Boolean
        get() = false

    var items: ArrayList<CartItem> = ArrayList()

    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: RecyclerViewAdapter? = null
    private var mLayoutManager: RecyclerView.LayoutManager? = null

    //private var rRecyclerView: RecyclerView? = null
    //private var rAdapter: RecommendRecyclerViewAdapter? = null

    private var checkoutBtn:Button? = null
    private var emptyView: TextView? = null
    private var clearBtn: FloatingActionButton? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        items = ArrayList(IikoAPI.cart.values.toList())
        val rItems = IikoAPI.getRecomendedProducts()
        mRecyclerView = findViewById(R.id.recycler_view)
        mRecyclerView?.setHasFixedSize(true)
        mLayoutManager = LinearLayoutManager(this)
        mRecyclerView?.layoutManager = mLayoutManager

        mAdapter = RecyclerViewAdapter(this, items) { product ->
            val detailActivity = Intent(this, ProductActivity::class.java)
            detailActivity.putExtra("id", product.id)
            this.runOnUiThread { startActivity(detailActivity) }
        }
        mAdapter!!.rDataset = rItems
        mRecyclerView?.adapter = mAdapter

        mAdapter?.callback = {
            mAdapter?.mDataset = ArrayList(IikoAPI.cart.values.toList())
            mRecyclerView?.post{
                mAdapter?.notifyDataSetChanged()
            }
        }

        checkoutBtn = findViewById(R.id.checkout_btn)
        checkoutBtn?.setOnClickListener {
            if (App.userManager.isLoggedIn()) {
                val detailActivity = Intent(this, CheckoutActivity::class.java)
                startActivity(detailActivity)
            } else {
                val intent = Intent(this, RegistrationActivity::class.java)
                intent.putExtra("isCheckout", true)
                //intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                startActivity(intent)
            }
        }
        checkoutBtn?.visibility = if(IikoAPI.getCartCount() > 0) View.VISIBLE else View.INVISIBLE

        emptyView = findViewById(R.id.empty_message_cart)
        emptyView?.visibility = if (items.count() != 0) View.GONE else View.VISIBLE
    }

    override fun onCartClear() {
        super.onCartClear()
        mAdapter?.mDataset = ArrayList(IikoAPI.cart.values.toList())
        mRecyclerView?.post {
            mAdapter?.notifyDataSetChanged()
        }
        //rRecyclerView?.post {
        //    rAdapter?.notifyDataSetChanged()
        //}
    }

    override fun updateCartCount() {
        super.updateCartCount()
        checkoutBtn?.visibility = if(IikoAPI.getCartCount() > 0) View.VISIBLE else View.INVISIBLE
        emptyView?.visibility = if(IikoAPI.getCartCount() > 0) View.GONE else View.VISIBLE
    }

    class RecommendRecyclerViewAdapter(private val getContext: BaseActivity, var mDataset: ArrayList<Product>): RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewAdapter.ViewHolder {
            val view = LayoutInflater.from(parent?.context).inflate(R.layout.cell_recommend_product, parent, false)
            return RItem(getContext, view) { i ->
            }
        }

        override fun getItemCount(): Int {
            return mDataset.count()
        }

        override fun onBindViewHolder(holder: RecyclerViewAdapter.ViewHolder, position: Int) {
            if (holder is RItem) {
                val item = mDataset[position]

                holder.mWeightLabel?.text = (item.calcWeight() * 1000).toString("гр")
                holder.mPriceLabel?.text = item.calcPrice().toString("\u20BD")

                holder.mTitleLabel?.text = item.name

                holder.selectedSize = item.selectedSize
                holder.selectedPiquancy = item.selectedPiquancy

                if (item.tags != null) {
                    val tag = item.tags.firstOrNull()
                    if (tag != null && tag != "recomend" && tag != "") {
                        holder.mTagLabel?.text = tag
                        holder.mTagLabel?.visibility = View.VISIBLE
                    } else {
                        holder.mTagLabel?.visibility = View.INVISIBLE
                    }
                } else {
                    holder.mTagLabel?.visibility = View.INVISIBLE
                }

                holder.productID = item.id

                val img = item.images.firstOrNull()

                holder.mImageView?.setImageResource(R.drawable.placeholder)
                if (img != null && img.imageUrl != "") {
                    Picasso.get()
                            .load(img.imageUrl)
                            .placeholder(R.drawable.placeholder)
                            .into(holder.mImageView)
                }

                //holder.updateUI()
            }
        }

        internal inner class RItem(private val getContext: BaseActivity, getView: View, private var clickListener: (Int) -> Unit): RecyclerViewAdapter.ViewHolder(getView) {
            internal var mImageView: ImageView? = null
            internal var mTitleLabel: TextView? = null
            internal var mWeightLabel: TextView? = null
            internal var mPriceLabel: TextView? = null
            private var mItemsCountLabel: TextView? = null

            internal var mTagLabel: TextView? = null

            internal var isSetup = false

            internal var isPizzaSize = false
            internal var isPizzaPiquancy = false

            internal var pizzaSizes: ArrayList<Product> = ArrayList()
            internal var pizzaPiquancy: ArrayList<Product> = ArrayList()

            internal var selectedSize: String? = null
            internal var selectedPiquancy: String? = null

            private var minus: Button? = null
            private var plus: Button? = null

            internal var productID: String = ""

            init {
                mImageView = itemView.findViewById(R.id.image)
                mTitleLabel = itemView.findViewById(R.id.title_cart)
                mWeightLabel = itemView.findViewById(R.id.weight)
                mPriceLabel = itemView.findViewById(R.id.price)
                mItemsCountLabel = itemView.findViewById(R.id.itemsCount)

                mTagLabel = itemView.findViewById(R.id.tag)

                minus = itemView.findViewById(R.id.minus)
                plus = itemView.findViewById(R.id.plus)

                minus?.setOnClickListener {
                    IikoAPI.removeFromCart(productID, selectedSize, selectedPiquancy)
                    updateUI()
                }
                plus?.setOnClickListener {
                    IikoAPI.addTocart(productID, selectedSize, selectedPiquancy)
                    updateUI()
                }

                itemView.setOnClickListener { clickListener(layoutPosition) }
            }

            fun updateUI() {
                val count = IikoAPI.getCartCount(productID, selectedSize, selectedPiquancy)
                if (count > 0) {
                    minus?.visibility = View.VISIBLE
                    mItemsCountLabel?.visibility = View.VISIBLE

                    mItemsCountLabel?.text = count.toString()
                } else {
                    minus?.visibility = View.GONE
                    mItemsCountLabel?.visibility = View.GONE
                }

                getContext.updateCartCount()

            }
        }
    }

    class RecyclerViewAdapter(private val getContext: BaseActivity, var mDataset: ArrayList<CartItem>, val clickListener: (Product) -> Unit): RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

        private val TYPE_HEADER = 0
        private val TYPE_ITEM = 1
        private val TYPE_TITLE = 2
        private val TYPE_RECOMMENDED_ITEM = 3
        private val TYPE_FOOTER = 4
        private val TYPE_RECOMMENDED_LIST = 5

        var callback: (() -> Unit)? = null
        var rDataset: ArrayList<Product> = ArrayList()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return when (viewType) {
                TYPE_HEADER -> {
                    val view = LayoutInflater.from(parent?.context).inflate(R.layout.cell_cart_header, parent, false)
                    VHHeader(getContext, view)
                }
                TYPE_FOOTER -> {
                    val view = LayoutInflater.from(parent?.context).inflate(R.layout.cell_cart_lite_footer, parent, false)
                    VHFooter(view)
                }
                TYPE_RECOMMENDED_ITEM -> {
                    val view = LayoutInflater.from(parent?.context).inflate(R.layout.cell_recommend_product, parent, false)
                    VHRItem(getContext, view) { i -> clickListener(rDataset[i - 1 - mDataset.count()])
                    }
                }
                TYPE_RECOMMENDED_LIST -> {
                    val view = LayoutInflater.from(parent.context).inflate(R.layout.cell_recommend_products, parent, false)
                    VHRLItem(getContext, view)
                }
                TYPE_TITLE -> {
                    val view = LayoutInflater.from(parent?.context).inflate(R.layout.cell_header_cart, parent, false)
                    VHTitle(view)
                }
                else -> {
                    val view = LayoutInflater.from(parent?.context).inflate(R.layout.cell_cart, parent, false)
                    VHItem(getContext, this, view)
                }
            }
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            when (holder) {
                is VHHeader -> {
                    val myCityId = CitiesInfo.instance.getSelectedCityId()
                    val deliveries = IikoAPI.loadDeliveryInfo(getContext)
                    if (deliveries != null) {
                        val delivery = deliveries[myCityId]
                        if (delivery != null) {
                            holder.mRegionLabel?.text = delivery.translate
                            holder.mDeliveryText?.text = delivery.delivery
                            holder.mPhoneLabel?.text = "8 (800) 707-02-32"//delivery.phone
                        }
                    }
                }
                is VHFooter -> {
                    val freeDeliveryPrice = CitiesInfo.instance.getDefaultFreeDeliveryPrice()
                    var deliveryPrice = CitiesInfo.instance.getDefaultDeliveryPrice()
                    val cartPrice = IikoAPI.getCartPrice()
                    //holder.mCoutLabel?.text = IikoAPI.getCartCount().toString()
                    //holder.mPriceLabel?.text = cartPrice.toString("\u20BD")
                    if (cartPrice > freeDeliveryPrice) {
                        deliveryPrice = 0
                        //holder.mDeliveryPrice?.text = "БЕСПЛАТНО"
                    }/* else {
                        holder.mDeliveryPrice?.text = deliveryPrice.toString("\u20BD")
                    }*/
                    holder.mTotalPrice?.text = (cartPrice + deliveryPrice).toString("\u20BD")
                }
                is VHTitle -> {
                    holder.mTitleLabel?.text = "Рекомендуем попробовать"
                }
                is VHRItem -> {
                    val item = rDataset[position - 1 - mDataset.count()]

                    holder.mWeightLabel?.text =(item.calcWeight()*1000).toString("гр")
                    holder.mPriceLabel?.text = item.calcPrice().toString("\u20BD")

                    holder.mTitleLabel?.text = item.name

                    holder.selectedSize = item.selectedSize
                    holder.selectedPiquancy = item.selectedPiquancy

                    if (item.tags != null) {
                        val tag = item.tags.firstOrNull()
                        if (tag != null && tag != "recomend" && tag != "") {
                            holder.mTagLabel?.text = tag
                            holder.mTagLabel?.visibility = View.VISIBLE
                        } else {
                            holder.mTagLabel?.visibility = View.INVISIBLE
                        }
                    } else {
                        holder.mTagLabel?.visibility = View.INVISIBLE
                    }

                    holder.productID = item.id

                    val img = item.images.firstOrNull()

                    holder.mImageView?.setImageResource(R.drawable.placeholder)
                    if (img != null && img.imageUrl != "") {
                        Picasso.get()
                                .load(img.imageUrl)
                                .placeholder(R.drawable.placeholder)
                                .into(holder.mImageView)
                    }
//

                    holder.updateUI()
                }
                is VHItem -> {
                    val item = mDataset[position]
                    val id = item.id
                    val product = IikoAPI.getProduct(id)

                    holder.productID = item.id

                    holder.selectedSize = item.size
                    holder.selectedPiquancy = item.piquancy

                    holder.updateUI()

                    if (product != null) {
                        holder.mTitleLabel?.text = product.name

                        if (item.size == null) {
                            holder.mPriceLabel?.text = (product.price * item.quantity).toString("\u20BD")
                            val weightText = "вес " + (product.weight * 1000).toString("г.")
                            holder.mWeightLabel?.text = weightText
                        } else {
                            val size = IikoAPI.getProduct(item.size!!)
                            if (size != null) {
                                holder.mPriceLabel?.text = (size.price * item.quantity).toString("\u20BD")
                                val weightText = "вес " + (size.weight * 1000).toString("г.")
                                holder.mWeightLabel?.text = weightText
                                holder.mTitleLabel?.text = product.name + " " + size.name
                            }
                        }

                        if (item.piquancy != null) {
                            holder.mPiquancyLabel?.visibility = View.VISIBLE
                            val piquancy = IikoAPI.getProduct(item.piquancy!!)
                            holder.mPiquancyLabel?.text = piquancy?.name
                        } else holder.mPiquancyLabel?.visibility = View.INVISIBLE

                        holder.itemsCount?.text = item.quantity.toString()

                        val img = product.images.firstOrNull()
                        if (img != null && img.imageUrl != "" && holder.mImageView != null) {
                            Picasso.get()
                                    .load(img.imageUrl)
                                    .into(holder.mImageView)
                        }
                    }
                }
                is VHRLItem -> {
                    val items = IikoAPI.getRecomendedProducts()
                    val mAdapter = RecommendRecyclerViewAdapter(getContext, items)
                    holder.mRecyclerView?.adapter = mAdapter
                    holder.mRecyclerView?.post {
                        holder.mRecyclerView?.adapter?.notifyDataSetChanged()
                    }
                    holder.mRecyclerView?.addOnScrollListener(object : RecyclerView.OnScrollListener(){
                        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                            super.onScrollStateChanged(recyclerView, newState)
                            if (newState == SCROLL_STATE_IDLE && recyclerView.canScrollHorizontally(1)) {
                                val firstVisiblePosition = holder.layoutManager?.findFirstVisibleItemPosition() ?: 0
                                holder.layoutManager?.smoothScrollToPosition(holder.mRecyclerView, RecyclerView.State(), firstVisiblePosition)
                            }
                        }
                    })
                }
            }
        }

        override fun getItemCount(): Int {
            if (mDataset.count() > 0) {
                return mDataset.count() + 3
            }
            return 0
        }

        override fun getItemViewType(position: Int): Int {
            return when (position) {
                in 0 until mDataset.count() -> {
                    TYPE_ITEM
                }
                mDataset.count() -> {
                    TYPE_TITLE
                }
                mDataset.count() + 1 -> {
                    TYPE_RECOMMENDED_LIST
                }
                mDataset.count() + 2 -> {
                    TYPE_FOOTER
                }
                else -> {
                    TYPE_RECOMMENDED_ITEM
                }
            }
        }

        internal inner class VHHeader(private val getContext: BaseActivity, itemView: View) : ViewHolder(itemView) {
            var mRegionLabel: TextView? = null
            var mDeliveryText: TextView? = null
            var mPhoneLabel: TextView? = null
            var mPhoneCallBtn: ImageButton? = null

            init {
                mRegionLabel = itemView.findViewById(R.id.delivery_region)
                mDeliveryText = itemView.findViewById(R.id.delivery_text)
                mPhoneLabel = itemView.findViewById(R.id.delivery_phone)

                mPhoneCallBtn = itemView.findViewById(R.id.delivery_call)

                mPhoneCallBtn?.setOnClickListener {
                    getContext.makeCallOn(mPhoneLabel?.text.toString())
                }
            }
        }

        internal inner class VHFooter(itemView: View) : ViewHolder(itemView) {
            var mCoutLabel: TextView? = null
            var mPriceLabel: TextView? = null
            var mDeliveryPrice: TextView? = null
            var mTotalPrice: TextView? = null

            //var promoInput: EditText? = null
            var discountImage: ImageView? = null
            var discountTitle: TextView? = null
            var discountValue: TextView? = null


            init {
                mCoutLabel = itemView.findViewById(R.id.itemsCount)
                mPriceLabel = itemView.findViewById(R.id.price)
                mDeliveryPrice = itemView.findViewById(R.id.delivery_price)
                mTotalPrice = itemView.findViewById(R.id.total_price)
                discountImage = itemView.findViewById(R.id.discount_image)
                discountTitle = itemView.findViewById(R.id.discount_title)
                discountValue = itemView.findViewById(R.id.discount)
                /*promoInput = itemView.findViewById(R.id.promocode_input)
                promoInput?.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(editable: Editable?) {
                    }

                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    }

                    override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        if (s != null) {
                           if (s.toString().isNotEmpty()) {
                               discountImage?.visibility = View.VISIBLE
                               discountTitle?.visibility = View.VISIBLE
                               discountValue?.visibility = View.VISIBLE
                           } else {
                               discountImage?.visibility = View.GONE
                               discountTitle?.visibility = View.GONE
                               discountValue?.visibility = View.GONE
                           }
                        }
                    }
                })*/
                discountImage?.visibility = View.GONE
                discountTitle?.visibility = View.GONE
                discountValue?.visibility = View.GONE
            }
        }

        internal inner class VHTitle(itemView: View) : ViewHolder(itemView) {
            var mTitleLabel: TextView? = null
            init {
                mTitleLabel = itemView.findViewById(R.id.title_cart)
            }
        }

        internal inner class VHRLItem(private val getContext: BaseActivity, getView: View): ViewHolder(getView) {
            var mRecyclerView: RecyclerView? = null
            var layoutManager: LinearLayoutManager? = null
            init {
                mRecyclerView = itemView.findViewById(R.id.recycler_view)
                layoutManager = LinearLayoutManager(getContext, LinearLayoutManager.HORIZONTAL, false)
                mRecyclerView?.layoutManager = layoutManager
            }
        }

        internal inner class VHRItem(private val getContext: BaseActivity, getView: View, private var clickListener: (Int) -> Unit): ViewHolder(getView) {
            internal var mImageView: ImageView? = null
            internal var mTitleLabel: TextView? = null
            internal var mWeightLabel: TextView? = null
            internal var mPriceLabel: TextView? = null
            private var mItemsCountLabel: TextView? = null

            internal var mTagLabel: TextView? = null

            internal var isSetup = false

            internal var isPizzaSize = false
            internal var isPizzaPiquancy = false

            internal var pizzaSizes: ArrayList<Product> = ArrayList()
            internal var pizzaPiquancy: ArrayList<Product> = ArrayList()

            internal var selectedSize: String? = null
            internal var selectedPiquancy: String? = null

            private var minus: Button? = null
            private var plus: Button? = null

            internal var productID: String = ""

            init {
                mImageView = itemView.findViewById(R.id.image)
                mTitleLabel = itemView.findViewById(R.id.title_cart)
                mWeightLabel = itemView.findViewById(R.id.weight)
                mPriceLabel = itemView.findViewById(R.id.price)
                mItemsCountLabel = itemView.findViewById(R.id.itemsCount)

                mTagLabel = itemView.findViewById(R.id.tag)

                minus = itemView.findViewById(R.id.minus_cart)
                plus = itemView.findViewById(R.id.plus_cart)

                minus?.setOnClickListener {
                    IikoAPI.removeFromCart(productID, selectedSize, selectedPiquancy)
                    updateUI()
                    callback?.invoke()
                }
                plus?.setOnClickListener {
                    IikoAPI.addTocart(productID, selectedSize, selectedPiquancy)
                    updateUI()
                    callback?.invoke()
                }

                itemView.setOnClickListener { clickListener(layoutPosition) }
            }

            fun updateUI() {
                val count = IikoAPI.getCartCount(productID, selectedSize, selectedPiquancy)
                if (count > 0) {
                    minus?.visibility = View.VISIBLE
                    mItemsCountLabel?.visibility = View.VISIBLE

                    mItemsCountLabel?.text = count.toString()
                } else {
                    minus?.visibility = View.GONE
                    mItemsCountLabel?.visibility = View.GONE
                }

                getContext.updateCartCount()

            }
        }

        internal inner class VHItem(private val getContext: BaseActivity, var adapter: RecyclerViewAdapter, itemView: View) : ViewHolder(itemView) {
            var mTitleLabel: TextView? = null
            var mWeightLabel: TextView? = null
            var mPriceLabel: TextView? = null
            var itemsCount: TextView? = null
            var mImageView: ImageView? = null

            var mPiquancyLabel: TextView? = null

            internal var selectedSize: String? = null
            internal var selectedPiquancy: String? = null

            private var minus: Button? = null
            private var plus: Button? = null

            internal var productID: String = ""

            init {
                mTitleLabel = itemView.findViewById(R.id.title_cart)
                mWeightLabel = itemView.findViewById(R.id.weight)
                mPriceLabel = itemView.findViewById(R.id.price)
                mImageView = itemView.findViewById(R.id.image)
                mPiquancyLabel = itemView.findViewById(R.id.piquancy)

                minus = itemView.findViewById(R.id.minus_cart)
                itemsCount = itemView.findViewById(R.id.items_count_cart)
                plus = itemView.findViewById(R.id.plus_cart)

                minus?.setOnClickListener {
                    val count = IikoAPI.getCartCount(productID, selectedSize, selectedPiquancy)
                    if (count > 0) {
                        val remainCount = IikoAPI.removeFromCart(productID, selectedSize, selectedPiquancy)
                        adapter.mDataset[layoutPosition].quantity = remainCount
                        updateUI()
                        callback?.invoke()
                        getContext.updateCartCount()
                    }
                }

                plus?.setOnClickListener {
                    val count = IikoAPI.addTocart(productID, selectedSize, selectedPiquancy)
                    adapter.mDataset[layoutPosition].quantity = count
                    updateUI()
                    callback?.invoke()
                    getContext.updateCartCount()
                }
            }

            fun updateUI() {
                val count = IikoAPI.getCartCount(productID, selectedSize, selectedPiquancy)
                if (count <= 0) {
                    minus?.visibility = View.GONE
                    itemsCount?.visibility = View.GONE
                } else {
                    minus?.visibility = View.VISIBLE
                    minus?.isEnabled = count > 0
                    if (count > 1) {
                        minus?.setTextColor(getContext.resources.getColor(R.color.colorPrimary))
                        minus?.background = getContext.resources.getDrawable(R.drawable.rounded_border_bold_square)
                        minus?.text = "-"
                    } else {
                        minus?.setTextColor(getContext.resources.getColor(R.color.disabledButton))
                        minus?.background = getContext.resources.getDrawable(R.drawable.rounded_border_bold_square_grey)
                        minus?.text = "-"
                    }
                    itemsCount?.visibility = View.VISIBLE
                    itemsCount?.text = count.toString()
                }
            }
        }

        open class ViewHolder(var getView: View): RecyclerView.ViewHolder(getView)

    }
}