package ru.totopizza.to_to.business

import android.util.Log
import java.util.ArrayList
import kotlin.Exception

class StreetNameParser {
    private val delimiter = "$"
    private val complexStreets = ArrayList<ComplexStreet>()

    fun parse(rawStreets: Array<String>) {
        complexStreets.clear()
        for (rawStreet in rawStreets) {
            val complexStreet = parseStreetName(rawStreet)
            complexStreets.add(complexStreet)
        }
    }

    fun parseStreetName(rawStreet: String): ComplexStreet {
        if (rawStreet.contains(delimiter)) {
            var streetName = rawStreet.substringBefore(delimiter).trim()
            if (rawStreet.contains("{") && rawStreet.contains("}")) {
                val deliveryInfo = rawStreet.substringBefore("}").trim().substringAfter("{").trim()
                val deliveryStr = deliveryInfo.substringBefore(";").trim()
                var delivery = -1
                try {
                    delivery = deliveryStr.toInt()
                } catch (e: Exception) {
                    Log.d("logtag", "exception for street name: $rawStreet")
                    e.printStackTrace()
                }
                val deliveryFreeStr = deliveryInfo.substringAfter(";").trim().substringBefore(";").trim()
                var deliveryFree = -1
                try {
                    deliveryFree = deliveryFreeStr.toInt()
                } catch (e: Exception) {
                    Log.d("logtag", "exception for street name: $rawStreet")
                    e.printStackTrace()
                }
                val urlicoStr = deliveryInfo.substringAfterLast(";").trim()
                return ComplexStreet(rawStreet, streetName, delivery, deliveryFree, urlicoStr)
            } else {
                var streetDeliveryStr = rawStreet.substringAfter(delimiter).trim()
                if (streetDeliveryStr.contains(" ")) {
                    val streetName2 = streetDeliveryStr.substringAfter(" ")
                    streetName += ", $streetName2"
                    val city = CitiesInfo.instance.getSelectedCity()
                    if (streetName.contains(city)){
                        streetName = streetName.substringBefore(city).trim()
                    }
                    if (streetName.endsWith(",")){
                        streetName = streetName.substringBeforeLast(",")
                    }
                    streetDeliveryStr = streetDeliveryStr.substringBefore(" ")
                }
                var streetDelivery = -1
                try {
                    streetDelivery = streetDeliveryStr.toInt()
                } catch (e: Exception) {
                    Log.d("logtag", "exception for street name: $rawStreet")
                    e.printStackTrace()
                }
                return ComplexStreet(rawStreet, streetName, streetDelivery, -1, "")
            }
        } else {
            return ComplexStreet(rawStreet, rawStreet, -1, -1, "")
        }
    }

    fun getStreets(): ArrayList<String> {
        val resultList = ArrayList<String>()
        for (complexStreet in complexStreets) {
            resultList.add(complexStreet.simpleName)
        }
        return resultList
    }

    fun getComplexStreetByName(streetName: String): ComplexStreet? {
        if (streetName.isEmpty())
            return null
        for (complexStreet in complexStreets) {
            if (complexStreet.simpleName == streetName){
                return complexStreet
            }
        }
        return null
    }
}

data class ComplexStreet(
        val originalName: String,
        val simpleName: String,
        val delivery: Int,
        val deliveryFree: Int,
        val urlico: String
)