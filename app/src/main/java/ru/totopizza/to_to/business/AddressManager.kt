package ru.totopizza.to_to.business

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import ru.totopizza.to_to.Address
import ru.totopizza.to_to.App


class AddressManager {
    private val logtag = "logtag"
    private val userManager = App.userManager

    init {
        Log.d(logtag, "AddressManager()")
    }

    private var prefs: SharedPreferences? = null

    var addressList = ArrayList<Address>()
    var savedAddressesToServer = ArrayList<Address>() //адреса, сохраненные на сервер, но еще не загруженные с него
    private var cityId = ""

    fun init(context: Context) {
        if (prefs == null) {
            prefs = context.getSharedPreferences("TTAppPrefs", AppCompatActivity.MODE_PRIVATE)
        }
    }

    fun setCityId(cityId: String) {
        this.cityId = cityId
    }

    private fun getKey(): String {
        return cityId + "AddressList"
    }

    fun loadAddressList(loadLocalAddress: Boolean, loadSavedAddress: Boolean) {
        val city = CitiesInfo.instance.getSelectedCity()
        addressList = userManager.getAddressList(city)

        if (loadLocalAddress) {
            val localList = loadLocalList()
            addressList.addAll(localList)
        }
        if (loadSavedAddress) {
            addressList.addAll(savedAddressesToServer)
        }
    }

    private fun loadLocalList() : ArrayList<Address> {
        val key = getKey()
        val addressListJson = prefs?.getString(key, null)
        if (addressListJson != null) {
            val moshi = Moshi.Builder().build()
            val type = Types.newParameterizedType(List::class.java, Address::class.java)
            val adapter = moshi.adapter<List<Address>>(type)
            val result = adapter.fromJson(addressListJson)
            if (result != null) {
                return ArrayList(result)
            }
        }
        return ArrayList()
    }

/*
    private fun sendLocalAddressToServer() {
        if (addressList.count() > 0) {
            for (address in addressList) {
                addNewAddress(address, true)
            }
        }
    }
*/
/*
fun reloadAddressList() {
    loadAddressList()
}


fun setPdress(address: Address?) {
    if (address != null) {
        preferAddress = address
        if (!addressList.contains(address)) {
            addressList.add(address)
        }
        for (customerAddress in addressList) {
            customerAddress.isPrefer = customerAddress == address
        }
    }
    saveAddressList()
}

fun setPreferAddressIndex(index: Int) {
    val newPreferAddress = addressList[index]
    setPreferAddress(newPreferAddress)
}*/

    private fun saveAddressListLocally() {
        val locallyAddresses = getLocallyAddresses()
        val moshi = Moshi.Builder().build()
        val type = Types.newParameterizedType(List::class.java, Address::class.java)
        val adapter = moshi.adapter<List<Address>>(type)
        val jsonAddressList = adapter.toJson(locallyAddresses)
        val prefEditor = prefs?.edit()
        val key = getKey()
        prefEditor?.putString(key, jsonAddressList)
        prefEditor?.apply()
    }

    private fun getLocallyAddresses(): ArrayList<Address> {
        val resultList = ArrayList<Address>()
        val city = CitiesInfo.instance.getSelectedCity()
        val serverAddressList = userManager.getAddressList(city)
        if (serverAddressList.count() == 0) {
            return addressList
        }
        for (localAddress in addressList) {
            if (!serverAddressList.contains(localAddress)) {
                resultList.add(localAddress)
            }
        }
        return resultList
    }


    fun clearLocalAddress() {
        val editor = prefs?.edit()
        val key = getKey()
        editor?.remove(key)
        editor?.apply()
    }

    fun addressCount(): Int {
        return addressList.count()
    }

    fun addNewAddress(newAddress: Address, saveToAccount: Boolean) {
        Log.d(logtag, "AddressManager addNewAddress($newAddress)")
        if (!addressList.contains(newAddress)) {
            addressList.add(newAddress)
            if (saveToAccount) {
                val sApi = ServerApi()
                sApi.addNewAddress(newAddress, userManager.getUserId())
                if (!savedAddressesToServer.contains(newAddress)){
                    savedAddressesToServer.add(newAddress)
                }
            }
        }
        saveAddressListLocally()
    }

    private var currentAddressIndex = 0
    fun setCurrentAddressIndex(index: Int) {
        currentAddressIndex = index
    }

    fun getCurrentAddress(): Address? {
        return addressList[currentAddressIndex]
    }

    fun saveEditedCurrentAddress(currentAddress: Address?) {
        if (currentAddress != null) {
            addressList[currentAddressIndex] = currentAddress
            //saveAddressList()

            val sApi = ServerApi()
            sApi.editAddress(currentAddress, userManager.getUserId())
        }
    }

    fun deleteCurrentAddress(index: Int, currentAddress: Address?) {
        currentAddressIndex = 0
        addressList.removeAt(index)
        if (currentAddress != null) {
            val sApi = ServerApi()
            sApi.deleteAddress(currentAddress, userManager.getUserId())
        }
    }

    fun getCurrentAddressIndex(): Int {
        return currentAddressIndex
    }
}
