package ru.totopizza.to_to.business

import android.util.Log

class SuburbanNameParser {
    private val delimiter = "$"
    private val names = ArrayList<String>()
    private val deliveries = ArrayList<Int>()

    fun parse(citiesAndSuburbRawList: List<String>) {
        names.clear()
        deliveries.clear()

        for (city in citiesAndSuburbRawList) {
            if (city.contains(delimiter)) {
                val cityName = city.substringBefore(delimiter).trim()
                var cityDeliveryStr = city.substringAfter(delimiter).trim()
                if (cityDeliveryStr.contains(" ")){
                    cityDeliveryStr = cityDeliveryStr.substringBefore(" ")
                }
                var cityDelivery = -1
                try {
                    cityDelivery = cityDeliveryStr.toInt()
                } catch (e: Exception) {
                    Log.d("logtag", "exception for city name: $city")
                    e.printStackTrace()
                }
                names.add(cityName)
                deliveries.add(cityDelivery)
            } else {
                names.add(city)
                deliveries.add(-1)
            }
        }
    }

    fun getSuburbanDeliveriesByName(suburbanName: String) : Int {
        if (suburbanName.isNullOrEmpty())
            return -1
        val index = names.indexOf(suburbanName)
        if (index < 0 || index > deliveries.count() - 1)
            return -1
        return deliveries[index]
    }

    fun getNames(): ArrayList<String> {
        return names
    }

    fun getNameByPosition(position: Int): String {
        if (position < 0 || position > names.count() - 1)
            return ""
        return names[position]
    }

    fun getNameByPosition(name: String): Int {
        return names.indexOf(name)
    }
}