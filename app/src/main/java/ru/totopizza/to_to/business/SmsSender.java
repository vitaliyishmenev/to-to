package ru.totopizza.to_to.business;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SmsSender {
    private final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private OkHttpClient client = new OkHttpClient();

    public SmsSender() {
    }
    public void SendCodeToPhone(String code, String phone) {
        JSONObject mess;
        try {
            mess = new JSONObject();
            mess.put("phone", phone);
            mess.put("sender", "totopizza");
            mess.put("clientId", "0");
            mess.put("text",code + " - код для входа в приложеине.");
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        JSONArray ja = new JSONArray();
        ja.put(mess);
        JSONObject resultJson = new JSONObject();
        try {
            resultJson.put("messages", ja);
            resultJson.put("showBillingDetails",true);
            resultJson.put("login","z1518162445769");
            resultJson.put("password","852756");
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        RequestBody body = RequestBody.create(resultJson.toString(), JSON);
        URL url = null;
        try {
            url = new URL("http://api.iqsms.ru/messages/v2/send.json");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if (url == null) {
            return;
        }
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response;
        try {
            response = client.newCall(request).execute();
            Log.d("logtag", "response: " + response);
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.d("logtag", "onFailure, e: " + e.getMessage());
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) {
                Log.d("logtag", "onResponse, response: " + response);
                ResponseBody responseBody = response.body();
                if (responseBody != null) {
                    try {
                        Log.d("logtag", "       responseBody: " + responseBody.string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });*/
    }
}

