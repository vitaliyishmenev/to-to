package ru.totopizza.to_to.business

import android.content.Context
import android.content.SharedPreferences
import android.location.Address
import android.location.Geocoder
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.squareup.moshi.Moshi
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import ru.totopizza.to_to.Location
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class CitiesInfo {
    companion object {
        val instance = CitiesInfo()
        val availableCities = arrayOf("Владимир", "Вязники", "Гусь-Хрустальный", "Иваново", "Ковров", "Нижний Новгород", "Дзержинск")
        val citiesIds = arrayOf("vladimir", "vyazniki", "gus-hrustalny", "ivanovo", "kovrov", "niznii-novgorod", "dzerzinsk")
        val citiesAreaName = arrayOf("Владимирская область", "Вязниковский район", "Гусь-Хрустальный район", "Ивановский район", "Ковровский район", "Нижегородская область", "Дзержинский район")
        const val myCityIdKey = "myCityId"
        const val myCityKey = "myCity"
    }

    val citiesFreeDeliveryPrice = arrayOf(600, 600, 600, 600, 600, 600, 600)    //значения по умолчанию, если не добавлены адреса
    val citiesDeliveryPrice = arrayOf(100, 100, 80, 120, 100, 100, 100)          //значения по умолчанию, если не добавлены адреса

    private val logtag: String = "logtag"
    private var urlica: List<Urlico>? = null
    private var selectedCityId = ""
    private var selectedCity = ""

    fun loadSelectedCity(context: Context) {
        val prefs = context.getSharedPreferences("TTAppPrefs", AppCompatActivity.MODE_PRIVATE)
        selectedCityId = prefs.getString(myCityIdKey, "") ?: ""
        selectedCity = prefs.getString(myCityKey, "") ?: ""
    }

    fun setSelectedCity(cityId: String, context: Context) {
        selectedCityId = cityId
        var number = citiesIds.indexOf(selectedCityId)
        if (number < 0) {
            number = availableCities.indexOf(selectedCityId)
            if (number >= 0) {
                selectedCityId = citiesIds[number]
            }
        } else {
            selectedCity = availableCities[number]
        }
        saveSelectedCity(context)
    }

    private fun getSelectedCityNumber(): Int {
        var number = citiesIds.indexOf(selectedCityId)
        if (number < 0) {
            number = availableCities.indexOf(selectedCityId)
        }
        return number
    }

    private fun saveSelectedCity(context: Context) {
        val prefEditor = context.getSharedPreferences("TTAppPrefs", AppCompatActivity.MODE_PRIVATE).edit()
        prefEditor?.putString(myCityKey, selectedCity)
        prefEditor?.putString(myCityIdKey, selectedCityId)
        prefEditor?.apply()
    }

    fun getSelectedCity(): String {
        return selectedCity
    }

    fun getSelectedCityId(): String {
        return selectedCityId
    }

    fun getSelectedCityAreaName(): String {
        val number = getSelectedCityNumber()
        return citiesAreaName[number]
    }

    fun loadData(context: Context) {
        val serverApi = ServerApi()
        serverApi.loadCitiesInfo(object : Callback {
            private var prefs: SharedPreferences? = null
            private val fullDataKey = "full_data_urlica"

            override fun onFailure(call: Call, e: IOException) {
                Log.d(logtag, "CitiesInfo citiesInfoCallback onFailure")
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                Log.d(logtag, "CitiesInfo citiesInfoCallback onResponse")
                val responseData = response.body?.string() ?: ""
                saveLoadedData(responseData)
                val cityData = convertToCityData(responseData)
                urlica = parseLoadedData(cityData, context)
            }

            private fun convertToCityData(responseData: String): ServerCityData? {
                val moshi = Moshi.Builder().build()
                val jsonAdapter = moshi.adapter(ServerCityData::class.java)
                return jsonAdapter.fromJson(responseData)
            }

            private fun saveLoadedData(responseData: String) {
                if (prefs == null)
                    prefs = context.getSharedPreferences("TTAppPrefs", AppCompatActivity.MODE_PRIVATE)
                val prefEditor = prefs?.edit()
                prefEditor?.putString(fullDataKey, responseData)
                prefEditor?.apply()
            }
        })
    }

    /*
    private fun testGetPricesByAddress(context: Context) {
        Log.d("logtag", "======testGetPricesByAddress======")
        val locationAddress = "город Дзержинск, проспект Свердлова, д.22В"
        Log.d("logtag", "------------locationAddress: $locationAddress")
        selectedCityId = "dzerzinsk"
        selectedCity = "Дзержинск"
        val deliveryPrices = getPricesByAddress(locationAddress, context)
        Log.d("logtag", "------------free_delivery_price: ${deliveryPrices!!["free_delivery_price"]}")
        Log.d("logtag", "------------delivery_price: ${deliveryPrices["delivery_price"]}")
    }*/

    fun getDefaultFreeDeliveryPrice(): Int {
        if (selectedCityId.isEmpty() || !selectedCityId.contains(selectedCityId))
            return 0
        val number = citiesIds.indexOf(selectedCityId)
        return citiesFreeDeliveryPrice[number]
    }

    fun getDefaultDeliveryPrice(): Int {
        if (selectedCityId.isEmpty() || !selectedCityId.contains(selectedCityId))
            return 0
        val number = citiesIds.indexOf(selectedCityId)
        return citiesDeliveryPrice[number]
    }

    private fun parseLoadedData(citiesInfo: ServerCityData?, context: Context): List<Urlico>? {
        val resultList = ArrayList<Urlico>()
        if (citiesInfo == null || citiesInfo.features.isNullOrEmpty())
            return resultList
        for (feature in citiesInfo.features) {
            val urlicoId = feature.properties.payment_urlico
            if (urlicoId == null || urlicoId.isEmpty())
                continue
            val urlico = getUrlico(urlicoId, resultList)
            setOfficialNameToUrlico(urlico, feature.properties.description)
            val cityName = getCityName(feature, context)
            val gorod = urlico.getGorod(cityName)
            val terminalId = feature.properties.terminal_id
            val terminal = gorod.getTerminal(terminalId)
            terminal.addGeometry(feature.properties, feature.geometry)
        }
        //Log.d(logtag, "data: $data")
        return resultList
    }

    private fun setOfficialNameToUrlico(urlico: Urlico, description: String) {
        if (urlico.officialName.isNotEmpty())
            return
        var name = ""
        val delimiter = '#'
        if (description.contains(delimiter)) {
            name = description.substringAfter(delimiter)
            name = name.substringBefore(delimiter)
        }
        if (name.isNotEmpty()) {
            //Log.d(logtag, "setOfficialNameToUrlico(description: $description) => $name")
            urlico.officialName = name
        }
    }

    private fun getUrlico(urlicoId: String, data: ArrayList<Urlico>): Urlico {
        if (data.isEmpty()) {
            return createUrlico(urlicoId, data)
        }
        for (ul in data) {
            if (ul.id == urlicoId)
                return ul
        }
        return createUrlico(urlicoId, data)
    }

    private fun createUrlico(urlicoId: String, data: ArrayList<Urlico>): Urlico {
        val ul = Urlico(urlicoId)
        data.add(ul)
        return ul
    }

    private fun getCityName(feature: ServerCityData.Feature, context: Context): String {
        var cityName = getCityNameByDescription(feature.properties.description)
        if (cityName.isEmpty() && feature.properties.iconCaption != null) {
            cityName = getCityNameByDescription(feature.properties.iconCaption)
        }
        if (cityName.isEmpty()) {
            cityName = getCityNameByLocation(feature.geometry, context)
        }
        return cityName
    }

    private fun getCityNameByDescription(description: String?): String {
        if (description.isNullOrEmpty())
            return ""
        for (cityName in availableCities) {
            if (description.contains(cityName))
                return cityName
        }
        return ""
    }

    private fun getCityNameByLocation(geometry: ServerCityData.Geometry, context: Context): String {
        var result = ""
        val location = getLocation(geometry)
        if (location != null) {
            val gcd = Geocoder(context, Locale.getDefault())
            val addresses: List<Address> = gcd.getFromLocation(location.lon, location.lat, 1)
            if (addresses != null && addresses.isNotEmpty() && addresses[0].locality != null) {
                result = addresses[0].locality
                if (result.startsWith("город ")) {
                    result = result.substringAfter("город ")
                }
                return result
            }
        }
        return result
    }

    private fun getLocation(geometry: ServerCityData.Geometry): Location? {
        if (geometry.type == ServerCityData.Geometry.pointType) {
            return getPointLocation(geometry)
        } else if (geometry.type == ServerCityData.Geometry.polygonType) {
            return getPolygonLocation(geometry)

        }
        return null
    }

    private fun getPointLocation(geometry: ServerCityData.Geometry): Location? {
        if (geometry.coordinates.isNotEmpty() && geometry.coordinates.count() == 2) {
            val lat = geometry.coordinates[1] as Double
            val lon = geometry.coordinates[0] as Double
            return Location(lat, lon)
        }
        return null
    }

    private fun getPolygonLocation(geometry: ServerCityData.Geometry): Location? {
        if (geometry.coordinates.isNotEmpty()) {
            for (c1 in geometry.coordinates) {
                try {
                    val pointsList = c1 as List<List<Double>>
                    if (pointsList.count() > 0) {
                        val point = pointsList[0]
                        val lat = point[0]
                        val lon = point[1]
                        return Location(lat, lon)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
        return null
    }


    fun getPropertiesByAddress(address: String, context: Context): AddressProperty? {
        val location = findLocationByAddress(address, context)
        //Log.d("logtag", "getPricesByAddress() location: ${location.lat}, ${location.lon}")
        if (location.lat < 0.1 || location.lon < 0.1) {
            return null
        }
        return getPropertiesByLocation(location)
    }

    private fun findLocationByAddress(address: String, context: Context): Location {
        //Log.d("logtag", "findLocationByAddress($address)")
        val coder = Geocoder(context)
        try {
            val foundedAddress = coder.getFromLocationName(address, 2)
            if (foundedAddress != null && foundedAddress.count() > 0) {
                val loc = foundedAddress[0]
                //Log.d("logtag", "findLocationByAddress() loc: ${loc.latitude}, ${loc.longitude}")
                return Location(loc.latitude, loc.longitude)
            }
        } catch (e: Exception) {
            //Log.d("logtag", e.message)
            e.printStackTrace()
        }
        //Log.d("logtag", "return empty location")
        return Location(0.0, 0.0)
    }

    private fun getPropertiesByLocation(loc: Location): AddressProperty? {
        //Log.d("logtag", "getPropertiesByLocation($loc)")
        val urlica = getUrlicaForCity(selectedCity)
        if (urlica.isNotEmpty()) {
            for (ul in urlica) {
                val properties = findLocationInUrlicoAreas(ul, loc, selectedCity)
                if (properties != null) {
                    return properties
                }
            }
        }
        //Log.d("logtag", "location not found")
        return null
    }

    private fun findLocationInUrlicoAreas(urlico: Urlico, loc: Location, selectedCity: String): AddressProperty? {
        //Log.d("logtag", "findLocationInUrlicoAreas(urlico: ${urlico.officialName}, loc: $loc, selectedCity: $selectedCity)")
        if (urlico == null || loc == null)
            return null
        val cities = urlico.getCities()
        for (city in cities) {
            if (city.name == selectedCity) {
                val properties = findLocationInCityAreas(city, loc)
                if (properties != null) {
                    val freeDeliveryPrice = properties["free_delivery_price"]?.toIntOrNull() ?: -1
                    val deliveryPrice = properties["delivery_price"]?.toIntOrNull() ?: -1
                    return AddressProperty(urlico.id, urlico.officialName, freeDeliveryPrice, deliveryPrice)
                }
            }
        }
        return null
    }

    private fun findLocationInCityAreas(city: Gorod, loc: Location): HashMap<String, String>? {
        //Log.d("logtag", "findLocationInCityAreas(city: ${city.name}, loc: $loc)")
        if (city == null) return null
        for (terminal in city.getTerminals()) {
            val properties = findLocationInTerminalAreas(terminal, loc)
            if (properties != null) {
                return properties
            }
        }
        return null
    }

    private fun findLocationInTerminalAreas(terminal: Terminal, loc: Location): HashMap<String, String>? {
        //Log.d("logtag", "findLocationInTerminalAreas(terminal: ${terminal.terminalId}, loc: $loc)")
        if (terminal == null) return null
        for (area in terminal.getAreas()) {
            if (area.isLocationInsideThis(loc)) {
                //Log.d("logtag", "findLocationInTerminalAreas() terminal: ${terminal.terminalId}")
                return hashMapOf("free_delivery_price" to area.freeDeliveryPrice.toString(), "delivery_price" to area.deliveryPrice.toString())
            }
        }
        return null
    }

    private fun getUrlicaForCity(selectedCity: String): List<Urlico> {
        //Log.d("logtag", "getUrlicaForCity(selectedCity: $selectedCity)")
        val resultList = ArrayList<Urlico>()
        if (urlica == null || urlica!!.isEmpty())
            return resultList
        for (ul in urlica!!) {
            for (city in ul.getCities()) {
                if (selectedCity == city.name && !resultList.contains(ul)) {
                    resultList.add(ul)
                    continue
                }
            }
        }
        return resultList
    }

    fun getUrlicoOfficialName(urlico: String): String {
        if (!urlica.isNullOrEmpty()) {
            for (ul in urlica!!) {
                if (ul.id == urlico) {
                    return ul.officialName
                }
            }
        }
        return ""
    }
}

data class AddressProperty(val urlicoName: String, val urlicoOfficialName: String, val freeDeliveryPrice: Int, val deliveryPrice: Int)
