package ru.totopizza.to_to.business

import ru.totopizza.to_to.Location
import java.lang.Exception

public class Urlico(val id: String) {
    var officialName = ""
    private val cities = ArrayList<Gorod>()

    fun getGorod(cityName: String): Gorod {
        if (!containsGorod(cityName)) {
            return createGorod(cityName)
        }
        for (addedCity in cities) {
            if (cityName == addedCity.name) {
                return addedCity
            }
        }
        return createGorod(cityName)
    }

    private fun containsGorod(cityName: String): Boolean {
        if (cityName.isEmpty() || cities.isEmpty())
            return false
        for (addedCity in cities) {
            if (cityName == addedCity.name) {
                return true
            }
        }
        return false
    }

    private fun createGorod(cityName: String): Gorod {
        val newGorod = Gorod(cityName)
        cities.add(newGorod)
        return newGorod
    }

    fun getCities() : List<Gorod> {
        return cities
    }
}

public class Gorod(val name: String) {
    private val terminals = ArrayList<Terminal>()

    fun getTerminal(terminalId: String): Terminal {
        if (!containsTerminal(terminalId)) {
            return createTerminal(terminalId)
        }
        for (terminal in terminals) {
            if (terminalId == terminal.terminalId) {
                return terminal
            }
        }
        return createTerminal(terminalId)
    }

    private fun containsTerminal(terminalId: String): Boolean {
        if (terminalId.isEmpty() || terminals.isEmpty())
            return false
        for (terminal in terminals) {
            if (terminalId == terminal.terminalId) {
                return true
            }
        }
        return false
    }

    private fun createTerminal(terminalId: String): Terminal {
        val newTerminal = Terminal(terminalId)
        terminals.add(newTerminal)
        return newTerminal
    }

    fun getTerminals() : List<Terminal> {
        return terminals;
    }
}

public class Terminal(val terminalId: String) {
    private val points = ArrayList<Point>()
    private val areas = ArrayList<Area>()

    fun addGeometry(properties: ServerCityData.Properties, geometry: ServerCityData.Geometry) {
        val coordinates = geometry.coordinates
        if (geometry.type == ServerCityData.Geometry.pointType && !containsPoint(properties.description)) {
            if (coordinates.isNotEmpty() && coordinates.count() == 2) {
                val lat = coordinates[0] as Double
                val lon = coordinates[1] as Double
                val point = Point(Location(lat, lon), properties.description)
                points.add(point)
            }
        } else if (geometry.type == ServerCityData.Geometry.polygonType && !containsPolygon(properties.description)) {
            if (coordinates.isNotEmpty()) {
                for (c1 in coordinates) {
                    try {
                        val pointsList = c1 as List<List<Double>>
                        val locationList = convertToLocations(pointsList)
                        val area = Area(properties.free_delivery_price, properties.delivery_price, properties.description, locationList)
                        areas.add(area)
                    }catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }

    private fun convertToLocations(pointsList: List<List<Double>>): List<Location> {
        val resultList = ArrayList<Location>()
        if (pointsList.isEmpty()) return resultList
        for (point in pointsList) {
            if (point.count() == 2) {
                resultList.add(Location(point[1], point[0]))
            }
        }
        return resultList
    }

    private fun containsPoint(description: String): Boolean {
        if (description == null)
            return false
        if (description.isEmpty() || points.isEmpty())
            return false
        for (point in points) {
            if (description == point.description) {
                return true
            }
        }
        return false
    }

    private fun containsPolygon(description: String): Boolean {
        if (description.isEmpty() || areas.isEmpty())
            return false
        for (area in areas) {
            if (description == area.description) {
                return true
            }
        }
        return false
    }

    fun getAreas() : List<Area> {
        return areas
    }
}

data class Point(val loc: Location, val description: String?)

data class Area(val freeDeliveryPrice: Int, val deliveryPrice: Int, val description: String, val polygon: List<Location>) {
    fun isLocationInsideThis(loc: Location): Boolean {
        if (polygon.count() < 3)
            return false
        val firstPoint = polygon.first()
        var lastLat = firstPoint.lat - loc.lat
        var lastLong = firstPoint.lon - loc.lon
        var sum = 0.0
        val endPosition = polygon.count() - 1
        for (i in 1..endPosition) {
            val currentLong = polygon[i].lon - loc.lon
            val currentLat = polygon[i].lat - loc.lat
            val del = lastLong * currentLat - lastLat * currentLong
            val xy = lastLong * currentLong + lastLat * currentLat
            sum += Math.atan((lastLat * lastLat + lastLong * lastLong - xy) / del) + Math.atan((currentLat * currentLat + currentLong * currentLong - xy) / del)

            lastLong = currentLong
            lastLat = currentLat
        }
        if (Math.abs(sum) > 0.000001) {
            return true
        }
        return false
    }
}

/*
Urlico {
    имя:
    Реквизиты:
    Cities{
        city:
            Terminals {
                id:
                Points {

                }
                Areas {
                    free_delivery_price:
                    delivery_price:
                    polygon [locations]
                }
            }
    }
}

if ($payment_urlico == "urlico1") {
  $params['user_name'] = 'totopizza5-api';
  $params['password'] = '$O35p54A$';
}
elseif ($payment_urlico == "urlico2") {
  $params['user_name'] = 'totopizza_2-api';
  $params['password'] = '%9R5e4D%';
}
elseif ($payment_urlico == "urlico3") {
  $params['user_name'] = 'totopizza6-api';
  $params['password'] = '$O35p54A$';
}
elseif ($payment_urlico == "urlico4") {
  $params['user_name'] = 'totopizza2-api';
  $params['password'] = '$O35p54A$';
}
elseif ($payment_urlico == "urlico5") {
  $params['user_name'] = 'totopizza3-api';
  $params['password'] = '$O35p54A$';
}
elseif ($payment_urlico == "urlico6") {
  $params['user_name'] = 'totopizza-api';
  $params['password'] = '%9R5e4D%';
}
elseif ($payment_urlico == "urlico7") {
  $params['user_name'] = 'totopizza1-api';
  $params['password'] = '%9R5e4D%';
}
elseif ($payment_urlico == "urlico8") {
  $params['user_name'] = 'totopizza_3-api';
  $params['password'] = '$O35p54A$';
}
*/
