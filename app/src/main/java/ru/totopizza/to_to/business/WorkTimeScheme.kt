package ru.totopizza.to_to.business

class WorkTimeScheme(private val workScheme: Map<String, Map<String, Map<String, String>>>) {

    public fun getTimeByParam(city: String, dayOfWeek: String, parameterType: String): Int {
        if (checkParameterType(parameterType)) return -1
        val cityMap = workScheme[city] ?: return -1
        val dayMap = cityMap[dayOfWeek] ?: return -1
        val startHourStr = dayMap[parameterType]
        if (startHourStr != null) {
            return try {
                startHourStr.toInt()
            } catch (e: NumberFormatException){
                e.printStackTrace()
                -1
            }
        }
        return -1
    }

    private fun checkParameterType(type: String): Boolean {
        return !(type == startHour ||
                type == startMin ||
                type == endHour ||
                type == endMin)
    }

    companion object {
        val startHour = "startHour"
        val startMin = "startMin"
        val endHour = "endHour"
        val endMin = "endMin"
    }
}