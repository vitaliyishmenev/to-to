package ru.totopizza.to_to.business

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import ru.totopizza.to_to.Address
import ru.totopizza.to_to.App
import java.io.IOException

class UserManager() {
    private val logtag: String = "logtag"
    private var prefs: SharedPreferences? = null
    private val customerPhoneKey = "customer_phone"
    private val customerNameKey = "customer_name"
    private val customerLoggedInKey = "customer_logged_in"
    private val phoneConfirmedKey = "phone_confirmed"

    private var user: User? = null
    private var editUser = EditUser("", "", "", "")
    private var userHistory: ArrayList<UserHistoryItem>? = null
    private var userPhone: String = ""
    private var phoneConfirmed: Boolean = false
    private var userLoggedIn: Boolean = false

    fun load(context: Context) {
        if (prefs == null)
            prefs = context.getSharedPreferences("TTAppPrefs", AppCompatActivity.MODE_PRIVATE)

        userPhone = prefs?.getString(customerPhoneKey, "") ?: ""
        phoneConfirmed = prefs?.getBoolean(phoneConfirmedKey, false) ?: false
        userLoggedIn = prefs?.getBoolean(customerLoggedInKey, false) ?: false

        loadFromServer()
    }

    fun loadFromServer() {
        if (userPhone.isEmpty())
            return
        val clearedUserPhone = clearPhone(userPhone)
        if (clearedUserPhone.isEmpty())
            return
        editUser.phone = clearedUserPhone
        val sApi = ServerApi()
        loadUser(sApi, clearedUserPhone)
        loadUserHistory(sApi, clearedUserPhone)
    }

    fun reloadUser() {
        val clearedUserPhone = clearPhone(userPhone)
        if (clearedUserPhone.isEmpty())
            return
        loadUser(ServerApi(), clearedUserPhone)
    }

    fun reloadHistory() {
        val clearedUserPhone = clearPhone(userPhone)
        if (clearedUserPhone.isEmpty())
            return
        loadUserHistory(ServerApi(), clearedUserPhone)
    }

    private fun loadUser(sApi: ServerApi, phone: String) {
        sApi.getUser(phone, object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.d(logtag, "ServerApi userCallback onFailure")
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                val responseData = response.body?.string() ?: ""
                Log.d(logtag, "ServerApi userCallback onResponse responseData: $responseData")
                user = convertToUser(responseData)
                fillEditUser()
                App.addressManager.loadAddressList(false, false)
            }

            private fun fillEditUser() {
                if (user?.NAME != null && user!!.NAME.isNotEmpty()) {
                    editUser.name = user!!.NAME
                } else {
                    editUser.name = getName()
                }
                if (user?.PERSONAL_BIRTHDAY != null && user!!.PERSONAL_BIRTHDAY!!.isNotEmpty()) {
                    editUser.birthday = user!!.PERSONAL_BIRTHDAY!!
                }
                if (user?.EMAIL != null && user!!.EMAIL.isNotEmpty()) {
                    editUser.email = user!!.EMAIL
                }
            }

            private fun convertToUser(responseData: String): User? {
                return try {
                    val moshi = Moshi.Builder().build()
                    val jsonAdapter = moshi.adapter(User::class.java)
                    jsonAdapter.fromJson(responseData)
                } catch (e: Exception) {
                    e.printStackTrace()
                    null
                }
            }
        })
    }

    private fun loadUserHistory(sApi: ServerApi, phone: String) {
        Log.d(logtag, "loadUserHistory(phone: $phone)")
        sApi.getHistory(phone, object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.d(logtag, "ServerApi historyCallback onFailure")
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                val responseData = response.body?.string() ?: ""
                Log.d(logtag, "ServerApi historyCallback onResponse responseData: $responseData")
                try {
                    userHistory = convertToUserHistory(responseData)
                    Log.d(logtag, "ServerApi historyCallback userHistory: $userHistory")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            private fun convertToUserHistory(responseData: String): ArrayList<UserHistoryItem>? {
                val moshi = Moshi.Builder().build()
                val type = Types.newParameterizedType(List::class.java, UserHistoryItem::class.java)
                val adapter = moshi.adapter<List<UserHistoryItem>>(type)
                val list = adapter.fromJson(responseData)
                if (list != null)
                    return ArrayList(list)
                return null
            }
        })
    }

    private fun clearPhone(phone: String): String {
        var withoutPrefix = ""
        if (phone.startsWith("+7")) {
            withoutPrefix = phone.substringAfter("+7")
        } else if (phone.startsWith("8")) {
            withoutPrefix = phone.substringAfter("8")
        }
        val result = StringBuilder()
        for (c in withoutPrefix) {
            if (c.isDigit()) {
                result.append(c)
            }
        }
        return result.toString()
    }

    fun getName(): String {
        if (user?.NAME != null && user!!.NAME.isNotEmpty()) {
            return user!!.NAME
        }
        return prefs?.getString(customerNameKey, "") ?: ""
    }

    fun saveName(name: String) {
        editUser.name = name
        val prefEditor = prefs?.edit()
        prefEditor?.putString(customerNameKey, name)
        prefEditor?.apply()
    }

    fun getPhone(): String {
        return userPhone
    }

    fun getClearedPhone(): String {
        return clearPhone(userPhone)
    }

    fun savePhone(phone: String) {
        userPhone = phone
        val prefEditor = prefs?.edit()
        prefEditor?.putString(customerPhoneKey, userPhone)
        prefEditor?.apply()
    }

    fun getMail(): String {
        return user?.EMAIL ?: ""
    }

    fun saveMail(mail: String) {
        editUser.email = mail
    }

    fun savePhoneConfirmed(isConfirmed: Boolean) {
        phoneConfirmed = isConfirmed
        val prefEditor = prefs?.edit()
        prefEditor?.putBoolean(phoneConfirmedKey, phoneConfirmed)
        prefEditor?.apply()
    }

    fun isLoggedIn(): Boolean {
        return userLoggedIn
    }

    fun saveLoggedIn(isLoggedIn: Boolean) {
        userLoggedIn = isLoggedIn
        val prefEditor = prefs?.edit()
        prefEditor?.putBoolean(customerLoggedInKey, userLoggedIn)
        prefEditor?.apply()
    }

    fun getUserHistory(): ArrayList<UserHistoryItem>? {
        return userHistory
    }

    fun resetBirthday() {
        editUser.birthday = ""
    }

    fun getBirthdayIsSetted(): Boolean {
        val birthday: String? = user?.PERSONAL_BIRTHDAY
        return !birthday.isNullOrEmpty()
    }

    fun getBirthdayText(): String {
        if (!getBirthdayIsSetted())
            return ""
        val day = getBirthdayDay()
        val month = getBirthdayMonth()
        if (month < 1 || month > 12) {
            return ""
        }
        val monthStr = months[month]
        return "$day $monthStr"
    }

    fun getMonthByIndex(i: Int): String {
        if (i < 1 || i > 12) {
            return ""
        }
        return months[i]
    }


    private val months = arrayOf("","января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря")

    fun saveCustomerBirthday(year: Int, month: Int, day: Int) {
        editUser.birthday = "$day.$month.$year"
        saveUserToServer()
    }

    private val defaultYear = 2000
    fun getBirthdayYear(): Int {
        if (user?.PERSONAL_BIRTHDAY == null)
            return defaultYear
        if (!user!!.PERSONAL_BIRTHDAY!!.contains("."))
            return defaultYear

        val year = user!!.PERSONAL_BIRTHDAY!!.substringAfterLast(".")
        return if (!year.isNullOrEmpty())
            year.toInt()
        else
            defaultYear
    }

    private val defaultMonth = 1
    fun getBirthdayMonth(): Int {
        if (user?.PERSONAL_BIRTHDAY == null)
            return defaultMonth
        if (!user!!.PERSONAL_BIRTHDAY!!.contains("."))
            return defaultMonth

        val month = user!!.PERSONAL_BIRTHDAY!!.substringAfter(".").substringBefore(".")
        return if (!month.isNullOrEmpty())
            month.toInt()
        else
            defaultMonth
    }

    private val defaultDay = 1
    fun getBirthdayDay(): Int {
        if (user?.PERSONAL_BIRTHDAY == null)
            return defaultDay
        if (!user!!.PERSONAL_BIRTHDAY!!.contains("."))
            return defaultDay

        val day = user!!.PERSONAL_BIRTHDAY!!.substringBefore(".")
        return if (!day.isNullOrEmpty())
            day.toInt()
        else
            defaultDay
    }

    fun getUserId(): String {
        return user?.ID ?: ""
    }

    fun saveUserToServer() {
        val sApi = ServerApi()
        sApi.editUser(editUser, object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.d(logtag, "ServerApi editUserCallback onFailure")
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                val responseData = response.body?.string() ?: ""
                Log.d(logtag, "ServerApi editUserCallback onResponse responseData: $responseData")
                if (responseData == "true") {
                    reloadUser()
                }
            }
        })
    }

    fun getAddressList(city: String): ArrayList<Address> {
        val resultList = ArrayList<Address>()
        if (user?.LIST_ADRESS == null || user!!.LIST_ADRESS.isEmpty())
            return resultList

        for (address in user!!.LIST_ADRESS) {
            if (address.YANDEX == city) {
                val parser = StreetNameParser()
                val complexStreet = parser.parseStreetName(address.STREETORIGIN)
                val adr = Address(
                        address.NAME,
                        address.YANDEX,
                        address.CITYORIGIN,
                        address.CITY,
                        complexStreet.simpleName,
                        address.STREETID,
                        address.STREETORIGIN,
                        address.HOUSE,
                        address.FLATE,
                        address.ETAZH,
                        address.PODYEZD,
                        false,
                        "",
                        "",
                        complexStreet.delivery,
                        complexStreet.deliveryFree,
                        complexStreet.urlico,
                        address.ID)
                resultList.add(adr)
            }
        }
        return resultList
    }

}

data class User(
        val DATE_REGISTER: String,
        val EMAIL: String,
        val ID: String,
        val LAST_LOGIN: String,
        val LIST_ADRESS: List<ADRESS>,
        val NAME: String,
        val PERSONAL_PHONE: String,
        val PERSONAL_BIRTHDAY: String?
)

data class ADRESS(
        val CITY: String,
        val CITYORIGIN: String,
        val ETAZH: String,
        val FLATE: String,
        val HOUSE: String,
        val ID: String,
        val NAME: String,
        val NAME_ADRESS: String,
        val PODYEZD: String,
        val STREETID: String,
        val STREETORIGIN: String,
        val YANDEX: String
)

data class EditUser(
        var phone: String,
        var name: String,
        var email: String,
        var birthday: String
)

data class UserHistoryItem(
        val ADDITIONAL_INFO: String,
        val DATE_INSERT: String,
        val DELIVERY_ID: String,
        val ID: String,
        val PAY_SYSTEM_ID: String,
        val PRICE: String,
        val PRODUCT_ITEM: List<PRODUCTITEM>,
        val USER_EMAIL: String
)

data class PRODUCTITEM(
        val CALLBACK_FUNC: Any,
        val ID: String,
        val NAME: String,
        val PRICE: String,
        val PRODUCT_ID: String,
        val QUANTITY: Int
)