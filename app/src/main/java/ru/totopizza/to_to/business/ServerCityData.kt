package ru.totopizza.to_to.business

public class ServerCityData(
        val features: List<Feature>,
        val metadata: Metadata,
        val type: String) {

    data class Feature(
            val geometry: Geometry,
            val id: Int,
            val properties: Properties,
            val type: String                //Point - рестораны, Polygon - зона доставки
    )

    data class Metadata(
            val creator: String,
            val description: String,
            val name: String
    )

    data class Geometry(
            val coordinates: List<Any>,
            val type: String
    ) {
        companion object {
            val polygonType = "Polygon"
            val pointType = "Point"
        }
    }

    data class Properties(
            val delivery_price: Int,
            val description: String,
            val fill: String,
            val fill_opacity: Double,
            val free_delivery_price: Int,
            val iconCaption: String,
            val marker_color: String,
            val payment_urlico: String,
            val stroke: String,
            val stroke_opacity: Int,
            val stroke_width: String,
            val terminal_id: String
    )
}