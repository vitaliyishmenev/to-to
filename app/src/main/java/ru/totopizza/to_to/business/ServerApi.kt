package ru.totopizza.to_to.business
import android.util.Log
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import okhttp3.*
import okhttp3.OkHttpClient
//import okhttp3.logging.HttpLoggingInterceptor
import org.apache.http.conn.ssl.SSLSocketFactory.SSL
import org.jetbrains.anko.doAsync
import ru.totopizza.to_to.Address
import java.io.IOException
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

class ServerApi {
    private val logtag: String = "logtag"
    private var client: OkHttpClient
    private val apiKey = "fa9sgwlgjs9gdsjlgjdsjglsdlgs"
    private val baseUrl = "https://totopizza.ru/"
    private val getUserUrl = baseUrl + "ajax/api.php"
    private val addOrderUrl = baseUrl + "ajax/api_android_addorder.php"
    private val editUserUrl = baseUrl + "ajax/api_edituser.php"
    private val addressUrl = baseUrl + "ajax/api_adress.php"
    private val fullDataUrl = baseUrl + "zones/full_data.json"
    private val baseUrlWithKey = "$getUserUrl?key=$apiKey"
    private val getUserAction = "&action=getuser"
    private val getHistoryAction = "&action=gethistory"

    init {
        client = getUnsafeOkHttpClient()
        //client = OkHttpClient()
    }

    private fun getUnsafeOkHttpClient(): OkHttpClient {
        val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
            override fun checkClientTrusted(chain: Array<out X509Certificate>?, authType: String?) {
            }

            override fun checkServerTrusted(chain: Array<out X509Certificate>?, authType: String?) {
            }

            override fun getAcceptedIssuers() = arrayOf<X509Certificate>()
        })
        val sslContext = SSLContext.getInstance(SSL)
        sslContext.init(null, trustAllCerts, SecureRandom())
        val sslSocketFactory = sslContext.socketFactory

        return OkHttpClient.Builder()
                .sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
                .hostnameVerifier(HostnameVerifier { _, _ -> true })
                //.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build()
    }

    fun getUser(phone: String, callback: Callback) {
        doAsync {
            enqueueGetUserCall(phone, callback)
        }
    }

    private fun enqueueGetUserCall(phone: String, callback: Callback) {
        Log.d(logtag, "ServerApi enqueueGetUserCall(phone: $phone)")
        val request = Request.Builder()
                .url("$baseUrlWithKey$getUserAction&phone=$phone")
                .build()

        val call = client.newCall(request)
        call.enqueue(callback)
    }


    fun editUser(user: EditUser, callback: Callback) {
        doAsync {
            enqueueEditUserCall(user, callback)
        }
    }

    private fun enqueueEditUserCall(user: EditUser, callback: Callback) {
        Log.d(logtag, "ServerApi enqueueEditUserCall(user.phone: ${user.phone})")
        val requestBody: RequestBody = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("key", apiKey)
                .addFormDataPart("action", "edituser")
                .addFormDataPart("phone", user.phone)
                .addFormDataPart("name", user.name)
                .addFormDataPart("email", user.email)
                .addFormDataPart("birthday", user.birthday)
                .build()
        val request = Request.Builder()
                .url(editUserUrl)
                .addHeader("content-type", "application/json; charset=utf-8")
                .post(requestBody)
                .build()

        val call = client.newCall(request)
        call.enqueue(callback)
    }

    fun getHistory(phone: String, callback: Callback) {
        doAsync {
            enqueueGetHistoryCall(phone, callback)
        }
    }

    private fun enqueueGetHistoryCall(phone: String, callback: Callback) {
        val request = Request.Builder()
                .url("$baseUrlWithKey$getHistoryAction&phone=$phone")
                .build()

        val call = client.newCall(request)
        call.enqueue(callback)
    }


    fun sendOrder(phone: String, city: String, products: List<ProductListItem>, deliveryPrice: String, totalPrice: String, callback: Callback) {
        doAsync {
            enqueueOrder(phone, city, products, deliveryPrice, totalPrice, callback)
        }
    }

    private fun enqueueOrder(phone: String, city: String, products: List<ProductListItem>, deliveryPrice: String, totalPrice: String, callback: Callback) {
        val productsArrays = convertToListMap(products)
        val requestBody: RequestBody = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("key", apiKey)
                .addFormDataPart("phone", phone)
                .addFormDataPart("city", city)
                .addFormDataPart("product", productsArrays)
                .addFormDataPart("deliveryprice", deliveryPrice)
                .addFormDataPart("totalprice", totalPrice)
                .build()
        val request: Request = Request.Builder()
                .url(addOrderUrl)
                .post(requestBody)
                .addHeader("content-type", "application/json; charset=utf-8")
                .build()
        client.newCall(request).enqueue(callback)
    }

    private fun convertToListMap(products: List<ProductListItem>): String {
        val mapList = ArrayList<Map<String, Any>>()
        for (product in products) {
            val productMap = mapOf(
                    "xml_id" to product.xml_id,
                    "quantity" to product.quantity,
                    "productModifier" to product.productModifier
            )
            mapList.add(productMap)
        }

        val type = Types.newParameterizedType(List::class.java, Any::class.java)
        val adapter = Moshi.Builder().build().adapter<List<Any>>(type)
        return adapter.toJson(mapList)
    }

    fun loadCitiesInfo(callback: Callback) {
        doAsync {
            executeLoadCitiesInfo(callback)
        }
    }

    private fun executeLoadCitiesInfo(callback: Callback) {
        val request = Request.Builder()
                .url(fullDataUrl)
                .build()
        val call = client.newCall(request)
        try {
            call.enqueue(callback)
        } catch (e: IOException) {
            e.printStackTrace()
            callback.onFailure(call, e)
        }
    }

    fun addNewAddress(address: Address, userId: String) {
        doAsync {
            enqueueAddNewAddress(address, userId, object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Log.d(logtag, "ServerApi addNewAddressCallback onFailure")
                    e.printStackTrace()
                }

                override fun onResponse(call: Call, response: Response) {
                    val responseData = response.body?.string() ?: ""
                    Log.d(logtag, "ServerApi addNewAddressCallback onResponse responseData: $responseData")
                    if (!responseData.isNullOrEmpty()) {
                        address.addressId = responseData
                    }
                }
            })
        }
    }

    private fun enqueueAddNewAddress(address: Address, userId: String, callback: Callback) {
        val requestBody: RequestBody = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("key", apiKey)
                .addFormDataPart("ADRESS", "new")
                .addFormDataPart("YANDEX", address.city)                //это поле необходимо брать из iiko aditional, есть у каждого города в апи, если нет такой возможности ставим просто название города оригинал
                .addFormDataPart("STREETORIGIN", address.streetIiko)        //название улицы, оригинальное из иико
                .addFormDataPart("NEW_HOUSE", address.home)
                .addFormDataPart("NEW_FLATE", address.apartment)
                .addFormDataPart("PODYEZD", address.entrance)
                .addFormDataPart("KOD_DVERI", address.entranceCod)
                .addFormDataPart("ETAZH", address.floor)
                .addFormDataPart("ADRESS_NEW", address.name)            //название адреса
                .addFormDataPart("ADRESS_COMMENT", address.comment)     //комментарий к адрессу
                .addFormDataPart("PROFILE", userId)                     //ид юзера
                .addFormDataPart("USER_ID", userId)
                .addFormDataPart("CITY", address.suburban)              //название города, оригинал из иико
                .addFormDataPart("CITYORIGIN", address.city)
                .addFormDataPart("STREETID", address.streetId)          //ид улицы из иико
                .build()
        val request: Request = Request.Builder()
                .url(addressUrl)
                .post(requestBody)
                .addHeader("content-type", "application/json; charset=utf-8")
                .build()
        client.newCall(request).enqueue(callback)
    }

    fun editAddress(address: Address, userId: String) {
        doAsync {
            enqueueEditAddress(address, userId, object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Log.d(logtag, "ServerApi editAddressCallback onFailure")
                    e.printStackTrace()
                }

                override fun onResponse(call: Call, response: Response) {
                    val responseData = response.body?.string() ?: ""
                    Log.d(logtag, "ServerApi editAddressCallback onResponse responseData: $responseData")
                }
            })
        }
    }

    private fun enqueueEditAddress(address: Address, userId: String, callback: Callback) {
        val requestBody: RequestBody = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("key", apiKey)
                .addFormDataPart("ADRESS", "edit")
                .addFormDataPart("YANDEX", address.city)                //это поле необходимо брать из iiko aditional, есть у каждого города в апи, если нет такой возможности ставим просто название города оригинал
                .addFormDataPart("STREETORIGIN", address.streetIiko)        //название улицы, оригинальное из иико
                .addFormDataPart("NEW_HOUSE", address.home)
                .addFormDataPart("NEW_FLATE", address.apartment)
                .addFormDataPart("PODYEZD", address.entrance)
                .addFormDataPart("KOD_DVERI", address.entranceCod)
                .addFormDataPart("ETAZH", address.floor)
                .addFormDataPart("ADRESS_NEW", address.name)            //название адреса
                .addFormDataPart("ADRESS_COMMENT", address.comment)     //комментарий к адрессу
                .addFormDataPart("PROFILE", userId)                     //ид юзера
                .addFormDataPart("USER_ID", userId)
                .addFormDataPart("CITYORIGIN", address.cityId)          //название города, оригинал из иико
                .addFormDataPart("STREETID", address.streetId)          //ид улицы из иико
                .addFormDataPart("ADRESS_ID", address.addressId)
                .build()
        val request: Request = Request.Builder()
                .url(addressUrl)
                .addHeader("content-type", "application/json; charset=utf-8")
                .post(requestBody)
                .build()
        client.newCall(request).enqueue(callback)
    }

    fun deleteAddress(address: Address, userId: String) {
        doAsync {
            enqueueDeleteAddress(address, userId, object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Log.d(logtag, "ServerApi deleteAddressCallback onFailure")
                    e.printStackTrace()
                }

                override fun onResponse(call: Call, response: Response) {
                    val responseData = response.body?.string() ?: ""
                    Log.d(logtag, "ServerApi deleteAddressCallback onResponse responseData: $responseData")
                }
            })
        }
    }

    private fun enqueueDeleteAddress(address: Address, userId: String, callback: Callback) {
        val requestBody: RequestBody = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("key", apiKey)
                .addFormDataPart("ADRESS", "delete")
                .addFormDataPart("YANDEX", address.city)                //это поле необходимо брать из iiko aditional, есть у каждого города в апи, если нет такой возможности ставим просто название города оригинал
                .addFormDataPart("STREETORIGIN", address.streetIiko)        //название улицы, оригинальное из иико
                .addFormDataPart("NEW_HOUSE", address.home)
                .addFormDataPart("NEW_FLATE", address.apartment)
                .addFormDataPart("PODYEZD", address.entrance)
                .addFormDataPart("KOD_DVERI", address.entranceCod)
                .addFormDataPart("ETAZH", address.floor)
                .addFormDataPart("ADRESS_NEW", address.name)            //название адреса
                .addFormDataPart("ADRESS_COMMENT", address.comment)     //комментарий к адрессу
                .addFormDataPart("PROFILE", userId)                     //ид юзера
                .addFormDataPart("USER_ID", userId)
                .addFormDataPart("CITYORIGIN", address.cityId)          //название города, оригинал из иико
                .addFormDataPart("STREETID", address.streetId)          //ид улицы из иико
                .addFormDataPart("ADRESS_ID", address.addressId)
                .build()
        val request: Request = Request.Builder()
                .url(addressUrl)
                .addHeader("content-type", "application/json; charset=utf-8")
                .post(requestBody)
                .build()
        client.newCall(request).enqueue(callback)
    }
}

data class ProductListItem(
        val quantity: Int,
        val xml_id: String,
        val productModifier: String
)