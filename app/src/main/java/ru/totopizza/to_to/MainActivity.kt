package ru.totopizza.to_to

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.util.AttributeSet

import kotlinx.android.synthetic.main.activity_main.*

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.widget.TextView
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentManager
import com.onesignal.OneSignal
import q.rorbin.badgeview.QBadgeView
import ru.totopizza.to_to.business.CitiesInfo
import ru.totopizza.to_to.business.UserManager

class MainActivity : BaseActivity(), PromoFragment.OnListFragmentInteractionListener {
    private var active = Fragment()
    private var main = MainFragment()
    private var promo = PromoFragment()
    private var cart = OrdersFragment()
    private var cafes = CafesFragment()
    private var about = AboutFragment()
    private var cartBadge: QBadgeView? = null

    override val noMenu: Boolean
        get() = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init()

        this.title = ""
        toolbar_title.text = "Меню"

        logo.visibility = View.VISIBLE
        toolbar_title.visibility = View.GONE
        showRestList.visibility = View.GONE

        showRestList.setOnClickListener { view: View? ->
            val detailActivity = Intent(this, RestaurantsActivity::class.java)
            this.startActivity(detailActivity)
        }

        this.supportFragmentManager.beginTransaction().add(R.id.main_container, main).commit()
        this.supportFragmentManager.beginTransaction().add(R.id.main_container, promo).hide(promo).commit()
        this.supportFragmentManager.beginTransaction().add(R.id.main_container, cart).hide(cart).commit()
        this.supportFragmentManager.beginTransaction().add(R.id.main_container, cafes).hide(cafes).commit()
        this.supportFragmentManager.beginTransaction().add(R.id.main_container, about).hide(about).commit()

        navigation.onNavigationItemSelectedListener = mOnNavigationItemSelectedListener
        navigation.enableAnimation(false)
        navigation.enableShiftingMode(false)
        navigation.enableItemShiftingMode(false)

        active = main

        IikoAPI.onGoToCart = {
            navigation.selectedItemId = navigation.menu.getItem(2).itemId
        }

        clearcart.setOnClickListener {
            showClearCartConfirmationDialog()
        }

        CitiesInfo.instance.loadData(this)
        loadData()
        val prefs = getSharedPreferences("TTAppPrefs", MODE_PRIVATE)
        IikoAPI.loadCart(prefs)

        navigation.menu.getItem(0).setOnMenuItemClickListener {
            if (active == main) {
                main.scrollToStart()
                return@setOnMenuItemClickListener true
            }
            return@setOnMenuItemClickListener false
        }

        cartBadge = QBadgeView(this)
        cartBadge?.bindTarget(navigation.getBottomNavigationItemView(2))
        cartBadge?.badgeBackgroundColor = resources.getColor(R.color.colorPrimary)
        cartBadge?.isShowShadow = false
    }

    private fun showClearCartConfirmationDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        builder.setTitle("Очистить корзину?")
        builder.setMessage("Все товарый в Вашей корзине будут удалены")
        builder.setPositiveButton("ОК") { _, _ -> onClearCart()}
        builder.setNegativeButton("Отмена") { dialog, _ -> dialog.dismiss()}
        builder.show()
    }

    private fun onClearCart(){
        IikoAPI.clearCart()
        cart.onResume()
        updateCartCount()
        navigation.menu.getItem(2).title = resources.getString(R.string.title_orders)
        cartBadge?.hide(true)
    }

    private fun loadData() {
        CitiesInfo.instance.loadSelectedCity(this)
        val selectedCityId = CitiesInfo.instance.getSelectedCityId()
        App.addressManager.setCityId(selectedCityId)
        App.addressManager.clearLocalAddress()
        if (selectedCityId.isEmpty()) {
            val builder = AlertDialog.Builder(this)

            builder.setTitle("Выберите город доставки")
            builder.setItems(CitiesInfo.availableCities) { _, i ->
                val newCityId = CitiesInfo.citiesIds[i]
                CitiesInfo.instance.setSelectedCity(newCityId, this)
                App.addressManager.setCityId(newCityId)
                main.setCityToToolbar(CitiesInfo.availableCities[i])
                if (isNetworkConnected()) {
                    IikoAPI.getBanners {
                        main.updateBanners()
                    }
                }
            }
            builder.show()
        } else {
            if (isNetworkConnected()) {
                IikoAPI.getBanners {
                    main.updateBanners()
                }
            }
        }

        if (isNetworkConnected()) {
            IikoAPI.getToken { token ->
                println("Token: $token")
                IikoAPI.getOrganisationList(token) { orgId ->
                    println("Organization ID: $orgId")

                    IikoAPI.getNomenclature(token, orgId) { result ->
                        this@MainActivity.runOnUiThread {
                            main.updateCategoryList()
                        }
                    }
                    IikoAPI.getCitiesAndStreets(token, orgId) { s ->
                    }

                    IikoAPI.getPaymentTypes(token, orgId)
                }
            }

        } else {
            AlertDialog.Builder(this).setTitle("Нет соединения с интернетом")
                    .setMessage("Проверьте ваше интернет соединение и попробуйте снова")
                    .setPositiveButton(android.R.string.ok) { dialog, which -> }
                    .setIcon(android.R.drawable.ic_dialog_alert).show()
        }
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        toolbar_title.text = item.title
        toolbar_title.visibility = View.VISIBLE
        logo.visibility = View.GONE
        showRestList.visibility = View.GONE
        appBarLayout.visibility = View.GONE
        clearcart.visibility = View.GONE

        val fm = this.supportFragmentManager

        when (item.itemId) {
            R.id.navigation_menu -> {
                openMainFragment(fm)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_promo -> {
                openPromoFragment(fm)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_cafes -> {
                openCafesFragment(fm)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_orders -> {
                openCartFragment(fm)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_about -> {
                openAboutFragment(fm)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun openMainFragment(fm: FragmentManager) {
        logo.visibility = View.VISIBLE
        toolbar_title.visibility = View.GONE
        fm.beginTransaction().hide(active).show(main).commit()
        active = main
    }

    private fun openPromoFragment(fm: FragmentManager) {
        appBarLayout.visibility = View.VISIBLE
        toolbar_title.visibility = View.VISIBLE
        fm.beginTransaction().hide(active).show(promo).commit()
        active = promo
    }

    private fun openCafesFragment(fm: FragmentManager) {
        appBarLayout.visibility = View.VISIBLE
        showRestList.visibility = View.VISIBLE
        toolbar_title.visibility = View.VISIBLE
        fm.beginTransaction().hide(active).show(cafes).commit()
        active = cafes
    }

    private fun openCartFragment(fm: FragmentManager) {
        logo.visibility = View.GONE
        showRestList.visibility = View.GONE
        appBarLayout.visibility = View.VISIBLE
        toolbar_title.visibility = View.VISIBLE
        toolbar_title.text = "Корзина"
        clearcart.visibility = View.VISIBLE
        cart.onResume()
        fm.beginTransaction().hide(active).show(cart).commit()
        active = cart
    }

    private fun openAboutFragment(fm: FragmentManager) {
        appBarLayout.visibility = View.VISIBLE
        toolbar_title.visibility = View.VISIBLE
        toolbar_title.text = "Информация"
        fm.beginTransaction().hide(active).show(about).commit()
        active = about
    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager// 1
        val networkInfo = connectivityManager.activeNetworkInfo // 2
        return networkInfo != null && networkInfo.isConnected // 3
    }

    override fun onListFragmentInteraction(item: Banner) {
        val detailActivity = Intent(this, PromoActivity::class.java)
        detailActivity.putExtra("url", item.image)
        detailActivity.putExtra("title", item.title)
        detailActivity.putExtra("description", item.description)
        startActivity(detailActivity)
    }

    override fun onResume() {
        super.onResume()
        val layout = findViewById<ConstraintLayout>(R.id.container_main_activity)
        layout.setBackgroundColor(Color.WHITE)

        if (intent != null) {
            val openCart = intent.getBooleanExtra("open_cart", false)
            if (openCart) {
                //openCartFragment(supportFragmentManager)
                navigation.selectedItemId = navigation.menu.getItem(2).itemId
            }
        }
    }

    override fun updateCartCount() {
        super.updateCartCount()
        cart.updateCartCount()
        val cartItem = navigation.menu.getItem(2)
        val cartPrice = IikoAPI.getCartPrice()
        val cartCount = IikoAPI.getCartCount()
        if (cartPrice > 0) {
            val newTitle = "$cartPrice \u20BD"
            cartItem.title =  newTitle
            cartBadge?.badgeNumber = cartCount
        }
        else {
            cartItem.title = resources.getString(R.string.title_orders)
            cartBadge?.hide(true)
        }
    }
}

class WrapContentViewPager(private val getContext: Context, private val attrs: AttributeSet) : ViewPager(getContext, attrs) {
    private var mCurrentPagePosition: Int = 0

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var newHeightMeasureSpec = heightMeasureSpec
        val child = getChildAt(mCurrentPagePosition)
        if (child != null) {
            child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED))
            val h = child.measuredHeight - 10.dpToPx(getContext)
            newHeightMeasureSpec = MeasureSpec.makeMeasureSpec(h, MeasureSpec.EXACTLY)
        }

        super.onMeasure(widthMeasureSpec, newHeightMeasureSpec)
    }
}

open class BaseActivity : AppCompatActivity() {

    open val showCartMenu = true
    open val noMenu = false

    private val CALL_REQUEST = 100

    var _cartCountLabel: TextView? = null

    private var _lastPhoneNumber: String = ""

    override fun finish() {
        super.finish()
        overridePendingTransitionExit()
    }

    override fun startActivity(intent: Intent) {
        super.startActivity(intent)
        overridePendingTransitionEnter()
    }

    override fun onResume() {
        super.onResume()

        updateCartCount()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    /*override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }*/

    /*override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/")
                .setFontAttrId(R.attr.fontPath)
                .build()
        )
    }*/

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    protected fun overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    protected fun overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        //Log.d("logtag", "BaseActivity onCreateOptionsMenu() noMenu: $noMenu")
        if (noMenu) {
            return super.onCreateOptionsMenu(menu)
        }

        //Log.d("logtag", "BaseActivity onCreateOptionsMenu() showCartMenu: $showCartMenu")
        if (showCartMenu) {
            menuInflater.inflate(R.menu.menu_actionbar, menu)
            var item = menu?.findItem(R.id.menu_hotlist)
            val menu_hotlist = item?.actionView
            _cartCountLabel = menu_hotlist?.findViewById(R.id.hotlist_hot)
            menu_hotlist?.setOnClickListener {
                var detailActivity = Intent(this, CartActivity::class.java)
                this.startActivity(detailActivity)
            }

            updateCartCount()
        } else {
            menuInflater.inflate(R.menu.menu_cart, menu)
            val item = menu?.findItem(R.id.menu_clear_cart)
            item?.isVisible = IikoAPI.cart.count() > 0
        }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_clear_cart) {
            IikoAPI.cart.clear()
            this.updateCartCount()
            this.onCartClear()
        }
        return super.onOptionsItemSelected(item)
    }

    open fun onCartClear() {

    }

    open fun updateCartCount() {
        var count = IikoAPI.getCartCount()
        this.runOnUiThread {
            if (count == 0)
                _cartCountLabel?.visibility = View.INVISIBLE
            else
            {
                _cartCountLabel?.visibility = View.VISIBLE
                _cartCountLabel?.text = count.toString()
            }
        }
    }

    fun makeCallOn(number: String) {
        _lastPhoneNumber = number
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.CALL_PHONE), CALL_REQUEST)
                    return
                }
            }

            val callIntent = Intent(Intent.ACTION_CALL)
            callIntent.data = Uri.parse("tel:" + number)
            startActivity(callIntent)
        } catch (ex: Exception) {
            ex.printStackTrace()
            Toast.makeText(applicationContext, "Ошибка вызова: " + ex.message, Toast.LENGTH_LONG).show()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CALL_REQUEST) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                makeCallOn(_lastPhoneNumber)
            } else {
                Toast.makeText(this, resources.getString(R.string.call_permission_denied_message), Toast.LENGTH_SHORT).show();
            }
        }
    }

    companion object {
        private val vibrateLength = 100L
        fun vibrate(context: Context?) {
            if (context == null) return

            val v = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                v.vibrate(VibrationEffect.createOneShot(vibrateLength, VibrationEffect.DEFAULT_AMPLITUDE))
            } else {
                v.vibrate(vibrateLength)
            }
        }
    }
}

fun Int.dpToPx(context: Context): Int {
    val displayMetrics = context.resources.displayMetrics
    return Math.round(this * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
}
fun Int.pxToDp(context: Context): Int {
    val displayMetrics = context.resources.displayMetrics
    return Math.round(this / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
}