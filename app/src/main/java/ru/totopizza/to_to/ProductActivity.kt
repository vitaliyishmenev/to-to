package ru.totopizza.to_to

import android.graphics.Color
import android.os.Bundle
import android.view.View
import com.squareup.picasso.Picasso

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.viewpager.widget.ViewPager
import android.widget.*
import kotlinx.android.synthetic.main.activity_product.*
import android.view.WindowManager
import android.os.Build
import android.app.Activity
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.widget.FrameLayout
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types

class ProductActivity : BaseActivity() {

    private var mProductCollectionPagerAdapter: ProductCollectionPagerAdapter? = null
    private var mViewPager: ViewPager? = null
    private var products: ArrayList<Product> = ArrayList()

    private var productID: String = ""
    private var currentItemId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBarColored(this)
        setContentView(R.layout.activity_product)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        android_appbar_layout.bringToFront()
        supportActionBar?.title = ""
        productID = intent.getStringExtra("id") ?: ""
        loadData()
        mProductCollectionPagerAdapter = ProductCollectionPagerAdapter(supportFragmentManager, products)
        mViewPager = findViewById(R.id.pager)
        mViewPager?.adapter = mProductCollectionPagerAdapter
        mViewPager?.currentItem = currentItemId
    }

    private fun loadData() {
        products.clear()
        for (group in IikoAPI.groups) {
            val catProducts = IikoAPI.getProducts(group.id)
            for (product in catProducts) {
                if (product.id == productID) {
                    currentItemId = products.count()
                }
                products.add(product)
            }
        }
    }

    fun setStatusBarColored(context: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val w = context.window
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            val statusBarHeight = getStatusBarHeight(context)

            val view = View(context)
            view.layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            view.layoutParams.height = statusBarHeight
            (w.decorView as ViewGroup).addView(view)
            view.setBackgroundColor(Color.parseColor("#33000000"))
        }
    }

    fun getStatusBarHeight(context: Activity): Int {
        var result = 0
        val resourceId = context.resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = context.resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    private fun loadFavorites(): java.util.ArrayList<Product> {
        val resultList = java.util.ArrayList<Product>()
        val prefs = getSharedPreferences("TTAppPrefs", MODE_PRIVATE)
        val addressListJson = prefs?.getString("favoriteProductList", null)
        if (addressListJson != null) {
            val moshi = Moshi.Builder().build()
            val type = Types.newParameterizedType(List::class.java, String::class.java)
            val adapter = moshi.adapter<List<String>>(type)
            val result = adapter.fromJson(addressListJson)
            if (result != null) {
                val loadedList = java.util.ArrayList(result)
                for (productId in loadedList) {
                    val product = IikoAPI.getProduct(productId)
                    if (product != null){
                        resultList.add(product)
                    }
                }
            }
        }
        return resultList
    }
}

class ProductFragment : Fragment() {
    private var _activity: BaseActivity? = null
    private var productID: String = ""
    private var isPizzaSize = false
    private var isPizzaPiquancy = false

    private var pizzaSizes: ArrayList<Product> = ArrayList()
    private var pizzaPiquancy: ArrayList<Product> = ArrayList()

    private var selectedSize: String? = null

    private var pizzaPhoto:ImageView? = null
    private var nonPizzaPhoto:ImageView? = null

    private var prefs: SharedPreferences? = null
    private var favoriteProductList = ArrayList<String>()
    private lateinit var favoriteButton: ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.content_product, container, false)

        _activity = activity as BaseActivity
        productID = arguments?.getString("productId")!!

        val product = IikoAPI.getProduct(productID)

        val close = view.findViewById<View>(R.id.close)
        close.setOnClickListener {
            activity?.finish()
        }
        if (_activity != null) {
            prefs = _activity!!.getSharedPreferences("TTAppPrefs", AppCompatActivity.MODE_PRIVATE)
            loadFavorites()
        }
        favoriteButton = view.findViewById(R.id.favorite)
        updateFavoriteButtonView()
        favoriteButton.setOnClickListener {
            if (isProductFavorite()) {
                removeProductFromFavorite()
            } else {
                addProductToFavorite()
            }
            saveFavorites()
            updateFavoriteButtonView()
        }

        if (product != null) {

            if (product.groupModifiers.count() > 0) {
                for (mod in product.groupModifiers) {
                    val gMod = IikoAPI.getGroup(mod.modifierId)

                    if (gMod != null) {
                        isPizzaSize = gMod.name.contains("пицца", true)
                    }

                    if (mod.childModifiers.count() > 0) {
                        for (chmod in mod.childModifiers) {
                            val pMod = IikoAPI.getProductModifier(chmod.modifierId)

                            if (pMod != null) {
                                if (isPizzaSize) pizzaSizes.add(pMod)
                                else if (isPizzaPiquancy) pizzaPiquancy.add(pMod)
                            }
                        }
                    }
                }
            }

            isPizzaSize = pizzaSizes.count() > 0
            isPizzaPiquancy = pizzaPiquancy.count() > 0

            val pizza_size = view.findViewById<RadioGroup>(R.id.pizza_size)

            pizza_size.visibility = if (isPizzaSize) View.VISIBLE else View.GONE

            var size = ""

            if (isPizzaSize) {
                val selectedButton:RadioButton = view.findViewById(pizza_size.checkedRadioButtonId)
                selectedSize = pizzaSizes.find { it.name.contains(selectedButton.text, true) }?.id
                size = selectedButton.text.toString().replace("размер ", "", true)+ ", "
            }

            pizza_size.setOnCheckedChangeListener { radioGroup, i ->
                val selectedButton:RadioButton = view.findViewById(radioGroup.checkedRadioButtonId)
                selectedSize = pizzaSizes.find { it.name.contains(selectedButton.text, true) }?.id

                itemsCount?.text = 1.toString()
                size = selectedButton.text.toString().replace("размер ", "", true)+ ", "
                updateUI(size)
            }

            if (product.modifiers.count() > 0) {
                for (mod in product.modifiers) {
                    val pMod = IikoAPI.getProductModifier(mod.modifierId)

                    if (pMod != null) {

                    }
                }
            }

            val name = view.findViewById<TextView>(R.id.streetEdit)
            weight = view.findViewById(R.id.weight)
            price = view.findViewById(R.id.price)
            val desc = view.findViewById<TextView>(R.id.desc)

            name.text = product.name

            val isItPizzaImage = product.name.contains("пицца", true) && !product.name.contains("кальцоне", true)
            pizzaPhoto = view.findViewById(R.id.pizzaPic)
            nonPizzaPhoto = view.findViewById(R.id.nonPizzaPic)


            val img = product.images.firstOrNull()
            if (img != null && img.imageUrl != "") {
                if (isItPizzaImage) {
                    nonPizzaPhoto?.visibility = View.GONE
                    pizzaPhoto?.visibility = View.VISIBLE

                    Picasso.get()
                            .load(img.imageUrl)
                            .placeholder(R.drawable.placeholder)
                            .into(pizzaPhoto)
                } else {
                    pizzaPhoto?.visibility = View.GONE
                    nonPizzaPhoto?.visibility = View.VISIBLE

                    Picasso.get()
                            .load(img.imageUrl)
                            .placeholder(R.drawable.placeholder)
                            .into(nonPizzaPhoto)
                }
            } else {
                pizzaPhoto?.visibility = View.GONE
                nonPizzaPhoto?.visibility = View.GONE
            }

            weight?.text = (product.weight * 1000).toString("г.")
            price?.text = product.price.toString("\u20BD")
            desc.text = product.description

            minus = view.findViewById(R.id.minus_product)
            plus = view.findViewById(R.id.plus_product)
            itemsCount = view.findViewById(R.id.items_count_product)

            addToCart = view.findViewById(R.id.to_cart)
            cartAmount = view.findViewById(R.id.cart_amount)

            cart = view.findViewById(R.id.cart)

            minus?.setOnClickListener {
                var count: Int = itemsCount?.text.toString().toInt()
                count -= 1
                if (count <= 1) count = 1
                itemsCount?.text = count.toString()
                IikoAPI.removeFromCart(productID, selectedSize)
                updateUI()
            }
            plus?.setOnClickListener {
                var count: Int = itemsCount?.text.toString().toInt()
                count += 1
                itemsCount?.text = count.toString()
                IikoAPI.addTocart(productID, selectedSize, 1)
                updateUI()
            }

            addToCart?.setOnClickListener {
                var count: Int = itemsCount?.text.toString().toInt()
                IikoAPI.addTocart(productID, selectedSize, count)
                count = 1
                itemsCount?.text = count.toString()
                BaseActivity.vibrate(context)
                updateUI()
            }

            cart?.setOnClickListener {
                IikoAPI.onGoToCart?.invoke()
                activity?.finish()
            }

            updateUI(size)

            val energy = view.findViewById<TextView>(R.id.energy)
            val energy_full = view.findViewById<TextView>(R.id.energy_full)
            val fiber = view.findViewById<TextView>(R.id.fiber)
            val fiber_full = view.findViewById<TextView>(R.id.fiber_full)
            val carbohydrate = view.findViewById<TextView>(R.id.carbohydrate)
            val carbohydrate_full = view.findViewById<TextView>(R.id.carbohydrate_full)
            val fats = view.findViewById<TextView>(R.id.fats)
            val fats_full = view.findViewById<TextView>(R.id.fats_full)

            energy?.text = product.energyAmount.toString("г.")
            energy_full?.text = product.energyFullAmount.toString("г.")

            fiber?.text = product.fiberAmount.toString("г.")
            fiber_full?.text = product.fiberFullAmount.toString("г.")

            carbohydrate?.text = product.carbohydrateAmount.toString("г.")
            carbohydrate_full?.text = product.carbohydrateFullAmount.toString("г.")

            fats?.text = product.fatAmount.toString("г.")
            fats_full?.text = product.fatFullAmount.toString("г.")
        }

        return view
    }

    private fun updateFavoriteButtonView() {
        if (isProductFavorite()){
            Picasso.get()
                    .load(R.drawable.favorite)
                    .into(favoriteButton)
        } else {
            Picasso.get()
                    .load(R.drawable.favorite_disabled)
                    .into(favoriteButton)
        }
    }

    private fun addProductToFavorite() {
        favoriteProductList.add(productID)
    }

    private fun removeProductFromFavorite() {
        favoriteProductList.remove(productID)
    }

    var minus: Button? = null
    var plus: Button? = null
    private var addToCart: Button? = null
    var itemsCount: TextView? = null

    var price: TextView? = null
    var weight: TextView? = null

    var cartAmount: TextView? = null
    var cart: ImageView? = null

    private fun updateUI(size: String? = "") {
        if (selectedSize != null) {
            val product = IikoAPI.getProduct(selectedSize!!)
            if (product != null) {
                price?.text = product.price.toString("\u20BD")
                weight?.text = size + "вес " + (product.weight*1000).toString("гр")
            }
        }

        val count: Int = itemsCount?.text.toString().toInt()
        minus?.isEnabled = count > 1

        _activity?.updateCartCount()

        val cartCount = IikoAPI.getCartCount()
        if (cartCount > 0) {
            cartAmount?.visibility = View.VISIBLE
            cartAmount?.text = cartCount.toString()
            minus?.isEnabled = true
            minus?.setTextColor(resources.getColor(R.color.colorPrimary))
        } else {
            cartAmount?.visibility = View.INVISIBLE
            minus?.isEnabled = false
            minus?.setTextColor(resources.getColor(R.color.disabledButton))
        }
    }

    private fun isProductFavorite(): Boolean{
        if (favoriteProductList.count() > 0 && productID != "") {
            return favoriteProductList.contains(productID)
        }
        return false
    }

    private fun loadFavorites() {
        val addressListJson = prefs?.getString("favoriteProductList", null)
        if (addressListJson != null) {
            val moshi = Moshi.Builder().build()
            val type = Types.newParameterizedType(List::class.java, String::class.java)
            val adapter = moshi.adapter<List<String>>(type)
            val result = adapter.fromJson(addressListJson)
            if (result != null) {
                favoriteProductList.clear()
                val loadedList = ArrayList(result)
                favoriteProductList.addAll(loadedList)
            }
        }
    }

    private fun saveFavorites() {
        val moshi = Moshi.Builder().build()
        val type = Types.newParameterizedType(List::class.java, String::class.java)
        val adapter = moshi.adapter<List<String>>(type)
        val jsonFavoriteList = adapter.toJson(favoriteProductList)
        val prefEditor = prefs?.edit()
        prefEditor?.putString("favoriteProductList", jsonFavoriteList)
        prefEditor?.putBoolean("favoriteChanged", true)
        prefEditor?.apply()
    }
}

class ProductCollectionPagerAdapter(fm: FragmentManager, private var products:ArrayList<Product>): FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        val item = products[position]
        val fragment = ProductFragment()
        val args = Bundle()
        args.putString("productId", item.id)
        fragment.arguments = args
        return fragment
    }

    override fun getCount(): Int {
        return  products.count()
    }

}