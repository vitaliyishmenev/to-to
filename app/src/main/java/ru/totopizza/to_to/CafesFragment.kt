package ru.totopizza.to_to

import android.content.pm.PackageManager
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import android.location.Criteria
import android.content.Context.LOCATION_SERVICE
import android.location.LocationManager

/**
 * Created by Enico on 12/07/2017.
 */
class CafesFragment : Fragment(), OnMapReadyCallback {

    private val MY_PERMISSIONS_REQUEST = 123

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        ActivityCompat.requestPermissions(activity!!,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION),
                MY_PERMISSIONS_REQUEST)

        val view = inflater.inflate(R.layout.fragment_cafes, container, false)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

//        fab.setOnClickListener { view ->
////            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
////                    .setAction("Action", null).show()
//
//            var detailActivity = Intent(activity, RestaurantsActivity::class.java)
//            activity.startActivity(detailActivity)
//
//        }

        val fmanager = childFragmentManager
        val fragment = fmanager.findFragmentById(R.id.map)
        val supportmapfragment = fragment as SupportMapFragment
        val supportMap = supportmapfragment.getMapAsync(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onMapReady(map: GoogleMap) {

        val restaurants = IikoAPI.loadRestraunts(activity!!)
        if (restaurants != null) {
            val rest = restaurants.values.flatten()
            for (restaurant in rest) {
                val cphbusiness = LatLng(restaurant.geo.latitude, restaurant.geo.longitude)
                map.addMarker(MarkerOptions()
                        .position(cphbusiness)
                        .title(restaurant.street)
                        .snippet(/*restaurant.phone + " " + */restaurant.worktime)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_image_little)))
                                //.fromResource(R.mipmap.ic_launcher_round)))

                map.moveCamera(CameraUpdateFactory.newLatLng(cphbusiness))
            }
        }

        if (ActivityCompat.checkSelfPermission(activity!!, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity!!, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }

        map.isMyLocationEnabled = true
        val locationManager = activity?.getSystemService(LOCATION_SERVICE) as LocationManager
        val criteria = Criteria()
        val provider = locationManager.getBestProvider(criteria, true)
        var location: android.location.Location? = null
        if (provider != null)
            location = locationManager.getLastKnownLocation(provider)

        if (location != null) {
            val latitude = location.latitude
            val longitude = location.longitude
            val latLng = LatLng(latitude, longitude)
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15F))
            map.animateCamera(CameraUpdateFactory.zoomTo(10.0F), 2000, null)
        }
    }

}