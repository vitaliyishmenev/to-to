package ru.totopizza.to_to

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import kotlinx.android.synthetic.main.activity_restaurant_select.*
import kotlinx.android.synthetic.main.content_restaurants.*
import android.content.Intent



class RestaurantSelectActivity : BaseActivity() {
    var restaurantList = ArrayList<Restaurant>()
    var items: ArrayList<RestaurantItem> = ArrayList()

    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: RestaurantsActivity.RecyclerViewAdapter? = null
    private var mLayoutManager: RecyclerView.LayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurant_select)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        var city = intent.getStringExtra("city")

        val restaurants = IikoAPI.loadRestraunts(this)
        if (restaurants != null) {
            val rest = restaurants.values.flatten()

            var location: Location? = null

            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
                val criteria = Criteria()
                val provider = locationManager.getBestProvider(criteria, true)
                if (provider != null)
                    location = locationManager.getLastKnownLocation(provider)
            }

            for (restaurant in rest) {
                if (!city.isNullOrEmpty() && restaurant.city == city) {
                    restaurantList.add(restaurant)
                    items.add(RestaurantItem(restaurant, location))
                }
            }

            items.sortBy { item -> item.distanceInMeters }

            mRecyclerView = recycler_view
            mRecyclerView?.setHasFixedSize(true)
            mLayoutManager = LinearLayoutManager(this)
            mRecyclerView?.layoutManager = mLayoutManager
            val selectedRestAddress = intent.getStringExtra("selectedRestAddress") ?: ""
            mAdapter = RestaurantsActivity.RecyclerViewAdapter(this, items, true, false, selectedRestAddress)
            mRecyclerView?.adapter = mAdapter


            mAdapter?.mOnClickListener = { item ->
                val intent = Intent()
                intent.putExtra("rest", item)
                intent.putExtra("restAddress", item.street)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

}
